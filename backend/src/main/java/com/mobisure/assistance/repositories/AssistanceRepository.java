package com.mobisure.assistance.repositories;

import com.mobisure.assistance.model.Assistance;
import com.mobisure.assistance.model.TypeAssistance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AssistanceRepository extends JpaRepository<Assistance, Integer> {
    List<Assistance> findAll();
    List<Assistance> findByIdPersonneEmploye(int id);
    List<Assistance> findAllByIdPartenaire(int idPartenaire);
    Assistance findByIdAssistance(int idAssistance);
}
