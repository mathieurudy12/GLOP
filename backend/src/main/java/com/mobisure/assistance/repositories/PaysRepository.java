package com.mobisure.assistance.repositories;

import com.mobisure.assistance.model.Pays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PaysRepository extends JpaRepository<Pays, Integer> {
    List<Pays> findAll();
    Optional<Pays> findByCodeIso(String codeISO);
}
