package com.mobisure.assistance.repositories;

import com.mobisure.assistance.model.EtatAssistance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EtatAssistanceRepository extends JpaRepository<EtatAssistance, Integer> {
    EtatAssistance findByNomEtat(String nomEtat);
    EtatAssistance findByIdEtat(int idEtat);
}
