package com.mobisure.assistance.repositories;

import com.mobisure.assistance.model.Situer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SituerRepository extends JpaRepository<Situer,Integer> {
    Optional<Situer> findByIdPartenaire(int idPartenaire);
}
