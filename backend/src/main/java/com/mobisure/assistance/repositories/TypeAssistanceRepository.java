package com.mobisure.assistance.repositories;

import com.mobisure.assistance.model.TypeAssistance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TypeAssistanceRepository extends JpaRepository<TypeAssistance, Integer> {
    List<TypeAssistance> findAll();
    Optional<TypeAssistance> findByLibelleTypeAssistance(String type);
    TypeAssistance findByIdTypeAssistance(int id);
}
