package com.mobisure.assistance.repositories;

import com.mobisure.assistance.model.DateEtat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface DateEtatRepository extends JpaRepository<DateEtat, LocalDateTime> {
}
