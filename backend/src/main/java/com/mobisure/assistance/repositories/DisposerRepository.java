package com.mobisure.assistance.repositories;

import com.mobisure.assistance.model.Disposer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisposerRepository extends JpaRepository<Disposer, Integer> {
    List<Disposer> findAllByIdAssistance(int idAssistance);
    @Query(value = "SELECT nom_etat FROM DISPOSER AS d JOIN ETAT_ASSISTANCE AS e ON e.id_etat=d.id_etat WHERE id_assistance = ?1", nativeQuery = true) // SQL
    String findLastEtatById(int idAssistance);
    Disposer findByIdAssistanceAndIdEtat(int idAssistance, int idEtat);
}
