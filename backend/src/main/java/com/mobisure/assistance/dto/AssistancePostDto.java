package com.mobisure.assistance.dto;

import lombok.Data;

@Data
public class AssistancePostDto {
    float tarif;
    String commentaires;
    int idPersonneEmploye;
    int idPersonneAssure;
    int idTypeAssistance;
    String valueTypeSinistre;
    String codeIso;
    int idPartenaire;
}
