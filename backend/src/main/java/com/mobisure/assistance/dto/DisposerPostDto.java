package com.mobisure.assistance.dto;

import lombok.Builder;
import lombok.Data;

@Data
public class DisposerPostDto {
    String idAssistance;
    String idEtatAssistance;
}
