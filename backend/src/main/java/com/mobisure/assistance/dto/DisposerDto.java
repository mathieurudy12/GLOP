package com.mobisure.assistance.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DisposerDto {
    String dateEtat;
    String heureEtat;
    String status;
}
