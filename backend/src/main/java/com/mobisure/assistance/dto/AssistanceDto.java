package com.mobisure.assistance.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;



@Data
@Builder
public class AssistanceDto {
    String commentaires;
    String typeAssistance;
    float tarif;
    List<DisposerDto> etats;
    String prenomAssure;
    String nomAssure;
    String dateNaissanceAssure;
}
