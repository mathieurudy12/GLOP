package com.mobisure.assistance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "pays")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Pays {
    @Id
    String codeIso;
    String nomPays;
}
