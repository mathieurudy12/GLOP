package com.mobisure.assistance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.time.LocalTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "disposer")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Disposer {
    @Id
    int idAssistance;
    LocalDateTime dateEtat;
    LocalTime heureEtat;
    int idEtat;
}
