package com.mobisure.assistance.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;


public class DateEtatId implements Serializable {
    LocalDateTime dateEtat;
    LocalTime heureEtat;

    public DateEtatId(LocalDateTime dateEtat, LocalTime heureEtat){
        this.dateEtat = dateEtat;
        this.heureEtat = heureEtat;
    }

    public DateEtatId(){

    }
}
