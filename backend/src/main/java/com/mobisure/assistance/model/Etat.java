package com.mobisure.assistance.model;

public enum Etat {
    ACCEPTED,REFUSED,PROCESSING,PENDING
}
