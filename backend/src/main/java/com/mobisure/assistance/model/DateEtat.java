package com.mobisure.assistance.model;

import com.mobisure.assurance.model.ComposerId;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.time.LocalTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "date_etat")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@IdClass(DateEtatId.class)
public class DateEtat {
    @Id
    LocalDateTime dateEtat;
    @Id
    LocalTime heureEtat;
}
