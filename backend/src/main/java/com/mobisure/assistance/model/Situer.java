package com.mobisure.assistance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "situer")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Situer {
    @Id
    int idPartenaire;
    int idAdresse;
}
