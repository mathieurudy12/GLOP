package com.mobisure.assistance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "etat_assistance")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EtatAssistance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idEtat;
    String nomEtat;
}
