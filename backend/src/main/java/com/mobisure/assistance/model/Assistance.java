package com.mobisure.assistance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "assistance")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Assistance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idAssistance;
    float tarif;
    String commentaires;
    int idPersonneEmploye;
    int idPersonneAssure;
    int idTypeAssistance;
    String typeSinistre;
    String codeIso;
    int idPartenaire;
}
