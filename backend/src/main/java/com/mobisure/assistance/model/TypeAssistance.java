package com.mobisure.assistance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "type_assistance")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TypeAssistance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idTypeAssistance;
    String libelleTypeAssistance;
}
