package com.mobisure.assistance;

import com.mobisure.assistance.dto.AssistancePostDto;
import com.mobisure.partenaire.PartenaireService;
import com.mobisure.partenaire.dto.PartenaireDto;
import com.mobisure.assistance.model.Pays;
import com.mobisure.assistance.model.TypeAssistance;
import com.mobisure.assistance.dto.DisposerPostDto;
import com.mobisure.assistance.model.*;
import com.mobisure.profile.model.Assure;
import com.mobisure.profile.model.Majeur;
import com.mobisure.profile.model.Personne;
import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.ASSISTANCE)
public class AssistanceController {

    AssistanceService assistanceService;
    PartenaireService partenaireService;

    @GetMapping("/getPays")
    public ResponseEntity<List<Pays>> getPays() {
        return ResponseEntity.ok(assistanceService.getPays());
    }

    @GetMapping("/getTypeAssistances")
    public ResponseEntity<List<TypeAssistance>> getTypeAssistances() {
        return ResponseEntity.ok(assistanceService.getTypeAssistances());
    }

    @PostMapping("/postTypeAssistance")
    public ResponseEntity<TypeAssistance> postTypeAssistance(@RequestBody String type) {
        return ResponseEntity.ok(assistanceService.postTypeAssistance(type));
    }

    @GetMapping("/getPartenaires")
    public ResponseEntity<List<PartenaireDto>> getPartenaires(@RequestParam String codeIso) {
        return ResponseEntity.ok(partenaireService.getPartenaires(codeIso));
    }

    @PostMapping("/postAssistance")
    public ResponseEntity postAssistance(@RequestBody AssistancePostDto assistancePostDto) {
        return ResponseEntity.ok(assistanceService.saveAssistance(assistancePostDto));
    }

    @GetMapping("/getAllAssist")
    public ResponseEntity<List<Assistance>> getAllAssist() {
        return ResponseEntity.ok(assistanceService.getAllAssistance());
    }

    @GetMapping("/getMyAssist")
    public ResponseEntity<List<Assistance>> getMyAssist(@RequestParam String idPerson) {
        return ResponseEntity.ok(assistanceService.getMyAssistance(Integer.parseInt(idPerson)));
    }

    @GetMapping("/getAssureFromId")
    public ResponseEntity<Personne> getAssureFromId(@RequestParam String idPerson) {
        return ResponseEntity.ok(assistanceService.getAssureFromId(idPerson));
    }

    @GetMapping("/getPartenaireFromId")
    public ResponseEntity<String> getPartenaireFromId(@RequestParam String idPartenaire) {
        return ResponseEntity.ok(assistanceService.getPartenaireFromId(idPartenaire));
    }

    @GetMapping("/getTypeAssistanceFromId")
    public ResponseEntity<String> getTypeAssistanceFromId(@RequestParam String idTypeAssistance) {
        return ResponseEntity.ok(assistanceService.getTypeAssistanceFromId(idTypeAssistance));
    }

    @GetMapping("/getPaysFromId")
    public ResponseEntity<String> getPaysFromId(@RequestParam String codeIso) {
        return ResponseEntity.ok(assistanceService.getPaysFromId(codeIso));
    }

    @GetMapping("/getCurrentEtat")
    public ResponseEntity<String> getCurrentEtat(@RequestParam String idAssistance) {
        return ResponseEntity.ok(assistanceService.getCurrentEtat(idAssistance));
    }

    @GetMapping("/getEtats")
    public ResponseEntity<List<EtatAssistance>> getEtats() {
        return ResponseEntity.ok(assistanceService.getEtats());
    }

    @PostMapping("/postState")
    public ResponseEntity postState(@RequestBody DisposerPostDto disposerPostDto) {
        return ResponseEntity.ok(assistanceService.postState(disposerPostDto));
    }
}
