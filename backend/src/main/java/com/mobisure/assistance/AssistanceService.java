package com.mobisure.assistance;

import com.mobisure.assistance.dto.AssistanceDto;
import com.mobisure.assistance.dto.AssistancePostDto;
import com.mobisure.assistance.dto.DisposerDto;
import com.mobisure.assistance.dto.DisposerPostDto;
import com.mobisure.assistance.model.*;
import com.mobisure.assistance.repositories.*;
import com.mobisure.collaborateur.repositories.ProfessionnelRepository;
import com.mobisure.exception.ApplicationException;
import com.mobisure.partenaire.dto.PartenaireDto;
import com.mobisure.partenaire.model.ContactPartenaire;
import com.mobisure.partenaire.model.Partenaire;
import com.mobisure.partenaire.repositories.ContactPartenaireRepository;
import com.mobisure.partenaire.repositories.PartenaireRepository;
import com.mobisure.profile.model.Adresse;
import com.mobisure.profile.model.Personne;
import com.mobisure.profile.repositories.AdresseRepository;
import com.mobisure.profile.repositories.AssureRepository;
import com.mobisure.profile.repositories.MajeurRepository;
import com.mobisure.profile.repositories.PersonneRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class AssistanceService {

    PaysRepository paysRepository;
    TypeAssistanceRepository typeAssistanceRepository;
    PartenaireRepository partenaireRepository;
    AssureRepository assureRepository;
    ProfessionnelRepository professionnelRepository;
    AssistanceRepository assistanceRepository;
    EtatAssistanceRepository etatAssistanceRepository;
    DisposerRepository disposerRepository;
    DateEtatRepository dateEtatRepository;
    ContactPartenaireRepository contactPartenaireRepository;
    PersonneRepository personneRepository;
    SituerRepository situerRepository;
    AdresseRepository adresseRepository;

    public List<Pays> getPays() {
        List<Pays> listPays;
        listPays = paysRepository.findAll();
        return listPays;
    }

    public List<TypeAssistance> getTypeAssistances() {
        List<TypeAssistance> listTypeAssistance;
        listTypeAssistance = typeAssistanceRepository.findAll();
        return listTypeAssistance;
    }

    public TypeAssistance postTypeAssistance(String type) {
        if (typeAssistanceRepository.findByLibelleTypeAssistance(type).isPresent()) {
            throw new ApplicationException(HttpStatus.CONFLICT, "Ce type d'assistance existe déjà !");
        }
        TypeAssistance typeAssistance = TypeAssistance.builder()
                .libelleTypeAssistance(type)
                .build();
        typeAssistanceRepository.save(typeAssistance);
        return typeAssistance;
    }

    public List<PartenaireDto> getPartenaires(String codeIso) {
        paysRepository.findByCodeIso(codeIso);
        List<Partenaire> listPartenaire = partenaireRepository.findAll();
        if (listPartenaire.isEmpty()) {
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Pas de partenaire pour ce pays !");
        }
        List<PartenaireDto> partenaireDtoList = new ArrayList<>();
        listPartenaire.stream().forEach(partenaire -> {
            Situer situer = situerRepository.findById(partenaire.getIdPartenaire()).
                    orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Pas d'adresse de trouver pour ce partenaire"));
            Adresse adresse = adresseRepository.findByIdAdresse(situer.getIdAdresse());
            if (adresse.getCodeIso().equals(codeIso)) {
                PartenaireDto partenaireDto = PartenaireDto.builder()
                        .partenaire(partenaire)
                        .adresse(adresse)
                        .build();
                partenaireDtoList.add(partenaireDto);
            }
        });
        return partenaireDtoList;
    }

    public Assistance saveAssistance(AssistancePostDto assistancePostDto) {
        if (!partenaireRepository.existsById(assistancePostDto.getIdPartenaire())) {
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Partenaire non trouvé");
        }
        if (!assureRepository.existsByIdPersonne(assistancePostDto.getIdPersonneAssure())) {
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Assuré non trouvé");
        }
        if (!professionnelRepository.existsByIdPersonne(assistancePostDto.getIdPersonneEmploye())) {
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Employe non trouvé");
        }
        if (!typeAssistanceRepository.existsById(assistancePostDto.getIdTypeAssistance())) {
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Type assistance non trouvé");
        }

        paysRepository.findByCodeIso(assistancePostDto.getCodeIso()).orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Code ISO non trouvé"));

        Assistance assistance = Assistance.builder()
                .commentaires(assistancePostDto.getCommentaires())
                .idPersonneEmploye(assistancePostDto.getIdPersonneEmploye())
                .idPersonneAssure(assistancePostDto.getIdPersonneAssure())
                .idPartenaire(assistancePostDto.getIdPartenaire())
                .idTypeAssistance(assistancePostDto.getIdTypeAssistance())
                .typeSinistre(assistancePostDto.getValueTypeSinistre())
                .tarif(assistancePostDto.getTarif())
                .codeIso(assistancePostDto.getCodeIso())
                .build();

        assistance = assistanceRepository.save(assistance);
        EtatAssistance etat = etatAssistanceRepository.findByNomEtat(Etat.PENDING.toString());
        DateEtat dateEtat = dateEtatRepository.save(DateEtat.builder()
                .dateEtat(LocalDateTime.now())
                .heureEtat(LocalTime.now())
                .build());
        Disposer disposer = Disposer.builder()
                .heureEtat(dateEtat.getHeureEtat())
                .dateEtat(dateEtat.getDateEtat())
                .idAssistance(assistance.getIdAssistance())
                .idEtat(etat.getIdEtat())
                .build();
        disposerRepository.save(disposer);
        return assistance;
    }

    public List<AssistanceDto> getAssistanceByIdContactPartenaire(String id) {
        ContactPartenaire contactPartenaire = contactPartenaireRepository.findByIdPersonne(Integer.parseInt(id))
                .orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Contact partenaire not found"));
        List<Assistance> assistanceList = assistanceRepository.findAllByIdPartenaire(contactPartenaire.getIdPartenaire());
        List<AssistanceDto> assistanceDtos = new ArrayList<>();
        if (assistanceList.isEmpty()) return assistanceDtos;
        assistanceList.forEach(assistance -> {
            Personne personne = personneRepository.findByIdPersonne(assistance.getIdPersonneAssure());
            TypeAssistance typeAssistance = typeAssistanceRepository.findByIdTypeAssistance(assistance.getIdTypeAssistance());
            List<Disposer> disposerList = disposerRepository.findAllByIdAssistance(assistance.getIdAssistance());
            List<DisposerDto> etats = disposerList.stream().map(disposer -> DisposerDto.builder()
                    .status(etatAssistanceRepository.findByIdEtat(disposer.getIdEtat()).getNomEtat())
                    .dateEtat(disposer.getDateEtat().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")))
                    .heureEtat(disposer.getHeureEtat().format(DateTimeFormatter.ofPattern("HH:mm")))
                    .build()).collect(Collectors.toList());
            AssistanceDto assistanceDto = AssistanceDto.builder()
                    .commentaires(assistance.getCommentaires())
                    .dateNaissanceAssure(personne.getDateNaissance().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")))
                    .nomAssure(personne.getNom())
                    .prenomAssure(personne.getPrenom())
                    .typeAssistance(typeAssistance.getLibelleTypeAssistance())
                    .etats(etats)
                    .tarif(assistance.getTarif())
                    .build();
            assistanceDtos.add(assistanceDto);
        });
        return assistanceDtos;
    }

    public List<Assistance> getAllAssistance() {
        return assistanceRepository.findAll();
    }

    public List<Assistance> getMyAssistance(int idEmploye) {
        return assistanceRepository.findByIdPersonneEmploye(idEmploye);
    }

    public Personne getAssureFromId(String idPerson) {
        return personneRepository.findById(Integer.parseInt(idPerson)).orElseThrow(() ->
                new ApplicationException(HttpStatus.NOT_FOUND, "Personne non trouvé"));
    }

    public String getPartenaireFromId(String idPartenaire) {
        return partenaireRepository.findById(Integer.parseInt(idPartenaire)).orElseThrow(() ->
                        new ApplicationException(HttpStatus.NOT_FOUND, "Partenaire non trouvé"))
                .getNomPartenaire();
    }

    public String getTypeAssistanceFromId(String idTypeAssistance) {
        return typeAssistanceRepository.findById(Integer.parseInt(idTypeAssistance)).orElseThrow(() ->
                new ApplicationException(HttpStatus.NOT_FOUND, "Type assistance non trouvé")).getLibelleTypeAssistance();
    }

    public String getPaysFromId(String codeIso) {
        return paysRepository.findByCodeIso(codeIso).orElseThrow(() ->
                new ApplicationException(HttpStatus.NOT_FOUND, "Code iso non trouvé")).getNomPays();
    }

    public String getCurrentEtat(String idAssistance) {
        return disposerRepository.findLastEtatById(Integer.parseInt(idAssistance));
    }

    public List<EtatAssistance> getEtats() {
        return etatAssistanceRepository.findAll();
    }

    public Disposer postState(DisposerPostDto disposerPostDto) {
        DateEtat dateEtat = dateEtatRepository.save(DateEtat.builder()
                .dateEtat(LocalDateTime.now())
                .heureEtat(LocalTime.now())
                .build());
        Disposer disposer = Disposer.builder()
                .idAssistance(Integer.parseInt(disposerPostDto.getIdAssistance()))
                .idEtat(Integer.parseInt(disposerPostDto.getIdEtatAssistance()))
                .heureEtat(dateEtat.getHeureEtat())
                .dateEtat(dateEtat.getDateEtat())
                .build();
        disposerRepository.save(disposer);
        return disposer;
    }
}