package com.mobisure.profile;

import com.mobisure.exception.ApplicationException;
import com.mobisure.profile.ProfileService;
import com.mobisure.profile.dtos.ProfileUpdateInfosDto;
import com.mobisure.profile.model.Adresse;
import com.mobisure.profile.model.Personne;
import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.PROFILE)
public class ProfileController {

    ProfileService profileService;

    @GetMapping("/getInfos/{idPersonne}")
    public ResponseEntity<HashMap<String, String>> getInfos(@PathVariable int idPersonne){
        return ResponseEntity.ok(profileService.getInfos(idPersonne));
    }

    @GetMapping("/getAdresses/{idPersonne}")
    public ResponseEntity<List<Adresse>> getAdresses(@PathVariable int idPersonne){
        return ResponseEntity.ok(profileService.getAdresses(idPersonne));
    }

    @GetMapping("/getAllVehicules/{idPersonne}")
    public ResponseEntity getAllVehicules(@PathVariable int idPersonne) {
        try {
            return ResponseEntity.ok(profileService.getAllVehicules(idPersonne));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @PutMapping("/employees/{idPersonne}")
    ResponseEntity updatePersonne(@RequestBody ProfileUpdateInfosDto profileUpdateInfosDto, @PathVariable int idPersonne) {

        Personne p = profileService.updatePersonne(profileUpdateInfosDto,idPersonne);

        if (p == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(idPersonne, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deletePersonne/{idPersonne}")
    public ResponseEntity deletePersonne(@PathVariable int idPersonne) {

        boolean isRemoved = profileService.deletePersonne(idPersonne);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(idPersonne, HttpStatus.OK);
    }
}
