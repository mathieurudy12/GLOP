package com.mobisure.profile.dtos;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class PersonneDto {
    int idPersonne;
    String nom;
    String prenom;
    String dateNaissance;
}
