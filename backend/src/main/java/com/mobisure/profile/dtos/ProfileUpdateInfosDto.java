package com.mobisure.profile.dtos;

import lombok.Data;

@Data
public class ProfileUpdateInfosDto {
    String nom;
    String prenom;
    String dateNaissance;
    String mdp;
    String telephone;
}
