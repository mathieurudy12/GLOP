package com.mobisure.profile;

import com.mobisure.assurance.model.Vehicule;
import com.mobisure.assurance.repositories.VehiculeRepository;
import com.mobisure.collaborateur.repositories.ProfessionnelRepository;
import com.mobisure.exception.ApplicationException;
import com.mobisure.profile.dtos.ProfileUpdateInfosDto;
import com.mobisure.profile.model.*;
import com.mobisure.profile.repositories.*;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class ProfileService {

    PersonneRepository personneRepository;
    AssureRepository assureRepository;
    AdresseRepository adresseRepository;
    HabiterRepository habiterRepository;
    MajeurRepository majeurRepository;
    VehiculeRepository vehiculeRepository;
    ProfessionnelRepository professionnelRepository;

    public Personne createAndSavePersonne(String nom, String prenom, LocalDateTime dateNaissance) {
        Personne personne = Personne.builder()
                .nom(nom)
                .prenom(prenom)
                .dateNaissance(dateNaissance)
                .build();
        return personneRepository.save(personne);
    }

    public Assure createAndSaveAssure(int idAssure) {
        Assure assure = Assure.builder()
                .idPersonne(idAssure)
                .build();
        return assureRepository.save(assure);
    }

    public Adresse createAndSaveAdresse(String rue, String villeCP, String complements, String codeISO) {
        Adresse adresse = Adresse.builder()
                .rueAdresse(rue)
                .villeCpAdresse(villeCP)
                .complements(complements)
                .codeIso(codeISO)
                .build();
        return adresseRepository.save(adresse);
    }

    public Habiter createAndSaveHabiter(int idPersonne, int idAdresse) {
        if (!majeurRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Habiter !");
        }
        Habiter habiter = Habiter.builder()
                .idPersonne(idPersonne)
                .idAdresse(idAdresse)
                .build();
        return habiterRepository.save(habiter);
    }

    public HashMap<String, String> getInfos(int idPersonne) {
        if (!majeurRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Habiter !");
        }
        HashMap<String, String> infosPersonne = new HashMap<>();
        Personne personne = personneRepository.findByIdPersonne(idPersonne);
        Majeur majeur = majeurRepository.findByIdPersonne(idPersonne);
        infosPersonne.put("nom", personne.getNom());
        infosPersonne.put("prenom", personne.getPrenom());
        infosPersonne.put("telephone", majeur.getTelephoneAssure());
        infosPersonne.put("mail", majeur.getMailAssure());
        infosPersonne.put("date_naissance", personne.getDateNaissance().toLocalDate().toString());
        return infosPersonne;
    }

    public List<Adresse> getAdresses(int idPersonne) {
        if (!majeurRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Habiter !");
        }
        List<Adresse> adresses = new ArrayList<>();
        List<Habiter> habitations = habiterRepository.getAllByIdPersonne(idPersonne);
        for (Habiter h : habitations) {
            adresses.add(adresseRepository.findByIdAdresse(h.getIdAdresse()));
        }
        return adresses;
    }

    public List<Vehicule> getAllVehicules(int idPersonne) {
        List<Vehicule> listVehicules = new ArrayList<>();
        listVehicules = vehiculeRepository.findAllByIdPersonne(idPersonne);
        return listVehicules;
    }

    public boolean deletePersonne(int idPersonne) {
        personneRepository.delete(personneRepository.findByIdPersonne(idPersonne));
        return true;
    }

    public Personne updatePersonne(ProfileUpdateInfosDto profileUpdateInfosDto, int idPersonne) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime dateNaissance = LocalDate.parse(profileUpdateInfosDto.getDateNaissance(), formatter).atStartOfDay();
        Personne personne = personneRepository.findByIdPersonne(idPersonne);
        personne.setNom(profileUpdateInfosDto.getNom());
        personne.setPrenom(profileUpdateInfosDto.getPrenom());
        personne.setDateNaissance(dateNaissance);
        Majeur majeur = majeurRepository.findByIdPersonne(idPersonne);
        majeur.setTelephoneAssure(profileUpdateInfosDto.getTelephone());
        majeur.setMdpAssure(profileUpdateInfosDto.getMdp());
        majeurRepository.save(majeur);
        return personneRepository.save(personne);
    }
}
