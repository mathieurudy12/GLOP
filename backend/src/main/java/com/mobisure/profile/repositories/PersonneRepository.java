package com.mobisure.profile.repositories;

import com.mobisure.profile.model.Personne;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface PersonneRepository extends JpaRepository<Personne, Integer> {
    Personne findByNomAndPrenomAndDateNaissance(String nom, String prenom, LocalDateTime dateNaissance);
    Personne findByIdPersonne(int idPersonne);
    @Query("select p from Personne p where upper(p.nom) like upper(concat(:search,'%')) or upper(p.prenom) like upper(concat(:search,'%')) order by p.nom")
    Optional<List<Personne>> findAllByNomOrPrenom(String search, Pageable pageable);
    @Query("select p from Personne p where upper(p.nom) like upper(concat(:nom,'%')) and upper(p.prenom) like upper(concat(:prenom,'%')) order by p.nom")
    Optional<List<Personne>> findAllByNomAndPrenom(String nom, String prenom, Pageable pageable);
    void deleteByIdPersonne(int idPersonne);
}
