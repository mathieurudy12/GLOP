package com.mobisure.profile.repositories;

import com.mobisure.profile.model.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdresseRepository extends JpaRepository<Adresse,Integer> {
    Adresse findByIdAdresse(int idAdresse);
}
