package com.mobisure.profile.repositories;

import com.mobisure.profile.model.Habiter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HabiterRepository extends JpaRepository<Habiter,Integer> {
    List<Habiter> getAllByIdPersonne(int idPersonne);
    Habiter findByIdPersonne(int idPersonne);
}
