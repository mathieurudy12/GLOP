package com.mobisure.profile.repositories;

import com.mobisure.profile.model.GestionAssure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GestionAssureRepository extends JpaRepository<GestionAssure,Integer> {

    GestionAssure findByIdPersonne(int idPersonne);
}
