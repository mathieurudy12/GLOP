package com.mobisure.profile.repositories;

import com.mobisure.profile.model.Assure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssureRepository extends JpaRepository<Assure, Integer> {
    boolean existsByIdPersonne(int idPersonne);
    Assure findByIdPersonne(int idPersonne);
}
