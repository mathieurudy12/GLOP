package com.mobisure.profile.repositories;

import com.mobisure.profile.model.Majeur;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MajeurRepository extends JpaRepository<Majeur, Integer> {

    boolean existsByIdPersonne(int idPersonne);
    Majeur findByIdPersonne(int idPersonne);
    Majeur findByMailAssure(String mailAssure);

    @Query("select m from Majeur m where upper(m.mailAssure) like upper(concat(:mailAssure,'%')) order by m.mailAssure")
    Optional<List<Majeur>> findAllByMailAssure(String mailAssure);
    List<Majeur> findAll();
}
