package com.mobisure.profile;

import com.mobisure.assurance.model.Mineur;
import com.mobisure.assurance.repositories.MineurRepository;
import com.mobisure.profile.model.Majeur;
import com.mobisure.profile.model.Personne;
import com.mobisure.profile.repositories.MajeurRepository;
import com.mobisure.profile.repositories.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@EnableScheduling
public class ProfileSchedule {

    @Autowired
    MajeurRepository majeurRepository;
    @Autowired
    MineurRepository mineurRepository;
    @Autowired
    PersonneRepository personneRepository;

    @Scheduled( cron = "0 0 0 * * *", zone = "Europe/Paris")
    public void mineurToMajeur() {
        List<Personne> majeurs = personneRepository.findAll();
        majeurs = majeurs.stream().filter(personne -> Period.between(personne.getDateNaissance().toLocalDate(), LocalDateTime.now().toLocalDate()).getYears() >= 18).collect(Collectors.toList());
        for (Personne personne : majeurs) {
            Optional<Mineur> mineur = mineurRepository.findById(personne.getIdPersonne());
            if (mineur.isPresent()) {
                majeurRepository.save(Majeur.builder().idPersonne(personne.getIdPersonne()).build());
                mineurRepository.delete(mineur.get());
            }
        }
    }
}
