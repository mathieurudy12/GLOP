package com.mobisure.profile.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "gestionAssure")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class GestionAssure {
    @Id
    int idPersonne;
    LocalDateTime dateDernierChangementMdp;
    LocalDateTime dateDerniereConnexion;
    LocalDateTime dateInscription;
}
