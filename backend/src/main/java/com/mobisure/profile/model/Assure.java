package com.mobisure.profile.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "assure")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Assure {
    @Id
    int idPersonne;
}
