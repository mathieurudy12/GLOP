package com.mobisure.profile.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "habiter")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Habiter{
    int idPersonne;
    @Id
    int idAdresse;
}
