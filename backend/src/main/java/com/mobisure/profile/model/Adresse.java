package com.mobisure.profile.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "adresse")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Adresse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idAdresse;
    String rueAdresse;
    String villeCpAdresse;
    String complements;
    String codeIso;
}
