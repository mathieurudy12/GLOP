package com.mobisure.profile.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "personne")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idPersonne;
    String nom;
    String prenom;
    LocalDateTime dateNaissance;
}
