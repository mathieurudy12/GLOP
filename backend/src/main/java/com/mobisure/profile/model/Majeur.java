package com.mobisure.profile.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "majeur")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Majeur {

    @Id
    int idPersonne;
    String mailAssure;
    String mdpAssure;
    String telephoneAssure;
    String situationProf;
    String situationFam;
}
