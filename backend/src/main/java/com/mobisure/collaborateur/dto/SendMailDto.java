package com.mobisure.collaborateur.dto;

import lombok.Data;

/**
 * Classe qui permet d'envoyer un mail
 */
@Data
public class SendMailDto {
    String mail;
    String message;
    String objet;
}
