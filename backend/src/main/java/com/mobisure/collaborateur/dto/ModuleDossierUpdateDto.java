package com.mobisure.collaborateur.dto;

import lombok.Data;

/**
 * Classe pour mettre à jour un module d'un contrat
 */
@Data
public class ModuleDossierUpdateDto {
    String nomModule;
    String etat;
}
