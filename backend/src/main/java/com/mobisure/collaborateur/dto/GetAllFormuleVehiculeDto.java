package com.mobisure.collaborateur.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe qui permet de récupérer tous les contrats véhicules
 */
@Data
@Builder
public class GetAllFormuleVehiculeDto {
    int idFormule;
    String immatriculation;
    String marque;
    String modele;
    String nomFormule;
    float prix;
    String status;
    String zoneGeographique;
    String nomUsage;
    String nomMode;
    String dateSouscription;
    String nom;
    String prenom;
    String mail;
    String telephone;
}
