package com.mobisure.collaborateur.dto;

import lombok.Data;

/**
 * Classe qui permet de mettre à jour le prix d'un contrat
 */
@Data
public class UpdatePrixFormuleDto {
    float prixFormule;
    String statut;
}
