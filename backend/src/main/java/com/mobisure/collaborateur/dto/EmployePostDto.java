package com.mobisure.collaborateur.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter un employé
 */
@Data
public class EmployePostDto {
    String mail;
    String prenom;
    String nom;
    String mdp;
    String dateEmbauche;
    String matricule;
}
