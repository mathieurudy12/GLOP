package com.mobisure.collaborateur.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe qui permet de récupérer un employe
 */
@Data
@Builder
public class EmployeDto {
    String matricule;
    String dateEmbauche;
    String nom;
    String prenom;
    String mail;
    int idPersonne;
}
