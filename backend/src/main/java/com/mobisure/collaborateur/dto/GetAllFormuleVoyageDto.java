package com.mobisure.collaborateur.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe qui permet de récupérer tous les contrats voyages
 */
@Data
@Builder
public class GetAllFormuleVoyageDto {
    int idFormule;
    String nomFormule;
    String destination;
    String type;
    int nbVoyageurs;
    float prix;
    String status;
    String dateSouscription;
    String dateDebut;
    String dateFin;
    String nomPersonne;
    String prenomPersonne;
    String mail;
    String telephone;
}
