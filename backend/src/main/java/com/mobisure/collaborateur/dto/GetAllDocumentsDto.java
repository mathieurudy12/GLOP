package com.mobisure.collaborateur.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe qui permet de récupérer tous les documents
 */
@Data
@Builder
public class GetAllDocumentsDto {
    int idDocument;
    String url_document;
    String nomDocument;
    int idPersonne;
    Integer idFormule;
    int nbDocument;
    String mailPersonne;
}
