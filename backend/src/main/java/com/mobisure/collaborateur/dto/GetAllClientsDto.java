package com.mobisure.collaborateur.dto;

import lombok.*;

import java.time.LocalDateTime;

/**
 * Classe qui permet de récupérer tous les assurés.
 */
@Data
@Builder
public class GetAllClientsDto {
    int idPersonne;
    String nom;
    String prenom;
    LocalDateTime dateNaissance;
    String mail;
    String telephone;
}
