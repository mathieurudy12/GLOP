package com.mobisure.collaborateur.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe qui permet de récupérer les informations d'un contrat
 */
@Data
@Builder
public class GetDossierDto {
    String prenom;
    String nom;
    String statut;
    String intitule;
    String mail;
    String telephone;
    String dateSouscription;
    float prix;
}
