package com.mobisure.collaborateur.dto;

import lombok.Data;

/**
 * Classe qui permet de mettre à jour le statut d'un contrat
 */
@Data
public class DossierUpdateDto {
    String status;
}
