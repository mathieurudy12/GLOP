package com.mobisure.collaborateur;

import com.mobisure.assurance.model.Formule;
import com.mobisure.assurance.model.ModuleFormule;
import com.mobisure.collaborateur.dto.*;
import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.COLLABORATEUR)
public class CollaborateurController {

    CollaborateurService collaborateurService;


    @GetMapping("/getAllClients")
    public ResponseEntity<List<GetAllClientsDto>> getAllClients() {
        return ResponseEntity.ok(collaborateurService.getAllClients());
    }
    @GetMapping("/getInfosAdmin/{idPersonne}")
    public ResponseEntity<HashMap<String, String>> getInfosAdmin(@PathVariable int idPersonne){
        return ResponseEntity.ok(collaborateurService.getInfosAdmin(idPersonne));
    }

    @GetMapping("/getModulesFormule/{idFormule}")
    public ResponseEntity<List<ModuleFormule>> getModulesFormule(@PathVariable int idFormule){
        return ResponseEntity.ok(collaborateurService.getModulesFormule(idFormule));
    }

    @Transactional
    @PutMapping("/deleteClient/{idPersonne}")
    public ResponseEntity deleteClient(@PathVariable int idPersonne) throws IOException {

        boolean isRemoved = collaborateurService.deleteClient(idPersonne);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(idPersonne, HttpStatus.OK);
    }

    @GetMapping("/getAllFormuleVoyage")
    public ResponseEntity<List<GetAllFormuleVoyageDto>> getAllFormuleVoyage() {
        return ResponseEntity.ok(collaborateurService.getAllFormuleVoyage());
    }

    @GetMapping("/getAllFormuleVehicule")
    public ResponseEntity<List<GetAllFormuleVehiculeDto>> getAllFormuleVehicule() {
        return ResponseEntity.ok(collaborateurService.getAllFormuleVehicule());
    }

    @GetMapping("/getDossier/{idDossier}")
    public ResponseEntity<GetDossierDto> getDossier(@PathVariable int idDossier){
        return ResponseEntity.ok(collaborateurService.getDossier(idDossier));
    }

    @PutMapping("/updateDossier/{idDossier}")
    ResponseEntity updateDossier(@RequestBody DossierUpdateDto dossierUpdateDto, @PathVariable int idDossier) {

        Formule f = collaborateurService.updateDossier(dossierUpdateDto,idDossier);

        if (f == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(idDossier, HttpStatus.OK);
    }

    @PutMapping("/updatePrixFormule/{idDossier}")
    ResponseEntity updatePrixFormule(@RequestBody UpdatePrixFormuleDto updatePrixFormuleDto, @PathVariable int idDossier){
        Formule f = collaborateurService.updatePrixFormule(updatePrixFormuleDto.getPrixFormule(), updatePrixFormuleDto.getStatut(), idDossier);

        if (f == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(idDossier, HttpStatus.OK);
    }

    @Transactional
    @PutMapping("/updateModulesDossier/{idDossier}")
    public ResponseEntity updateModulesDossier(@RequestBody ModuleDossierUpdateDto moduleDossierUpdateDto, @PathVariable int idDossier) {

        Formule f = collaborateurService.updateModulesDossier(moduleDossierUpdateDto,idDossier);

        if (f == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(idDossier, HttpStatus.OK);
    }

    @Transactional
    @PutMapping("/deleteDossier/{idDossier}")
    public ResponseEntity deleteDossier(@PathVariable int idDossier){

        boolean isRemoved = collaborateurService.deleteDossier(idDossier);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(idDossier, HttpStatus.OK);
    }

    @GetMapping("/getFilesDossier/{idDossier}")
    public ResponseEntity<List<GetAllDocumentsDto>> getFilesDossier(@PathVariable int idDossier){
        return ResponseEntity.ok(collaborateurService.getFilesDossier(idDossier));
    }

    @GetMapping("/getBytesFilesDossier/{nomFile}")
    public ResponseEntity<Resource> getBytesFilesDossier(@PathVariable String nomFile) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return ResponseEntity.ok()
                .headers(headers)
                .body(collaborateurService.getBytesFilesDossier(nomFile));
    }

    @RequestMapping(value = "/envoiMail", method = RequestMethod.PUT)
    public String envoiMail(@RequestBody SendMailDto sendMailDto) throws MessagingException {
        collaborateurService.envoiMail(sendMailDto);
        return "Email sent successfully";
    }

    @Transactional
    @DeleteMapping("/verifFichiers")
    public ResponseEntity<String> verifFichiers(){
        return ResponseEntity.ok(collaborateurService.verifFichiers());
    }


    @GetMapping("/employee")
    public ResponseEntity<List<EmployeDto>> getAllEmployees() {
        return ResponseEntity.ok(collaborateurService.getAllEmployees());
    }

    @PostMapping("/employee")
    public ResponseEntity<EmployeDto> createEmployee(@RequestBody EmployePostDto employePostDto) {
        return ResponseEntity.ok(collaborateurService.createEmployee(employePostDto));
    }

    @DeleteMapping("/employee")
    public ResponseEntity<EmployeDto> deleteEmployee(@RequestParam String id) {
        return ResponseEntity.ok(collaborateurService.deleteEmployee(id));
    }


}
