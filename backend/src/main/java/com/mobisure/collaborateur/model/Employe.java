package com.mobisure.collaborateur.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "employe")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Employe {
    @Id
    int idPersonne;
    LocalDateTime dateEmbauche;
    String matricule;
}
