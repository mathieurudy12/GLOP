package com.mobisure.collaborateur.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "professionnel")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Professionnel {
    @Id
    int idPersonne;
    String mailProfessionnel;
    String mdpProfessionnel;
    int droitsAcces;
}
