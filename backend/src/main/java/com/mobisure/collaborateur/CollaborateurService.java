package com.mobisure.collaborateur;

import com.mobisure.assurance.AssuranceService;
import com.mobisure.assurance.model.*;
import com.mobisure.assurance.repositories.*;
import com.mobisure.collaborateur.dto.*;
import com.mobisure.collaborateur.model.Employe;
import com.mobisure.collaborateur.model.Professionnel;
import com.mobisure.collaborateur.repositories.EmployeRepository;
import com.mobisure.collaborateur.repositories.ProfessionnelRepository;
import com.mobisure.exception.ApplicationException;
import com.mobisure.profile.model.Majeur;
import com.mobisure.profile.model.Personne;
import com.mobisure.profile.repositories.MajeurRepository;
import com.mobisure.profile.repositories.PersonneRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class CollaborateurService {

    ProfessionnelRepository professionnelRepository;
    PersonneRepository personneRepository;
    SouscrireRepository souscrireRepository;
    MajeurRepository majeurRepository;
    FormuleVoyageRepository formuleVoyageRepository;
    FormuleVehiculeRepository formuleVehiculeRepository;
    FormuleRepository formuleRepository;
    VehiculeRepository vehiculeRepository;
    AssocierRepository associerRepository;
    ModeStationnementRepository modeStationnementRepository;
    UsageTrajetRepository usageTrajetRepository;
    ComposerRepository composerRepository;
    ModuleFormuleRepository moduleFormuleRepository;
    AssuranceService assuranceService;
    DocumentsJustificatifsRepository documentsJustificatifsRepository;
    ApparaitreRepository apparaitreRepository;
    EmployeRepository employeRepository;
    PasswordEncoder passwordEncoder;

    /**
     * Méthode qui permet de récupérer les informations d'un admin
     *
     * @param idPersonne
     * @return
     */
    public HashMap<String, String> getInfosAdmin(int idPersonne) {
        if (!professionnelRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Habiter !");
        }
        HashMap<String, String> infosPersonne = new HashMap<>();
        Personne personne = personneRepository.findByIdPersonne(idPersonne);
        Professionnel professionnel = professionnelRepository.findByIdPersonne(idPersonne);
        infosPersonne.put("nom", personne.getNom());
        infosPersonne.put("prenom", personne.getPrenom());
        infosPersonne.put("mail", professionnel.getMailProfessionnel());
        if (personne.getDateNaissance() != null) {
            infosPersonne.put("date_naissance", personne.getDateNaissance().toLocalDate().toString());
        }
        return infosPersonne;
    }

    /**
     * Méthode qui permet de récupérer tous les clients
     *
     * @return
     */
    public List<GetAllClientsDto> getAllClients() {
        List<Majeur> souscrits = majeurRepository.findAll();
        List<GetAllClientsDto> allClients = new ArrayList<>();
        for (Majeur m : souscrits) {
            Personne p = personneRepository.findByIdPersonne(m.getIdPersonne());
            GetAllClientsDto client = GetAllClientsDto.builder()
                    .dateNaissance(p.getDateNaissance())
                    .idPersonne(p.getIdPersonne())
                    .mail(m.getMailAssure())
                    .nom(p.getNom())
                    .prenom(p.getPrenom())
                    .telephone(m.getTelephoneAssure())
                    .build();
            allClients.add(client);
        }
        return allClients;
    }

    /**
     * Méthode qui permet de supprimer un assuré
     *
     * @param idPersonne
     * @return
     * @throws IOException
     */
    public Boolean deleteClient(int idPersonne) throws IOException {
        final String PATH = "src/main/resources/";
        String directoryName = PATH.concat(idPersonne + "/");
        File directory = new File(directoryName);
        FileUtils.deleteDirectory(directory);
        personneRepository.deleteByIdPersonne(idPersonne);
        return true;
    }

    /**
     * Méthode pour récupérer tous contrats voyages
     *
     * @return
     */
    public List<GetAllFormuleVoyageDto> getAllFormuleVoyage() {
        List<FormuleVoyage> allFormules = formuleVoyageRepository.findAll();
        List<GetAllFormuleVoyageDto> allFormulesVoyage = new ArrayList<>();
        for (FormuleVoyage fV : allFormules) {
            Souscrire s = souscrireRepository.findByIdFormule(fV.getIdFormule());
            Personne p = personneRepository.findByIdPersonne(s.getIdPersonne());
            Majeur m = majeurRepository.findByIdPersonne(s.getIdPersonne());
            Formule f = formuleRepository.findByIdFormule(fV.getIdFormule());
            List<Apparaitre> apparaitres = apparaitreRepository.findAllByIdFormule(f.getIdFormule());
            GetAllFormuleVoyageDto formule = GetAllFormuleVoyageDto.builder()
                    .idFormule(f.getIdFormule())
                    .prenomPersonne(p.getPrenom())
                    .nomPersonne(p.getNom())
                    .nomFormule(f.getNomFormule())
                    .dateDebut(fV.getDateDepartVoyage().toString())
                    .dateFin(fV.getDateRetourVoyage().toString())
                    .dateSouscription(s.getDateSouscription().toString())
                    .destination(fV.getDestinationVoyage())
                    .nbVoyageurs(1+apparaitres.size())
                    .prix(f.getPrixFormule())
                    .status(f.getStatus())
                    .type(fV.getTypeSejour())
                    .mail(m.getMailAssure())
                    .telephone(m.getTelephoneAssure())
                    .build();
            allFormulesVoyage.add(formule);
        }
        return allFormulesVoyage;
    }

    /**
     * Méthode pour récupérer tous les contrats véhicules
     *
     * @return
     */
    public List<GetAllFormuleVehiculeDto> getAllFormuleVehicule() {
        List<FormuleVehicule> allFormules = formuleVehiculeRepository.findAll();
        List<GetAllFormuleVehiculeDto> allFormulesVehicule = new ArrayList<>();
        for (FormuleVehicule fV : allFormules) {
            Souscrire s = souscrireRepository.findByIdFormule(fV.getIdFormule());
            Personne p = personneRepository.findByIdPersonne(s.getIdPersonne());
            Majeur m = majeurRepository.findByIdPersonne(s.getIdPersonne());
            Formule f = formuleRepository.findByIdFormule(fV.getIdFormule());
            Associer a = associerRepository.findByIdFormule(fV.getIdFormule());
            Vehicule v = vehiculeRepository.findVehiculeByImmatriculation(a.getImmatriculation());
            ModeStationnement modeStationnement = modeStationnementRepository.findByIdMode(fV.getIdMode());
            UsageTrajet usageTrajet = usageTrajetRepository.findByIdUsage(fV.getIdUsage());
            GetAllFormuleVehiculeDto formule = GetAllFormuleVehiculeDto.builder()
                    .idFormule(f.getIdFormule())
                    .immatriculation(v.getImmatriculation())
                    .marque(v.getMarque())
                    .modele(v.getModele())
                    .dateSouscription(s.getDateSouscription().toString())
                    .mail(m.getMailAssure())
                    .nomMode(modeStationnement.getNomMode())
                    .nomUsage(usageTrajet.getNomUsage())
                    .nomFormule(f.getNomFormule())
                    .nom(p.getNom())
                    .zoneGeographique(fV.getZoneGeographique())
                    .prenom(p.getPrenom())
                    .prix(f.getPrixFormule())
                    .status(f.getStatus())
                    .telephone(m.getTelephoneAssure())
                    .build();
            allFormulesVehicule.add(formule);
        }
        return allFormulesVehicule;
    }

    /**
     * Méthode qui permet de récupérer un contrat en particulier
     *
     * @param idDossier
     * @return
     */
    public GetDossierDto getDossier(int idDossier) {
        Formule f = formuleRepository.findByIdFormule(idDossier);
        Souscrire s = souscrireRepository.findByIdFormule(idDossier);
        Personne p = personneRepository.findByIdPersonne(s.getIdPersonne());
        Majeur m = majeurRepository.findByIdPersonne(s.getIdPersonne());
        GetDossierDto getDossierDto = GetDossierDto.builder()
                .intitule(f.getNomFormule())
                .prix(f.getPrixFormule())
                .statut(f.getStatus())
                .prenom(p.getPrenom())
                .nom(p.getNom())
                .mail(m.getMailAssure())
                .telephone(m.getTelephoneAssure())
                .dateSouscription(s.getDateSouscription().toString())
                .build();
        return getDossierDto;
    }

    /**
     * Méthode qui permet de mettre à jour un contrat
     *
     * @param dossierUpdateDto
     * @param idDossier
     * @return
     */
    public Formule updateDossier(DossierUpdateDto dossierUpdateDto, int idDossier) {
        Formule f = formuleRepository.findByIdFormule(idDossier);
        f.setStatus(dossierUpdateDto.getStatus());
        return formuleRepository.save(f);
    }

    /**
     * Méthode qui permet de modifier les modules d'un contrat
     *
     * @param moduleDossierUpdateDto
     * @param idDossier
     * @return
     */
    @Transactional
    public Formule updateModulesDossier(ModuleDossierUpdateDto moduleDossierUpdateDto, int idDossier) {
        Formule f = formuleRepository.findByIdFormule(idDossier);
        ModuleFormule moduleFormule = moduleFormuleRepository.findByNomModuleFormuleIgnoreCase(moduleDossierUpdateDto.getNomModule());
        if(moduleDossierUpdateDto.getEtat().equals("supprimer")){
            composerRepository.deleteByIdModule(moduleFormule.getIdModuleFormule());
        }
        else{
            assuranceService.createAndSaveComposer(f.getIdFormule(), moduleFormule.getIdModuleFormule());
        }
        return f;
    }

    /**
     * Méthode qui permet de mettre à jour le prix d'un contrat
     *
     * @param prix
     * @param statut
     * @param idDossier
     * @return
     */
    public Formule updatePrixFormule(float prix, String statut, int idDossier){
        Formule f = formuleRepository.findByIdFormule(idDossier);
        f.setPrixFormule(prix);
        f.setStatus(statut);
        formuleRepository.save(f);
        return f;
    }

    /**
     * Méthode qui permet de récupérer les modules d'une formule
     *
     * @param idFormule
     * @return
     */
    public List<ModuleFormule> getModulesFormule(int idFormule) {
        List<ModuleFormule> modules = moduleFormuleRepository.findByTarifModuleFormule(1);
        List<Composer> composers = composerRepository.findByIdFormule(idFormule);
        for(Composer composer : composers){
            modules.add(moduleFormuleRepository.findByIdModuleFormule(composer.getIdModule()));
        }
        return modules;
    }

    /**
     * Méthode qui permet de supprimer un contrat
     *
     * @param idDossier
     * @return
     */
    public boolean deleteDossier(int idDossier) {
        Souscrire s = souscrireRepository.findByIdFormule(idDossier);
        String idPerson = String.valueOf(s.getIdPersonne());
        final String PATH = "src/main/resources/";
        String directoryName = PATH.concat(idPerson + "/");
        File directory = new File(directoryName);
        File[] files = directory.listFiles();
        for(File f : files){
            if(f.getName().contains(String.valueOf(idDossier))){
                boolean hasDelete = f.delete();
                if (!hasDelete) {
                    throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Bad delete of files");
                }
            }
        }
        formuleRepository.deleteByIdFormule(idDossier);
        return true;
    }

    /**
     * Méthode qui permet de récupérer tous les fichiers d'un contrat
     *
     * @param idDossier
     * @return
     */
    public List<GetAllDocumentsDto> getFilesDossier(int idDossier) {
        List<DocumentsJustificatifs> docsJ = documentsJustificatifsRepository.getDocumentsJustificatifsByIdFormule(idDossier);
        List<GetAllDocumentsDto> docs = new ArrayList<GetAllDocumentsDto>();
        for(DocumentsJustificatifs doc : docsJ){
            GetAllDocumentsDto getAllDocumentsDto = GetAllDocumentsDto.builder()
                    .idDocument(doc.getIdDocument())
                    .nbDocument(formuleRepository.findByIdFormule(doc.getIdFormule()).getNbFichiers())
                    .nomDocument(doc.getNomDocument())
                    .url_document(doc.getUrl_document())
                    .idFormule(doc.getIdFormule())
                    .idPersonne(doc.getIdPersonne())
                    .mailPersonne(majeurRepository.findByIdPersonne(doc.getIdPersonne()).getMailAssure())
                    .build();
            docs.add(getAllDocumentsDto);
        }
        return docs;
    }

    /**
     * Méthode qui permet de récupérer les données d'un fichier
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public Resource getBytesFilesDossier(String filename) throws IOException {
        List<Resource> bytesFiles = new ArrayList<Resource>();
        ByteArrayResource resource = null;
        String[] nomDecoupe = filename.split("_");
        int idDossier = Integer.parseInt(nomDecoupe[1]);
        Souscrire s = souscrireRepository.findByIdFormule(idDossier);
        String idPerson = String.valueOf(s.getIdPersonne());
        final String PATH = "src/main/resources/";
        String directoryName = PATH.concat(idPerson + "/");
        File directory = new File(directoryName);
        File[] files = directory.listFiles();
        for(File f : files){
            if(f.getName().contains(filename)){
                Path path = Paths.get(f.getAbsolutePath());
                resource = new ByteArrayResource(Files.readAllBytes(path));
                bytesFiles.add(resource);
            }
        }
        return resource;
    }

    /**
     * Méthode qui permet d'envoyer un mail
     *
     * @param sendMailDto
     * @throws MessagingException
     */
    public void envoiMail(SendMailDto sendMailDto) throws MessagingException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("mobisure.glop@gmail.com", "test");
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("mobisure.glop@gmail.com", false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(sendMailDto.getMail()));
        msg.setSubject(sendMailDto.getObjet());
        msg.setContent(sendMailDto.getMessage(), "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(sendMailDto.getMessage(), "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        msg.setContent(multipart);
        Transport.send(msg);
    }

    /**
     * Méthode qui permet de supprimer les fichiers non utilisés
     *
     * @return
     */
    public String verifFichiers() {
        final String PATH = "src/main/resources/";
        File directory = new File(PATH);
        File[] dirs = directory.listFiles();
        for(File f : dirs){
            if(f.isDirectory()){
                File[] files = f.listFiles();
                for(File file : files){
                    if(!file.getName().contains("_")){
                        documentsJustificatifsRepository.deleteByIdDocument(documentsJustificatifsRepository.getDocumentsJustificatifsByNomDocument(file.getName().split("\\.")[0]).getIdDocument());
                        boolean hasDelete = file.delete();
                        if (!hasDelete) {
                            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Bad delete of files");
                        }
                    }
                }
            }
        }
        return "ok";
    }


    /**
     * Méthode qui permet de récupérer tous les employés
     *
     * @return
     */
    public List<EmployeDto> getAllEmployees() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        List<Employe> employes = employeRepository.findAll();
        List<EmployeDto> employeDtos = new ArrayList<>();
        employes.forEach(employe -> {
            Personne personne = personneRepository.findByIdPersonne(employe.getIdPersonne());
            Professionnel professionnel = professionnelRepository.findByIdPersonne(employe.getIdPersonne());
            EmployeDto employeDto = EmployeDto.builder()
                    .dateEmbauche(employe.getDateEmbauche().format(formatter))
                    .matricule(employe.getMatricule())
                    .nom(personne.getNom())
                    .prenom(personne.getPrenom())
                    .idPersonne(personne.getIdPersonne())
                    .mail(professionnel.getMailProfessionnel())
                    .build();
            employeDtos.add(employeDto);
        });
        return employeDtos;
    }

    /**
     * Méthode qui permet de créer un nouvel employé
     *
     * @param employePostDto
     * @return
     */
    public EmployeDto createEmployee(EmployePostDto employePostDto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime dateEmbauche = LocalDate.parse(employePostDto.getDateEmbauche(), formatter).atStartOfDay();
        Personne personne = Personne.builder()
                .prenom(employePostDto.getPrenom())
                .nom(employePostDto.getNom())
                .build();

        personneRepository.save(personne);

        Professionnel professionnel = Professionnel.builder()
                .mailProfessionnel(employePostDto.getMail())
                .mdpProfessionnel(passwordEncoder.encode(employePostDto.getMdp()))
                .droitsAcces(1)
                .idPersonne(personne.getIdPersonne())
                .build();

        professionnelRepository.save(professionnel);

        Employe employe = Employe.builder()
                .dateEmbauche(dateEmbauche)
                .matricule(employePostDto.getMatricule())
                .idPersonne(personne.getIdPersonne())
                .build();

        employeRepository.save(employe);

        return EmployeDto.builder()
                .mail(professionnel.getMailProfessionnel())
                .prenom(personne.getPrenom())
                .nom(personne.getNom())
                .matricule(employe.getMatricule())
                .mail(professionnel.getMailProfessionnel())
                .dateEmbauche(employePostDto.getDateEmbauche())
                .build();
    }

    /**
     * Méthode qui permet de supprimer un employé
     *
     * @param id
     * @return
     */
    public EmployeDto deleteEmployee(String id) {
        Employe employe = employeRepository.findByIdPersonne(Integer.parseInt(id))
                .orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND,"Employe not found"));
        employeRepository.delete(employe);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return EmployeDto.builder()
                .matricule(employe.getMatricule())
                .dateEmbauche(employe.getDateEmbauche().format(formatter))
                .build();
    }

}
