package com.mobisure.collaborateur.repositories;

import com.mobisure.collaborateur.model.Professionnel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProfessionnelRepository extends JpaRepository<Professionnel, Integer> {
    Professionnel findByMailProfessionnel(String mail);
    Professionnel findByIdPersonne(int idPersonne);
    boolean existsByIdPersonne(int idPersonne);
}
