package com.mobisure.collaborateur.repositories;

import com.mobisure.collaborateur.model.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeRepository extends JpaRepository<Employe,Integer> {
    List<Employe> findAll();
    Optional<Employe> findByIdPersonne(int idPersonne);
}
