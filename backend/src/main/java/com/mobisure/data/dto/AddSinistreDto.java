package com.mobisure.data.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter un sinistre
 */
@Data
public class AddSinistreDto {
    int idAssistance;
    String etat;
}
