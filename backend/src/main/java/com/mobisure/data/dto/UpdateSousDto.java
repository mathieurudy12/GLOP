package com.mobisure.data.dto;

import lombok.Data;

/**
 * Classe qui permet de mettre à jour une souscription
 */
@Data
public class UpdateSousDto {
    int idFormule;
    int nbModules;
    float tarif;
}
