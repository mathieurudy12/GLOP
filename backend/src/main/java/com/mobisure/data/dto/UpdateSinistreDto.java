package com.mobisure.data.dto;

import lombok.Data;

/**
 * Classe qui permet de mettre à jour un sinistre
 */
@Data
public class UpdateSinistreDto {
    int idAssistance;
    int dureeResolution;
    float tarif;
}
