package com.mobisure.data.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter une souscription
 */
@Data
public class AddSousDto {
    int idFormule;
    String statut;
}
