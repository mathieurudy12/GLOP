package com.mobisure.data;

import com.mobisure.assistance.model.*;
import com.mobisure.assistance.repositories.*;
import com.mobisure.assurance.model.*;
import com.mobisure.assurance.repositories.*;
import com.mobisure.data.dto.*;
import com.mobisure.data.model.*;
import com.mobisure.data.repositories.*;
import com.mobisure.partenaire.model.Partenaire;
import com.mobisure.partenaire.repositories.PartenaireRepository;
import com.mobisure.profile.model.*;
import com.mobisure.profile.repositories.*;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.csv.*;
import org.springframework.core.io.*;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class DataService {

    SouscriptionVoyageRepository souscriptionVoyageRepository;
    SouscriptionVehiculeRepository souscriptionVehiculeRepository;
    SinistreRepository sinistreRepository;
    AssocierRepository associerRepository;
    ComposerRepository composerRepository;
    DetenirRepository detenirRepository;
    FormuleRepository formuleRepository;
    FormuleVehiculeRepository formuleVehiculeRepository;
    FormuleVoyageRepository formuleVoyageRepository;
    ModeStationnementRepository modeStationnementRepository;
    PermisRepository permisRepository;
    SouscrireRepository souscrireRepository;
    TypeAssuranceVoyageRepository typeAssuranceVoyageRepository;
    UsageTrajetRepository usageTrajetRepository;
    VehiculeRepository vehiculeRepository;
    PersonneRepository personneRepository;
    PaysRepository paysRepository;
    AdresseRepository adresseRepository;
    HabiterRepository habiterRepository;
    AssistanceRepository assistanceRepository;
    TypeAssistanceRepository typeAssistanceRepository;
    EtatAssistanceRepository etatAssistanceRepository;
    PartenaireRepository partenaireRepository;
    MajeurRepository majeurRepository;
    DisposerRepository disposerRepository;

    /**
     * Méthode qui permet d'exporter les données de l'entrepôt de données en fichier CSV
     *
     * @param souscriptionVoyages
     * @param souscriptionVehicules
     * @return
     */
    public ResponseEntity<Resource> exportCSV(List<SouscriptionVoyage> souscriptionVoyages, List<SouscriptionVehicule> souscriptionVehicules) {
        List<List<String>> csvBody = new ArrayList<>();
        String[] csvHeader;
        String csvFileName = "";
        if(souscriptionVehicules == null){
            csvHeader = new String[]{
                    "idSouscription","idClient","dateNaissance","villeCPAdresse","nomPays","destinationVoyage","dureeVoyage","dateDebutVoyage","typeSejour","statutSouscription","coutVoyage","coutSouscription", "dateSouscription", "nomType", "categPro", "situFamille"
            };
            for(SouscriptionVoyage souscriptionVoyage : souscriptionVoyages){
                csvBody.add(Arrays.asList(String.valueOf(souscriptionVoyage.getIdSouscription()), String.valueOf(souscriptionVoyage.getIdClient()), souscriptionVoyage.getDateNaissance().toString(), souscriptionVoyage.getVilleCpAdresse(), souscriptionVoyage.getNomPays(), souscriptionVoyage.getDestinationVoyage(), String.valueOf(souscriptionVoyage.getDureeVoyage()), souscriptionVoyage.getDateDebutVoyage().toString(), souscriptionVoyage.getTypeSejour(), souscriptionVoyage.getStatutSouscription(), String.valueOf(souscriptionVoyage.getCoutSouscription()), String.valueOf(souscriptionVoyage.getCoutVoyage()), souscriptionVoyage.getDateSouscription().toString(), souscriptionVoyage.getNomType(), "test", "test"));
            }
            csvFileName = "stats_voyage.csv";
        }
        else{
            csvHeader = new String[]{
                    "idSouscription","idClient","dateNaissance","villeCPAdresse","nomPays","typePermis","bonus_malus","marque","kilometrageVoiture","modeleVoiture","carburantVoiture","date_mise_en_service","carrosserie","puissanceFiscale","zoneGeographique","usageTrajet","modeStationnement","exclusif?","dateSouscription","coutSouscription","nbModule","statutSouscription", "categPro", "situFamille"
            };
            for(SouscriptionVehicule souscriptionVehicule : souscriptionVehicules){
                csvBody.add(Arrays.asList(String.valueOf(souscriptionVehicule.getIdSouscription()), String.valueOf(souscriptionVehicule.getIdClient()), souscriptionVehicule.getDateNaissance().toString(), souscriptionVehicule.getVilleCpAdresse(), souscriptionVehicule.getNomPays(), souscriptionVehicule.getTypePermis(), String.valueOf(souscriptionVehicule.getBonusMalus()), souscriptionVehicule.getMarqueVoiture(), String.valueOf(souscriptionVehicule.getKilometrageVoiture()), souscriptionVehicule.getModeleVoiture(), souscriptionVehicule.getCarburantVoiture(), souscriptionVehicule.getDateMiseEnService().toString(), souscriptionVehicule.getCarosserieVoiture(), String.valueOf(souscriptionVehicule.getPuissanceFiscale()), souscriptionVehicule.getZoneGeographique(), souscriptionVehicule.getUsageTrajet(), souscriptionVehicule.getModeStationnement(), String.valueOf(souscriptionVehicule.getExclusif_()), souscriptionVehicule.getDateSouscription().toString(), String.valueOf(souscriptionVehicule.getCoutSouscription()), String.valueOf(souscriptionVehicule.getNbModules()), souscriptionVehicule.getStatutSouscription(), "test", "test"));
            }
            csvFileName = "stats_voiture.csv";
        }

        ByteArrayInputStream byteArrayOutputStream;

        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                // defining the CSV printer
                CSVPrinter csvPrinter = new CSVPrinter(
                        new PrintWriter(out),
                        // withHeader is optional
                        CSVFormat.DEFAULT.withHeader(csvHeader)
                );
        )
        {
            // populating the CSV content
            for (List<String> record : csvBody)
                csvPrinter.printRecord(record);

            // writing the underlying stream
            csvPrinter.flush();

            byteArrayOutputStream = new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        InputStreamResource fileInputStream = new InputStreamResource(byteArrayOutputStream);

        // setting HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + csvFileName);
        // defining the custom Content-Type
        headers.set(HttpHeaders.CONTENT_TYPE, "text/csv");

        return new ResponseEntity<>(
                fileInputStream,
                headers,
                HttpStatus.OK
        );
    }

    /**
     * Méthode qui permet de récupérer toutes les souscriptions voyages
     *
     * @return
     */
    public ResponseEntity<Resource> getAllSouscriptionsVoyages() {
        List<SouscriptionVoyage> souscriptionVoyages = souscriptionVoyageRepository.findAll();
        return exportCSV(souscriptionVoyages, null);
    }

    /**
     * Méthode qui permet de récupérer toutes les souscriptions véhicules
     *
     * @return
     */
    public ResponseEntity<Resource> getAllSouscriptionsVoitures() {
        List<SouscriptionVehicule> souscriptionVehicules = souscriptionVehiculeRepository.findAll();
        return exportCSV(null, souscriptionVehicules);
    }

    /**
     * Méthode qui permet d'ajouter une souscription
     *
     * @param addSousDto
     * @return
     */
    public int addSous(AddSousDto addSousDto) {
        Formule formule = formuleRepository.findByIdFormule(addSousDto.getIdFormule());
        FormuleVoyage formuleVoyage = formuleVoyageRepository.findByIdFormule(addSousDto.getIdFormule());
        FormuleVehicule formuleVehicule = formuleVehiculeRepository.findByIdFormule(addSousDto.getIdFormule());
        Souscrire souscrire = souscrireRepository.findByIdFormule(formule.getIdFormule());
        Personne personne = personneRepository.findByIdPersonne(souscrire.getIdPersonne());
        Associer associer = associerRepository.findByIdFormule(formule.getIdFormule());
        Habiter habiter = habiterRepository.findByIdPersonne(personne.getIdPersonne());
        Adresse adresse = adresseRepository.findByIdAdresse(habiter.getIdAdresse());
        Pays pays = paysRepository.findByCodeIso(adresse.getCodeIso()).get();
        if(formuleVehicule != null){
            Detenir detenir = detenirRepository.findByIdPersonne(personne.getIdPersonne());
            Permis permis = permisRepository.findByNumeroPermis(detenir.getNumeroPermis());
            Vehicule vehicule = vehiculeRepository.findVehiculeByImmatriculation(associer.getImmatriculation());
            UsageTrajet usageTrajet = usageTrajetRepository.findByIdUsage(formuleVehicule.getIdUsage());
            ModeStationnement modeStationnement = modeStationnementRepository.findByIdMode(formuleVehicule.getIdMode());
            List<Composer> composers = composerRepository.findByIdFormule(formule.getIdFormule());
            int nbModules = 0;
            for(Composer c : composers){
                nbModules++;
            }
            SouscriptionVehicule souscriptionVehicule = SouscriptionVehicule.builder()
                    .bonusMalus(permis.getBonusMalus())
                    .dateSouscription(souscrire.getDateSouscription())
                    .carburantVoiture(vehicule.getCarburant())
                    .coutSouscription(formule.getPrixFormule())
                    .carosserieVoiture(vehicule.getCarosserie())
                    .dateMiseEnService(vehicule.getDateMiseEnService())
                    .dateNaissance(personne.getDateNaissance())
                    .exclusif_(formuleVehicule.isExclusif_())
                    .kilometrageVoiture(vehicule.getKilometrageEnCours())
                    .marqueVoiture(vehicule.getMarque())
                    .modeleVoiture(vehicule.getModele())
                    .dateSouscription(souscrire.getDateSouscription())
                    .usageTrajet(usageTrajet.getNomUsage())
                    .modeStationnement(modeStationnement.getNomMode())
                    .statutSouscription(addSousDto.getStatut())
                    .nomPays(pays.getNomPays())
                    .villeCpAdresse(adresse.getVilleCpAdresse())
                    .zoneGeographique(formuleVehicule.getZoneGeographique())
                    .idFormule(formule.getIdFormule())
                    .puissanceFiscale(vehicule.getPuissanceFiscale())
                    .typePermis(permis.getTypePermis())
                    .nbModules(nbModules)
                    .idClient(souscrire.getIdPersonne())
                    .categPro("test")
                    .situFamille("test")
                    .build();
            souscriptionVehiculeRepository.save(souscriptionVehicule);
        }
        else{
            long duree = DAYS.between(formuleVoyage.getDateDepartVoyage(),formuleVoyage.getDateRetourVoyage());
            TypeAssuranceVoyage typeAssuranceVoyage = typeAssuranceVoyageRepository.findByIdType(formuleVoyage.getIdType());
            SouscriptionVoyage souscriptionVoyage = SouscriptionVoyage.builder()
                    .coutSouscription(formule.getPrixFormule())
                    .coutVoyage(formuleVoyage.getCoutVoyage())
                    .dateDebutVoyage(formuleVoyage.getDateDepartVoyage())
                    .dateSouscription(souscrire.getDateSouscription())
                    .dureeVoyage((int)duree)
                    .dateNaissance(personne.getDateNaissance())
                    .destinationVoyage(formuleVoyage.getDestinationVoyage())
                    .statutSouscription(addSousDto.getStatut())
                    .nomType(typeAssuranceVoyage.getNomType())
                    .typeSejour(formuleVoyage.getTypeSejour())
                    .nomPays(pays.getNomPays())
                    .villeCpAdresse(adresse.getVilleCpAdresse())
                    .idFormule(formule.getIdFormule())
                    .idClient(souscrire.getIdFormule())
                    .categPro("test")
                    .situFamille("test")
                    .build();
            souscriptionVoyageRepository.save(souscriptionVoyage);
        }
        return formule.getIdFormule();
    }

    /**
     * Méthode qui permet de mettre à jour une souscription
     *
     * @param updateSousDto
     * @return
     */
    public int updateSous(UpdateSousDto updateSousDto) {
        Formule formule = formuleRepository.findByIdFormule(updateSousDto.getIdFormule());
        FormuleVoyage formuleVoyage = formuleVoyageRepository.findByIdFormule(updateSousDto.getIdFormule());
        FormuleVehicule formuleVehicule = formuleVehiculeRepository.findByIdFormule(updateSousDto.getIdFormule());
        if(formuleVehicule != null){
            SouscriptionVehicule souscriptionVehicule = souscriptionVehiculeRepository.findByIdFormule(formule.getIdFormule());
            int nbModules = 0;
            float tarif = 0;
            if(updateSousDto.getNbModules() != 0){
                nbModules = souscriptionVehicule.getNbModules() + updateSousDto.getNbModules();
                tarif = updateSousDto.getTarif();
            }
            else{
                List<Composer> composers = composerRepository.findByIdFormule(formule.getIdFormule());
                for(Composer c : composers){
                    nbModules++;
                }
                tarif = formule.getPrixFormule();
            }
            souscriptionVehicule.setStatutSouscription("ENDED");
            souscriptionVehicule.setNbModules(nbModules);
            souscriptionVehicule.setCoutSouscription(tarif);
            souscriptionVehiculeRepository.save(souscriptionVehicule);
        }
        else{
            SouscriptionVoyage souscriptionVoyage = souscriptionVoyageRepository.findByIdFormule(formule.getIdFormule());
            souscriptionVoyage.setStatutSouscription("ENDED");
            souscriptionVoyage.setCoutSouscription(formule.getPrixFormule());
            souscriptionVoyageRepository.save(souscriptionVoyage);
        }
        return formule.getIdFormule();
    }

    /**
     * Méthode qui permet de récupérer tous les sinsitres
     *
     * @return
     */
    public ResponseEntity<Resource> getAllSinistres() {
        List<Sinistre> sinistres = sinistreRepository.findAll();
        return exportCSVSinistre(sinistres);
    }

    /**
     * Méthode qui permet d'exporter les sinistres en csv
     *
     * @param sinistres
     * @return
     */
    private ResponseEntity<Resource> exportCSVSinistre(List<Sinistre> sinistres) {
        // replace this with your data retrieving logic
        List<List<String>> csvBody = new ArrayList<>();
        String[] csvHeader;
        String csvFileName = "";
        csvHeader = new String[]{
                "idSinistre","idClient","idPartenaire","libelleTypeAssistance","nomPartenaire","nomPays","typeSinistre","etatAssistance","tarif","dateSinistre","dureeResolution","idAssistance","categPro","situFamille"
        };
        for(Sinistre sinistre : sinistres){
            csvBody.add(Arrays.asList(String.valueOf(sinistre.getIdSinistre()), String.valueOf(sinistre.getIdClient()), String.valueOf(sinistre.getIdPartenaire()), sinistre.getLibelleTypeAssistance(), sinistre.getNomPartenaire(), sinistre.getNomPays(), sinistre.getTypeSinistre(), sinistre.getEtatAssistance(), String.valueOf(sinistre.getTarifSinistre()), sinistre.getDateSinistre().toString(), String.valueOf(sinistre.getDureeResolution()), String.valueOf(sinistre.getIdAssistance()), "test", "test"));
        }
        csvFileName = "stats_sinistres.csv";

        ByteArrayInputStream byteArrayOutputStream;

        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                // defining the CSV printer
                CSVPrinter csvPrinter = new CSVPrinter(
                        new PrintWriter(out),
                        // withHeader is optional
                        CSVFormat.DEFAULT.withHeader(csvHeader)
                );
        )
        {
            // populating the CSV content
            for (List<String> record : csvBody)
                csvPrinter.printRecord(record);

            // writing the underlying stream
            csvPrinter.flush();

            byteArrayOutputStream = new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        InputStreamResource fileInputStream = new InputStreamResource(byteArrayOutputStream);

        // setting HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + csvFileName);
        // defining the custom Content-Type
        headers.set(HttpHeaders.CONTENT_TYPE, "text/csv");

        return new ResponseEntity<>(
                fileInputStream,
                headers,
                HttpStatus.OK
        );
    }

    /**
     * Méthode qui permet de créer un sinistre
     *
     * @param addSinistreDto
     * @return
     */
    public int addSinistre(AddSinistreDto addSinistreDto) {
        Assistance assistance = assistanceRepository.findByIdAssistance(addSinistreDto.getIdAssistance());
        Majeur majeur = majeurRepository.findByIdPersonne(assistance.getIdPersonneAssure());
        EtatAssistance etatAssistance = etatAssistanceRepository.findByNomEtat(addSinistreDto.getEtat());
        Disposer disposer = disposerRepository.findByIdAssistanceAndIdEtat(assistance.getIdAssistance(), etatAssistance.getIdEtat());
        TypeAssistance typeAssistance = typeAssistanceRepository.findByIdTypeAssistance(assistance.getIdTypeAssistance());
        Pays pays = paysRepository.findByCodeIso(assistance.getCodeIso()).get();
        Partenaire partenaire = partenaireRepository.findByIdPartenaire(assistance.getIdPartenaire()).get();
        Sinistre sinistre = Sinistre.builder()
                .idAssistance(addSinistreDto.getIdAssistance())
                .idClient(majeur.getIdPersonne())
                .etatAssistance(addSinistreDto.getEtat())
                .tarifSinistre(assistance.getTarif())
                .dateSinistre(disposer.getDateEtat())
                .idPartenaire(assistance.getIdPartenaire())
                .libelleTypeAssistance(typeAssistance.getLibelleTypeAssistance())
                .nomPays(pays.getNomPays())
                .nomPartenaire(partenaire.getNomPartenaire())
                .typeSinistre(assistance.getTypeSinistre())
                .dureeResolution(0)
                .categPro("test")
                .situFamille("test")
                .build();
        sinistreRepository.save(sinistre);
        return assistance.getIdAssistance();
    }

    /**
     * Méthode qui permet de mettre à jour un sinsitre
     *
     * @param updateSinistreDto
     * @return
     */
    public int updateSinistre(UpdateSinistreDto updateSinistreDto) {
        Sinistre sinistre = sinistreRepository.findByIdAssistance(updateSinistreDto.getIdAssistance());
        sinistre.setDureeResolution(updateSinistreDto.getDureeResolution());
        sinistre.setTarifSinistre(updateSinistreDto.getTarif());
        sinistre.setEtatAssistance("ENDED");
        sinistreRepository.save(sinistre);
        return sinistre.getIdAssistance();
    }
}
