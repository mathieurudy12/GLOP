package com.mobisure.data.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "sinistre")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Sinistre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idSinistre;
    float tarifSinistre;
    String libelleTypeAssistance;
    int idPartenaire;
    String nomPays;
    String nomPartenaire;
    int idClient;
    String typeSinistre;
    String etatAssistance;
    LocalDateTime dateSinistre;
    int dureeResolution;
    int idAssistance;
    String categPro;
    String situFamille;
}
