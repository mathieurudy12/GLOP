package com.mobisure.data.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "souscriptionVehicule")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SouscriptionVehicule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idSouscription;
    LocalDateTime dateSouscription;
    LocalDateTime dateNaissance;
    String marqueVoiture;
    String zoneGeographique;
    String usageTrajet;
    int nbModules;
    double bonusMalus;
    double kilometrageVoiture;
    String modeStationnement;
    String modeleVoiture;
    String carburantVoiture;
    String statutSouscription;
    double coutSouscription;
    String villeCpAdresse;
    String nomPays;
    String typePermis;
    Boolean exclusif_;
    LocalDateTime dateMiseEnService;
    String carosserieVoiture;
    int puissanceFiscale;
    int idFormule;
    int idClient;
    String categPro;
    String situFamille;
}
