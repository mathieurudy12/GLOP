package com.mobisure.data.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "souscriptionVoyage")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SouscriptionVoyage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idSouscription;
    String destinationVoyage;
    LocalDateTime dateSouscription;
    float coutVoyage;
    LocalDateTime dateNaissance;
    String villeCpAdresse;
    String nomPays;
    int dureeVoyage;
    String typeSejour;
    String statutSouscription;
    float coutSouscription;
    String nomType;
    LocalDateTime dateDebutVoyage;
    int idFormule;
    int idClient;
    String categPro;
    String situFamille;
}
