package com.mobisure.data.repositories;

import com.mobisure.data.model.Sinistre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SinistreRepository extends JpaRepository<Sinistre, Integer> {
    List<Sinistre> findAll();
    Sinistre findByIdAssistance(int idAssistance);
}
