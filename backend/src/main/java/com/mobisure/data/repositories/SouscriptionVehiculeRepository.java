package com.mobisure.data.repositories;

import com.mobisure.data.model.SouscriptionVehicule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SouscriptionVehiculeRepository extends JpaRepository<SouscriptionVehicule, Integer> {
    List<SouscriptionVehicule> findAll();
    SouscriptionVehicule findByIdFormule(int idFormule);
}
