package com.mobisure.data.repositories;

import com.mobisure.data.model.SouscriptionVoyage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SouscriptionVoyageRepository extends JpaRepository<SouscriptionVoyage, Integer>{
    List<SouscriptionVoyage> findAll();
    SouscriptionVoyage findByIdFormule(int idFormule);
}
