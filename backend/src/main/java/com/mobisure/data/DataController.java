package com.mobisure.data;

import com.mobisure.data.dto.AddSinistreDto;
import com.mobisure.data.dto.AddSousDto;
import com.mobisure.data.dto.UpdateSinistreDto;
import com.mobisure.data.dto.UpdateSousDto;
import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.DATA)
public class DataController {

    DataService dataService;

    //@GetMapping(value = "/exportCSV", produces = "text/csv")
    /*public ResponseEntity<Resource> exportCSV() {
        return dataService.exportCSV();
    }*/
    @GetMapping(value = "/getAllSousVoyages")
    public ResponseEntity<Resource> getAllSouscriptionsVoyages(){
        return dataService.getAllSouscriptionsVoyages();
    }
    @GetMapping(value = "/getAllSousVoitures")
    public ResponseEntity<Resource> getAllSouscriptionsVoitures(){
        return dataService.getAllSouscriptionsVoitures();
    }
    @PostMapping("/addSous")
    public ResponseEntity addSous(@RequestBody AddSousDto addSousDto){
        return ResponseEntity.ok(dataService.addSous(addSousDto));
    }
    @PutMapping("/updateSous")
    public ResponseEntity updateSous(@RequestBody UpdateSousDto updateSousDto){
        return ResponseEntity.ok(dataService.updateSous(updateSousDto));
    }
    @GetMapping(value = "/getAllSinistres")
    public ResponseEntity<Resource> getAllSinistres(){
        return dataService.getAllSinistres();
    }
    @PostMapping("/addSinistre")
    public ResponseEntity addSinistre(@RequestBody AddSinistreDto addSinistreDto){
        return ResponseEntity.ok(dataService.addSinistre(addSinistreDto));
    }
    @PutMapping("/updateSinistre")
    public ResponseEntity updateSinistre(@RequestBody UpdateSinistreDto updateSinistreDto){
        return ResponseEntity.ok(dataService.updateSinistre(updateSinistreDto));
    }
}
