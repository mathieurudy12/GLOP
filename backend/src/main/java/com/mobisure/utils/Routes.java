package com.mobisure.utils;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Routes {
    public static final String AUTH = "/auth";
    public static final String ASSURANCE = "/assurance";
    public static final String PROFILE = "/profile";
    public static final String FILES = "/files";
    public static final String COLLABORATEUR = "/collaborateur";
    public static final String ASSISTANCE = "/assistance";
    public static final String SEARCH = "/search";
    public static final String PARTENAIRE = "/partenaire";
    public static final String DATA = "/data";
}
