package com.mobisure.auth.dto;

import lombok.Data;

/**
 * Classe qui permet de se connecter
 */
@Data
public class AuthSigninDto {
    String mailAssure;
    String mdpAssure;
}
