package com.mobisure.auth.dto;

import lombok.Data;

/**
 * Classe pour ajouter une adresse lors de l'inscription
 */
@Data
public class AuthAdresseDto {
    int idPersonne;
    String rueAdresse;
    String villeCpAdresse;
    String complements;
    String codeIso;
}
