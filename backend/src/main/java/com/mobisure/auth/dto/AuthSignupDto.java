package com.mobisure.auth.dto;

import lombok.Data;

/**
 * Méthode pour envoyer les informations lros de l'inscription
 */
@Data
public class AuthSignupDto {
    String nom;
    String prenom;
    String dateNaissance;
    String mailAssure;
    String mdpAssure;
    String telephoneAssure;
}
