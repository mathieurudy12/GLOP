package com.mobisure.auth.dto;

import lombok.*;

/**
 * Classe qui permet de retourner les informations lors de la connexion
 */
@Data
@Builder
public class AuthSigninInfosDto {
    int idPersonne;
    String mailAssure;
    String mdpAssure;
    boolean collaborateur;
    int droitsAcces;
}
