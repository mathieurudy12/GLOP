package com.mobisure.auth;

import com.mobisure.auth.dto.AuthAdresseDto;
import com.mobisure.auth.dto.AuthSigninDto;
import com.mobisure.auth.dto.AuthSigninInfosDto;
import com.mobisure.auth.dto.AuthSignupDto;
import com.mobisure.collaborateur.model.Professionnel;
import com.mobisure.collaborateur.repositories.ProfessionnelRepository;
import com.mobisure.configuration.PasswordValidator;
import com.mobisure.exception.ApplicationException;
import com.mobisure.profile.ProfileService;
import com.mobisure.profile.model.Adresse;
import com.mobisure.profile.model.GestionAssure;
import com.mobisure.profile.model.Majeur;
import com.mobisure.profile.model.Personne;
import com.mobisure.profile.repositories.GestionAssureRepository;
import com.mobisure.profile.repositories.MajeurRepository;
import com.mobisure.profile.repositories.PersonneRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class AuthService {

    ProfileService profileService;
    PersonneRepository personneRepository;
    MajeurRepository majeurRepository;
    ProfessionnelRepository professionnelRepository;
    GestionAssureRepository gestionAssureRepository;
    PasswordEncoder passwordEncoder;

    /**
     * Méthode qui permet de s'inscrire en tant qu'assuré
     *
     * @param authSignupDto
     * @return
     */
    public Majeur signup(AuthSignupDto authSignupDto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime dateTime = LocalDate.parse(authSignupDto.getDateNaissance(), formatter).atStartOfDay();

        if (personneRepository.findByNomAndPrenomAndDateNaissance(authSignupDto.getNom(), authSignupDto.getPrenom(), dateTime) != null
                || majeurRepository.findByMailAssure(authSignupDto.getMailAssure()) != null) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne existe deja dans la base !");
        }

        if (!PasswordValidator.isValid(authSignupDto.getMdpAssure())) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Le mot de passe n'est pas sécurisé");
        }

        Personne personne = profileService.createAndSavePersonne(authSignupDto.getNom(), authSignupDto.getPrenom(), dateTime);
        profileService.createAndSaveAssure(personne.getIdPersonne());
        Majeur majeur = Majeur.builder()
                .idPersonne(personne.getIdPersonne())
                .mailAssure(authSignupDto.getMailAssure())
                .mdpAssure(passwordEncoder.encode(authSignupDto.getMdpAssure()))
                .telephoneAssure(authSignupDto.getTelephoneAssure())
                .build();
        LocalDateTime now = LocalDateTime.now();

        Majeur save = majeurRepository.save(majeur);

        GestionAssure gestionAssure = GestionAssure.builder()
                .idPersonne(personne.getIdPersonne())
                .dateDernierChangementMdp(now)
                .dateInscription(now)
                .dateDerniereConnexion(now)
                .build();
        gestionAssureRepository.save(gestionAssure);

        return save;
    }

    /**
     * Méthode pour ajouter une adresse à un assuré
     *
     * @param authAdresseDto
     * @return
     */
    public Adresse postAdresse(AuthAdresseDto authAdresseDto) {
        Adresse adresse = profileService.createAndSaveAdresse(authAdresseDto.getRueAdresse(), authAdresseDto.getVilleCpAdresse(), authAdresseDto.getComplements(), authAdresseDto.getCodeIso());
        profileService.createAndSaveHabiter(authAdresseDto.getIdPersonne(), adresse.getIdAdresse());
        return adresse;
    }

    /**
     * Méthode qui permet de se connecter en vérifiant que l'email et le mot de passe sont bons.
     *
     * @param authSigninDto
     * @return
     */
    public AuthSigninInfosDto signin(AuthSigninDto authSigninDto) {
        Majeur majeur = majeurRepository.findByMailAssure(authSigninDto.getMailAssure());
        Professionnel professionnel = professionnelRepository.findByMailProfessionnel(authSigninDto.getMailAssure());
        boolean collaborateur = false;
        int droitsAcces = 0;
        int idPersonne;
        if (majeur != null && passwordEncoder.matches(authSigninDto.getMdpAssure(), majeur.getMdpAssure())) {
            idPersonne = majeur.getIdPersonne();
        } else if (majeur == null && passwordEncoder.matches(authSigninDto.getMdpAssure(), professionnel.getMdpProfessionnel())) {
            collaborateur = true;
            droitsAcces = professionnel.getDroitsAcces();
            idPersonne = professionnel.getIdPersonne();
        } else {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Le mot de passe n'est pas bon !");
        }

        if (!collaborateur) {
            GestionAssure gestionAssure = gestionAssureRepository.findByIdPersonne(idPersonne);

            if (gestionAssure == null) {
                LocalDateTime now = LocalDateTime.now();
                gestionAssure = GestionAssure.builder()
                        .idPersonne(idPersonne)
                        .dateDernierChangementMdp(now)
                        .dateInscription(now)
                        .dateDerniereConnexion(now)
                        .build();
            } else {
                gestionAssure.setDateDerniereConnexion(LocalDateTime.now());
            }

            gestionAssureRepository.save(gestionAssure);
        }

        return AuthSigninInfosDto.builder()
                .mailAssure(authSigninDto.getMailAssure())
                .mdpAssure(authSigninDto.getMdpAssure())
                .idPersonne(idPersonne)
                .collaborateur(collaborateur)
                .droitsAcces(droitsAcces)
                .build();
    }

}
