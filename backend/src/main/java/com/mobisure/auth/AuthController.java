package com.mobisure.auth;

import com.mobisure.auth.dto.AuthAdresseDto;
import com.mobisure.auth.dto.AuthSigninDto;
import com.mobisure.auth.dto.AuthSignupDto;
import com.mobisure.exception.ApplicationException;
import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.AUTH)
public class AuthController {

    AuthService authService;

    @PostMapping("/signup")
    public ResponseEntity postSignup(@RequestBody AuthSignupDto authSignupDto) {
        try {
            return ResponseEntity.ok(authService.signup(authSignupDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @PostMapping("/adresse")
    public ResponseEntity postAdresse(@RequestBody AuthAdresseDto authAdresseDto) {
        try {
            return ResponseEntity.ok(authService.postAdresse(authAdresseDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @PostMapping("/signin")
    public ResponseEntity postSignin(@RequestBody AuthSigninDto authSigninDto) {
        try {
            return ResponseEntity.ok(authService.signin(authSigninDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }
}
