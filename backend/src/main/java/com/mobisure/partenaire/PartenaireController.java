package com.mobisure.partenaire;

import com.mobisure.assistance.AssistanceService;
import com.mobisure.assistance.dto.AssistanceDto;
import com.mobisure.partenaire.dto.*;
import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.PARTENAIRE)
public class PartenaireController {

    PartenaireService partenaireService;
    AssistanceService assistanceService;

    @GetMapping
    public ResponseEntity<List<PartenaireDto>> getPartenaires(@RequestParam String codeIso) {
        return ResponseEntity.ok(partenaireService.getPartenaires(codeIso));
    }

    @GetMapping("/one")
    public ResponseEntity<PartenaireDto> getPartenaire(@RequestParam String id) {
        return ResponseEntity.ok(partenaireService.getPartenaire(id));
    }

    @GetMapping("/contact")
    public ResponseEntity<List<ContactPartenaireDto>> getContactPartenaire(@RequestParam String id) {
        return ResponseEntity.ok(partenaireService.getContactPartenaire(id));
    }

    @PostMapping("/contact")
    public ResponseEntity<ContactPartenaireDto> createContactPartenaire(@RequestBody ContactPartenairePostDto contactPartenairePostDto) {
        return ResponseEntity.ok(partenaireService.createContactPartenaire(contactPartenairePostDto));
    }

    @PostMapping
    public ResponseEntity<PartenaireDto> createPartenaire(@RequestBody PartenairePostDto partenairePostDto) {
        return ResponseEntity.ok(partenaireService.createPartenaire(partenairePostDto));
    }

    @DeleteMapping
    public ResponseEntity<PartenaireDto> deletePartenaire(@RequestParam String id) {
        return ResponseEntity.ok(partenaireService.deletePartenaire(id));
    }

    @DeleteMapping("/contact")
    public ResponseEntity<ContactPartenaireDto> deleteContactPartenaire(@RequestParam String id) {
        return ResponseEntity.ok(partenaireService.deleteContactPartenaire(id));
    }

    @PutMapping
    public ResponseEntity<PartenaireDto> updatePartenaire(@RequestBody PartenairePutDto partenairePutDto) {
        return ResponseEntity.ok(partenaireService.updatePartenaire(partenairePutDto));
    }

    @GetMapping("/assistance")
    public ResponseEntity<List<AssistanceDto>> getAssistanceByIdContactPartenaire(@RequestParam String id) {
        return ResponseEntity.ok(assistanceService.getAssistanceByIdContactPartenaire(id));
    }
}
