package com.mobisure.partenaire.dto;

import lombok.Data;

/**
 * Classe qui permet de créer un partenaire
 */
@Data
public class PartenairePostDto {
    String rueAdresse;
    String villeCpAdresse;
    String complements;
    String codeIso;
    String nomPartenaire;
    String numTelephone;
}
