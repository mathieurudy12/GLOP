package com.mobisure.partenaire.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe pour récupérer un contact partenaire
 */
@Data
@Builder
public class ContactPartenaireDto {
    String mail;
    String prenom;
    String nom;
    int idPersonne;
}
