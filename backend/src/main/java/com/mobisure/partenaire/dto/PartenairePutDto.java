package com.mobisure.partenaire.dto;

import lombok.Data;

/**
 * Classe qui permet de modifier un partenaire
 */
@Data
public class PartenairePutDto {
    int idPartenaire;
    String rueAdresse;
    String villeCpAdresse;
    String complements;
    String codeIso;
    String nomPartenaire;
    String numTelephone;
}
