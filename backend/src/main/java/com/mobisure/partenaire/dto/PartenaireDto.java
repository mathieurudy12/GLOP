package com.mobisure.partenaire.dto;

import com.mobisure.partenaire.model.Partenaire;
import com.mobisure.profile.model.Adresse;
import lombok.Builder;
import lombok.Data;

/**
 * Classe pour récupérer un partenaire
 */
@Data
@Builder
public class PartenaireDto {
    Adresse adresse;
    Partenaire partenaire;
}
