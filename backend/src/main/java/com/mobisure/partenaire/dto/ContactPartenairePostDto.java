package com.mobisure.partenaire.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter un contact partenaire
 */
@Data
public class ContactPartenairePostDto {
    String mail;
    String prenom;
    String nom;
    String mdp;
    int idPartenaire;
}
