package com.mobisure.partenaire;

import com.mobisure.assistance.model.Pays;
import com.mobisure.assistance.model.Situer;
import com.mobisure.assistance.repositories.PaysRepository;
import com.mobisure.assistance.repositories.SituerRepository;
import com.mobisure.collaborateur.model.Professionnel;
import com.mobisure.collaborateur.repositories.ProfessionnelRepository;
import com.mobisure.exception.ApplicationException;
import com.mobisure.partenaire.dto.*;
import com.mobisure.partenaire.model.ContactPartenaire;
import com.mobisure.partenaire.model.Partenaire;
import com.mobisure.partenaire.repositories.ContactPartenaireRepository;
import com.mobisure.partenaire.repositories.PartenaireRepository;
import com.mobisure.profile.ProfileService;
import com.mobisure.profile.model.Adresse;
import com.mobisure.profile.model.Personne;
import com.mobisure.profile.repositories.AdresseRepository;
import com.mobisure.profile.repositories.PersonneRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class PartenaireService {

    SituerRepository situerRepository;
    AdresseRepository adresseRepository;
    PaysRepository paysRepository;
    PartenaireRepository partenaireRepository;
    ProfileService profileService;
    ProfessionnelRepository professionnelRepository;
    PasswordEncoder passwordEncoder;
    PersonneRepository personneRepository;
    ContactPartenaireRepository contactPartenaireRepository;

    /**
     * Méthode qui permet de récupérer tous les partenaires
     *
     * @param codeIso
     * @return
     */
    public List<PartenaireDto> getPartenaires(String codeIso) {
        Optional<Pays> pays = paysRepository.findByCodeIso(codeIso);
        if (!pays.isPresent()) {
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Aucun pays avec ce code iso !");
        }
        List<Partenaire> listPartenaire = partenaireRepository.findAll();
        if (listPartenaire.isEmpty()) {
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Pas de partenaire pour ce pays !");
        }
        List<PartenaireDto> partenaireDtoList = new ArrayList<>();
        listPartenaire.forEach(partenaire -> {
            Situer situer = situerRepository.findById(partenaire.getIdPartenaire()).
                    orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Pas d'adresse de trouver pour ce partenaire"));
            Adresse adresse = adresseRepository.findByIdAdresse(situer.getIdAdresse());
            if (adresse.getCodeIso().equals(codeIso)) {
                PartenaireDto partenaireDto = PartenaireDto.builder()
                        .partenaire(partenaire)
                        .adresse(adresse)
                        .build();
                partenaireDtoList.add(partenaireDto);
            }
        });
        return partenaireDtoList;
    }

    /**
     * Méthode qui permet de récupérer un partenaire en particulier
     *
     * @param id
     * @return
     */
    public PartenaireDto getPartenaire(String id) {
        Partenaire partenaire = partenaireRepository.findByIdPartenaire(Integer.parseInt(id))
                .orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Partenaire non trouvé"));
        Situer situer = situerRepository.findById(partenaire.getIdPartenaire()).orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Pas d'adresse de trouver pour ce partenaire"));
        Adresse adresse = adresseRepository.findByIdAdresse(situer.getIdAdresse());
        return PartenaireDto.builder()
                .partenaire(partenaire)
                .adresse(adresse)
                .build();
    }

    /**
     * Méthode qui permer de créer un contact partenaire
     *
     * @param contactPartenairePostDto
     * @return
     */
    public ContactPartenaireDto createContactPartenaire(ContactPartenairePostDto contactPartenairePostDto) {

        Personne personne = Personne.builder()
                .nom(contactPartenairePostDto.getNom())
                .prenom(contactPartenairePostDto.getPrenom())
                .build();

        personneRepository.save(personne);

        Professionnel professionnel = Professionnel.builder()
                .droitsAcces(2)
                .idPersonne(personne.getIdPersonne())
                .mailProfessionnel(contactPartenairePostDto.getMail())
                .mdpProfessionnel(passwordEncoder.encode(contactPartenairePostDto.getMdp()))
                .build();
        professionnelRepository.save(professionnel);

        ContactPartenaire contactPartenaire = ContactPartenaire.builder()
                .idPartenaire(contactPartenairePostDto.getIdPartenaire())
                .idPersonne(personne.getIdPersonne())
                .build();

        contactPartenaireRepository.save(contactPartenaire);

        return ContactPartenaireDto.builder()
                .idPersonne(contactPartenaire.getIdPersonne())
                .mail(professionnel.getMailProfessionnel())
                .nom(personne.getNom())
                .prenom(personne.getPrenom())
                .build();
    }

    /**
     * Méthode qui permet de récupérer les contact partenaire d'un partenaire
     *
     * @param idPartenaire
     * @return
     */
    public List<ContactPartenaireDto> getContactPartenaire(String idPartenaire) {
        List<ContactPartenaireDto> contactPartenaireDtos = new ArrayList<>();
        List<ContactPartenaire> contactPartenaires = contactPartenaireRepository.findAllByIdPartenaire(Integer.parseInt(idPartenaire));
        contactPartenaires.forEach(contactPartenaire -> {
            Professionnel professionnel = professionnelRepository.findByIdPersonne(contactPartenaire.getIdPersonne());
            Personne personne = personneRepository.findByIdPersonne(contactPartenaire.getIdPersonne());
            contactPartenaireDtos.add(ContactPartenaireDto.builder()
                    .idPersonne(personne.getIdPersonne())
                    .mail(professionnel.getMailProfessionnel())
                    .nom(personne.getNom())
                    .prenom(personne.getPrenom())
                    .build());
        });
        return  contactPartenaireDtos;
    }

    /**
     * Méthode qui permet de créer un partenaire
     *
     * @param partenairePostDto
     * @return
     */
    public PartenaireDto createPartenaire(PartenairePostDto partenairePostDto) {
        Adresse adresse = profileService.createAndSaveAdresse(partenairePostDto.getRueAdresse(), partenairePostDto.getVilleCpAdresse(), partenairePostDto.getComplements(), partenairePostDto.getCodeIso());
        Partenaire partenaire = Partenaire.builder()
                .nomPartenaire(partenairePostDto.getNomPartenaire())
                .numTelephone(partenairePostDto.getNumTelephone())
                .build();

        partenaire = partenaireRepository.save(partenaire);
        Situer situer = Situer.builder()
                .idPartenaire(partenaire.getIdPartenaire())
                .idAdresse(adresse.getIdAdresse())
                .build();
        situerRepository.save(situer);

        return PartenaireDto.builder()
                .partenaire(partenaire)
                .adresse(adresse)
                .build();
    }

    /**
     * Méthode qui permet de supprimmer un partenaire
     *
     * @param id
     * @return
     */
    public PartenaireDto deletePartenaire(String id) {
        Partenaire partenaire = partenaireRepository.findById(Integer.parseInt(id)).orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Partenaire non trouvé"));
        partenaireRepository.delete(partenaire);
        return PartenaireDto.builder()
                .partenaire(partenaire)
                .build();
    }

    /**
     * Méthode qui permet de supprimer un contact partenaire
     *
     * @param id
     * @return
     */
    public ContactPartenaireDto deleteContactPartenaire(String id) {
        ContactPartenaire contactPartenaire = contactPartenaireRepository.findByIdPersonne(Integer.parseInt(id)).orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Contact partenaire non trouvé"));;
        contactPartenaireRepository.delete(contactPartenaire);
        return  ContactPartenaireDto.builder()
                .idPersonne(contactPartenaire.getIdPersonne())
                .build();
    }

    /**
     * Méthode qui permet de mettre à jour un partenaire
     *
     * @param partenairePutDto
     * @return
     */
    public PartenaireDto updatePartenaire(PartenairePutDto partenairePutDto) {
        Partenaire partenaire = partenaireRepository.findById(partenairePutDto.getIdPartenaire()).orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Partenaire non trouvé"));
        partenaire.setNomPartenaire(partenairePutDto.getNomPartenaire());
        partenaire.setNumTelephone(partenairePutDto.getNumTelephone());
        Situer situer = situerRepository.findByIdPartenaire(partenaire.getIdPartenaire()).orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Adresse non trouvé pour ce partenaire"));
        Adresse adresse = adresseRepository.findByIdAdresse(situer.getIdAdresse());
        adresse.setRueAdresse(partenairePutDto.getRueAdresse());
        adresse.setVilleCpAdresse(partenairePutDto.getVilleCpAdresse());
        adresse.setCodeIso(partenairePutDto.getCodeIso());
        adresse.setComplements(partenairePutDto.getComplements());
        partenaireRepository.save(partenaire);
        adresseRepository.save(adresse);
        return PartenaireDto.builder()
                .partenaire(partenaire)
                .adresse(adresse)
                .build();
    }

}
