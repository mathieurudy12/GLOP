package com.mobisure.partenaire.repositories;

import com.mobisure.partenaire.model.ContactPartenaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactPartenaireRepository extends JpaRepository<ContactPartenaire,Integer> {
    List<ContactPartenaire> findAllByIdPartenaire(Integer idPartenaire);
    Optional<ContactPartenaire> findByIdPersonne(Integer idPersonne);
}
