package com.mobisure.partenaire.repositories;

import com.mobisure.partenaire.model.Partenaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PartenaireRepository extends JpaRepository<Partenaire, Integer> {
    List<Partenaire> findAll();
    Optional<Partenaire> findByIdPartenaire(Integer integer);
}
