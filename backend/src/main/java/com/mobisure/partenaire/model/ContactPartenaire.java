package com.mobisure.partenaire.model;

import com.mobisure.partenaire.model.Partenaire;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "contact_partenaire")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ContactPartenaire {
    @Id
    int idPersonne;
    int idPartenaire;
}
