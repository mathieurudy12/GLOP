package com.mobisure.partenaire.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "partenaire")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Partenaire {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    int idPartenaire;
    String nomPartenaire;
    String numTelephone;
}
