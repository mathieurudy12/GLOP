package com.mobisure.search;

import com.mobisure.profile.model.Assure;
import com.mobisure.profile.model.Majeur;
import com.mobisure.profile.model.Personne;
import com.mobisure.profile.repositories.AssureRepository;
import com.mobisure.profile.repositories.MajeurRepository;
import com.mobisure.profile.repositories.PersonneRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class SearchService {

    MajeurRepository majeurRepository;
    PersonneRepository personneRepository;
    AssureRepository assureRepository;

    public List<SearchDto> searchForMajeurs(String search, Integer page) {
        List<SearchDto> results = new ArrayList<>();
        PageRequest limit = PageRequest.of(page, 10);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String[] splitString = search.split(" ");
        List<Assure> assures = assureRepository.findAll();
        if (splitString.length < 2) {
            Optional<List<Majeur>> majeurs = majeurRepository.findAllByMailAssure(search);
            majeurs.ifPresent(majeurList -> majeurList.forEach(majeur -> {
                Personne personne = personneRepository.findByIdPersonne(majeur.getIdPersonne());
                if (isAssure(personne.getIdPersonne(), assures)) {
                    results.add(SearchDto.builder()
                            .dateNaissance(personne.getDateNaissance().format(formatter))
                            .mail(majeur.getMailAssure())
                            .prenom(personne.getPrenom())
                            .nom(personne.getNom())
                            .idPersonne(personne.getIdPersonne())
                            .build());
                }
            }));
            Optional<List<Personne>> personnes = personneRepository.findAllByNomOrPrenom(search, limit);
            personnes.ifPresent(personneList -> personneList.forEach(personne -> {
                addPerson(results, formatter, assures, personne);
            }));
        } else {
            List<Personne> personnes = new ArrayList<>();
            personnes.addAll(personneRepository.findAllByNomAndPrenom(splitString[0], splitString[1], limit).orElse(Collections.emptyList()));
            personnes.addAll(personneRepository.findAllByNomAndPrenom(splitString[1], splitString[0], limit).orElse(Collections.emptyList()));
            personnes.forEach(personne -> {
                addPerson(results, formatter, assures, personne);
            });
        }
        return results;
    }

    private void addPerson(List<SearchDto> results, DateTimeFormatter formatter, List<Assure> assures, Personne personne) {
        if (isAssure(personne.getIdPersonne(), assures)
                && results.stream().noneMatch(searchDto -> searchDto.idPersonne == personne.getIdPersonne())
                && majeurRepository.existsByIdPersonne(personne.getIdPersonne())
        ) {
            results.add(SearchDto.builder()
                    .mail("")
                    .dateNaissance(personne.getDateNaissance().format(formatter))
                    .prenom(personne.getPrenom())
                    .nom(personne.getNom())
                    .idPersonne(personne.getIdPersonne())
                    .build());
        }
    }

    private boolean isAssure(int idPersonne, List<Assure> assures) {
        return assures.stream().anyMatch(assure -> assure.getIdPersonne() == idPersonne);
    }
}
