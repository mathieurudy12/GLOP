package com.mobisure.search;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SearchDto {
    int idPersonne;
    String nom;
    String prenom;
    String mail;
    String dateNaissance;
}
