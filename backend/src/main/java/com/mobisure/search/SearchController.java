package com.mobisure.search;

import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.SEARCH)
public class SearchController {

    SearchService searchService;

    @GetMapping("/majeurs/{research}")
    public ResponseEntity<List<SearchDto>> searchForMajeurs(
            @PathVariable String research,
            @RequestParam(defaultValue = "0") Integer page) {
            return ResponseEntity.ok(searchService.searchForMajeurs(research.replace("%20", " "),page));
    }
}
