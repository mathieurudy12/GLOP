package com.mobisure.files.service;

import com.mobisure.assurance.AssuranceService;
import com.mobisure.assurance.dto.AssuranceAddFilleDto;
import com.mobisure.assurance.model.DocumentsJustificatifs;
import com.mobisure.assurance.repositories.DocumentsJustificatifsRepository;
import com.mobisure.exception.ApplicationException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class FileService {

    AssuranceService assuranceService;
    DocumentsJustificatifsRepository documentsJustificatifsRepository;

    /**
     * Méthode qui permet d'upload un fichier dans la base et dans le stocke des fichiers
     *
     * @param file
     * @param filetype
     * @param idPerson
     * @return
     */
    @Transactional
    public String uploadFile(MultipartFile file, String filetype, String idPerson) {
        try {
            final String PATH = "src/main/resources/";
            String directoryName = PATH.concat(idPerson + "/");
            String filename = file.getOriginalFilename();
            if (filename == null) {
                throw new ApplicationException(HttpStatus.NOT_FOUND, "Get original filename not found");
            }
            String[] extension = filename.split("\\.");
            String ext = extension[extension.length - 1];
            File directory = new File(directoryName);
            if (!directory.exists()) {
                directory.mkdir();
            }
            else{
                boolean verif = verifIfFilesExists(filetype, Integer.parseInt(idPerson));
                if(verif){
                    File[] files = directory.listFiles();
                    for(File f : files){
                        if(f.getName().contains(filetype) && !f.getName().contains("_")){
                            boolean hasDelete = f.delete();
                            if (!hasDelete) {
                                throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Bad delete of files");
                            }
                        }
                    }
                }
            }
            File fileDir = new File(directoryName + filetype + '.' + ext);
            try (OutputStream os = new FileOutputStream(fileDir)) {
                os.write(file.getBytes());
            }

            //SAVE IN DB
            AssuranceAddFilleDto assuranceAddFilleDto = AssuranceAddFilleDto.builder()
                    .idPersonne(Integer.parseInt(idPerson))
                    .nom_document(filetype)
                    .url_document(directoryName + filetype + '.' + ext)
                    .build();
            assuranceService.addDocumentsJustificatifs(assuranceAddFilleDto);
            return fileDir.getPath();
        } catch (Exception e) {
            return "ERREUR";
        }
    }

    /**
     * Méthode qui vérifie si le fichier n'existe pas déjà
     *
     * @param filename
     * @param idPersonne
     * @return
     */
    public boolean verifIfFilesExists(String filename, int idPersonne){
        List<DocumentsJustificatifs> docs = documentsJustificatifsRepository.getDocumentsJustificatifsByIdPersonne(idPersonne);
        for(DocumentsJustificatifs doc : docs){
            if(doc.getIdFormule() == null){
                if(doc.getNomDocument().equals(filename)){
                    documentsJustificatifsRepository.deleteByIdDocument(doc.getIdDocument());
                    return true;
                }
            }
        }
        return false;
    }
}
