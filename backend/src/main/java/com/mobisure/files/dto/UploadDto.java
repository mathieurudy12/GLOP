package com.mobisure.files.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * Classe qui permet d'upload un fichier
 */
@Data
public class UploadDto {
    MultipartFile file;
}
