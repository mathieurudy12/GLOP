package com.mobisure.files;

import com.mobisure.files.service.FileService;
import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.FILES)
public class FileController {

    FileService fileService;

    @PostMapping("upload")
    public ResponseEntity<String> postSignup(@RequestParam("file") MultipartFile file,
                                             @RequestParam("fileType") String fileType,
                                             @RequestParam("idPerson") String idPerson) {
        return ResponseEntity.ok(fileService.uploadFile(file,fileType,idPerson));
    }
}
