package com.mobisure.assurance;

import com.mobisure.assurance.dto.*;
import com.mobisure.assurance.model.*;
import com.mobisure.assurance.repositories.*;
import com.mobisure.exception.ApplicationException;
import com.mobisure.profile.ProfileService;
import com.mobisure.profile.dtos.PersonneDto;
import com.mobisure.profile.model.Majeur;
import com.mobisure.profile.model.Personne;
import com.mobisure.profile.repositories.AssureRepository;
import com.mobisure.profile.repositories.MajeurRepository;
import com.mobisure.profile.repositories.PersonneRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.*;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@RequiredArgsConstructor
public class AssuranceService {
    ProfileService profileService;
    ApparaitreRepository apparaitreRepository;
    AssocierRepository associerRepository;
    AssureRepository assureRepository;
    ComposerRepository composerRepository;
    DetenirRepository detenirRepository;
    DocumentsJustificatifsRepository documentsJustificatifsRepository;
    FormuleRepository formuleRepository;
    FormuleVehiculeRepository formuleVehiculeRepository;
    FormuleVoyageRepository formuleVoyageRepository;
    MajeurRepository majeurRepository;
    MineurRepository mineurRepository;
    ModeStationnementRepository modeStationnementRepository;
    ModuleFormuleRepository moduleFormuleRepository;
    PermisRepository permisRepository;
    RattacherRepository rattacherRepository;
    SouscrireRepository souscrireRepository;
    TypeAssuranceVoyageRepository typeAssuranceVoyageRepository;
    UsageTrajetRepository usageTrajetRepository;
    VehiculeRepository vehiculeRepository;
    PersonneRepository personneRepository;

    /**
     * Méthode pour créer un véhicule et l'ajouter en base de données
     *
     * @param immatriculation
     * @param dateMiseEnService
     * @param kilometrageEnCours
     * @param marque
     * @param modele
     * @param serie
     * @param carosserie
     * @param puissanceFiscale
     * @param carburant
     * @param idPersonne
     * @return
     */
    public Vehicule createAndSaveVehicule(String immatriculation, LocalDateTime dateMiseEnService,
                                          float kilometrageEnCours, String marque, String modele,
                                          String serie, String carosserie, int puissanceFiscale,
                                          String carburant, int idPersonne) {
        if (!majeurRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Majeur!");
        }
        Vehicule vehicule = Vehicule.builder()
                .immatriculation(immatriculation)
                .dateMiseEnService(dateMiseEnService)
                .kilometrageEnCours(kilometrageEnCours)
                .marque(marque)
                .modele(modele)
                .serie(serie)
                .carosserie(carosserie)
                .puissanceFiscale(puissanceFiscale)
                .carburant(carburant)
                .idPersonne(idPersonne)
                .build();
        return vehiculeRepository.save(vehicule);
    }

    /**
     * Méthode pour créer un contrat et l'ajouter en base de données
     *
     * @param nomFormule
     * @param detailsFormule
     * @param prixFormule
     * @param nbDocument
     * @return
     */
    public Formule createAndSaveFormule(String nomFormule, String detailsFormule, float prixFormule, int nbDocument) {
        Formule formule = Formule.builder()
                .nomFormule(nomFormule)
                .detailsFormule(detailsFormule)
                .prixFormule(prixFormule)
                .status(FormuleStatus.PENDING.toString())
                .nbFichiers(nbDocument)
                .build();
        return formuleRepository.save(formule);
    }

    /**
     * Méthode pour créer un contrat véhicule et l'ajouter en base de données
     *
     * @param idFormule
     * @param zoneGeographique
     * @param exclusif_
     * @param idUsage
     * @param idMode
     * @return
     */
    public FormuleVehicule createAndSaveFormuleVehicule(int idFormule, String zoneGeographique,
                                                        boolean exclusif_, int idUsage, int idMode) {
        FormuleVehicule formuleVehicule = FormuleVehicule.builder()
                .idFormule(idFormule)
                .zoneGeographique(zoneGeographique)
                .exclusif_(exclusif_)
                .idUsage(idUsage)
                .idMode(idMode)
                .build();
        return formuleVehiculeRepository.save(formuleVehicule);
    }

    /**
     * Méthode pour créer un contrat voyage et l'ajouter en base de données
     *
     * @param idFormule
     * @param destinationVoyage
     * @param dateDepartVoyage
     * @param dateRetourVoyage
     * @param typeSejour
     * @param coutVoyage
     * @param idType
     * @return
     */
    public FormuleVoyage createAndSaveFormuleVoyage(int idFormule, String destinationVoyage,
                                                    LocalDateTime dateDepartVoyage,
                                                    LocalDateTime dateRetourVoyage, String typeSejour,
                                                    float coutVoyage, int idType) {
        FormuleVoyage formuleVoyage = FormuleVoyage.builder()
                .idFormule(idFormule)
                .destinationVoyage(destinationVoyage)
                .dateDepartVoyage(dateDepartVoyage)
                .dateRetourVoyage(dateRetourVoyage)
                .coutVoyage(coutVoyage)
                .idType(idType)
                .typeSejour(typeSejour)
                .build();
        return formuleVoyageRepository.save(formuleVoyage);
    }

    /**
     * Méthode pour créer un permis et l'ajouter en base de données
     *
     * @param numeroPermis
     * @param typePermis
     * @param bonusMalus
     * @param formationPermis
     * @return
     */
    public Permis createAndSavePermis(String numeroPermis, String typePermis,
                                      float bonusMalus, String formationPermis) {
        Permis permis = Permis.builder()
                .numeroPermis(numeroPermis)
                .typePermis(typePermis)
                .bonusMalus(bonusMalus)
                .formationPermis(formationPermis)
                .build();
        return permisRepository.save(permis);
    }

    /**
     * Méthode pour créer un assuré mineur et l'ajouter en base de données
     *
     * @param idPersonne
     * @return
     */
    public Mineur createAndSaveMineur(int idPersonne) {
        Mineur mineur = Mineur.builder()
                .idPersonne(idPersonne)
                .build();
        return mineurRepository.save(mineur);
    }

    /**
     * Méthode pour créer une souscription et l'ajouter en base de données
     *
     * @param idFormule
     * @param idPersonne
     * @param dateSouscription
     * @return
     */
    public Souscrire createAndSaveSouscrire(int idFormule, int idPersonne,
                                            LocalDateTime dateSouscription) {
        if (!formuleRepository.existsByIdFormule(idFormule)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La formule n'existe pas dans la table Formule !");
        }
        if (!majeurRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Majeur!");
        }
        Souscrire souscrire = Souscrire.builder()
                .idPersonne(idPersonne)
                .idFormule(idFormule)
                .dateSouscription(dateSouscription)
                .build();
        return souscrireRepository.save(souscrire);
    }

    /**
     * Méthode pour créer une ralation detenir et l'ajouter en base de données
     *
     * @param idPersonne
     * @param numeroPermis
     * @param dateObtention
     * @return
     */
    public Detenir createAndSaveDetenir(int idPersonne, String numeroPermis,
                                        LocalDateTime dateObtention) {
        if (!majeurRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Majeur!");
        }
        if (!permisRepository.existsByNumeroPermis(numeroPermis)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Le numéro de permis n'existe pas dans la table Permis!");
        }
        Detenir detenir = Detenir.builder()
                .idPersonne(idPersonne)
                .numeroPermis(numeroPermis)
                .dateObtention(dateObtention)
                .build();
        return detenirRepository.save(detenir);
    }

    /**
     * Méthode pour créer une relation composer et l'ajouter en base de données
     *
     * @param idFormule
     * @param idModule
     * @return
     */
    public Composer createAndSaveComposer(int idFormule, int idModule) {
        if (!formuleVehiculeRepository.existsByIdFormule(idFormule)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La formule n'existe pas dans la table Formule !");
        }
        if (!moduleFormuleRepository.existsByIdModuleFormule(idModule)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Le module n'existe pas dans la table ModuleFormule !");
        }
        Composer composer = Composer.builder()
                .idFormule(idFormule)
                .idModule(idModule)
                .build();
        return composerRepository.save(composer);
    }

    /**
     * Méthode pour créer une relation associer et l'ajouter en base de données
     *
     * @param idFormule
     * @param immatriculation
     * @return
     */
    public Associer createAndSaveAssocier(int idFormule, String immatriculation) {
        if (!formuleVehiculeRepository.existsByIdFormule(idFormule)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La formule n'existe pas dans la table Formule !");
        }
        if (!vehiculeRepository.existsByImmatriculation(immatriculation)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "L'immatriculation n'est pas présente dans la table Voiture !");
        }
        Associer associer = Associer.builder()
                .idFormule(idFormule)
                .immatriculation(immatriculation)
                .build();
        return associerRepository.save(associer);
    }

    /**
     * Méthode pour créer une relation apparaitre et l'ajouter en base de données
     *
     * @param idFormule
     * @param idPersonne
     * @return
     */
    public Apparaitre createAndSaveApparaitre(int idFormule, int idPersonne) {
        if (!formuleVehiculeRepository.existsByIdFormule(idFormule) && !formuleVoyageRepository.existsByIdFormule(idFormule)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La formule n'existe pas dans la table Formule !");
        }
        if (!mineurRepository.existsByIdPersonne(idPersonne) && !majeurRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Mineur!");
        }
        Apparaitre apparaitre = Apparaitre.builder()
                .idFormule(idFormule)
                .idPersonne(idPersonne)
                .build();
        return apparaitreRepository.save(apparaitre);
    }

    /**
     * Méthode pour créer une relation rattacher et l'ajouter en base de données
     *
     * @param idPersonneMajeur
     * @param idPersonneAssure
     * @return
     */
    public Rattacher createAndSaveRattacher(int idPersonneMajeur, int idPersonneAssure) {
        if (!assureRepository.existsByIdPersonne(idPersonneAssure)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La formule n'existe pas dans la table Formule !");
        }
        if (!majeurRepository.existsByIdPersonne(idPersonneMajeur)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Majeur!");
        }
        Rattacher rattacher = Rattacher.builder()
                .idPersonneAssure(idPersonneAssure)
                .idPersonneMajeur(idPersonneMajeur)
                .build();
        return rattacherRepository.save(rattacher);
    }

    /**
     * Méthode pour créer un document justificatif et l'ajouter en base de données
     *
     * @param idPersonne
     * @param url_document
     * @param nom_document
     * @return
     */
    public DocumentsJustificatifs createAndSaveDocument(int idPersonne, String url_document, String nom_document) {
        if (!majeurRepository.existsByIdPersonne(idPersonne)) {
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "La personne n'existe pas dans la table Majeur!");
        }
        DocumentsJustificatifs documentsJustificatifs = DocumentsJustificatifs.builder()
                .url_document(url_document)
                .nomDocument(nom_document)
                .idPersonne(idPersonne)
                .build();
        return documentsJustificatifsRepository.save(documentsJustificatifs);
    }

    /**
     * Méthode qui permet de créer un nouveau contrat véhicule et tout ce qui est lié à ce contrat à partir des informations récupérées depuis le front
     *
     * @param assuranceVoitureNewDto
     * @return
     */
    public Formule newFormule(AssuranceVoitureNewDto assuranceVoitureNewDto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime dateObtention = LocalDate.parse(assuranceVoitureNewDto.getDateObtention(), formatter).atStartOfDay();
        LocalDateTime dateMiseEnService = LocalDate.parse(assuranceVoitureNewDto.getDateMiseEnservice(), formatter).atStartOfDay();
        String nomUsage = assuranceVoitureNewDto.getNomUsage();
        String nomMode = assuranceVoitureNewDto.getNomMode();
        if (usageTrajetRepository.existsByNomUsageIgnoreCase(nomUsage)) {
            if (modeStationnementRepository.existsByNomModeIgnoreCase(nomMode)) {
                Formule formule = this.createAndSaveFormule(assuranceVoitureNewDto.getNomFormule(), assuranceVoitureNewDto.getDetailsFormule(), assuranceVoitureNewDto.getPrixFormule(), assuranceVoitureNewDto.getNbDocument());
                this.createAndSaveFormuleVehicule(formule.getIdFormule(),
                        assuranceVoitureNewDto.getZoneGeographique(),
                        assuranceVoitureNewDto.isExclusif_()
                        , usageTrajetRepository.findByNomUsageIgnoreCase(nomUsage).getIdUsage(),
                        modeStationnementRepository.findByNomModeIgnoreCase(nomMode).getIdMode());
                List<DocumentsJustificatifs> documents = documentsJustificatifsRepository.getDocumentsJustificatifsByIdPersonne(assuranceVoitureNewDto.getIdPersonne());
                for(DocumentsJustificatifs doc : documents){
                    if(doc.getIdFormule() == null){
                        doc.setIdFormule(formule.getIdFormule());
                        doc.setNomDocument(doc.getNomDocument() + "_" + formule.getIdFormule());
                        String url = doc.getUrl_document();
                        String[] urlSplit = url.split("\\.");
                        doc.setUrl_document(urlSplit[0] + formule.getIdFormule() + "." + urlSplit[1]);
                        documentsJustificatifsRepository.save(doc);
                        final String PATH = "src/main/resources/";
                        String directoryName = PATH.concat(assuranceVoitureNewDto.getIdPersonne() + "/");
                        File directory = new File(directoryName);
                        if (directory.exists()) {
                            File[] files = directory.listFiles();
                            for(File file : files){
                                if(!file.getName().contains("_")){
                                    String[] fileNameSplit = file.getName().split("\\.");
                                    boolean hasCopy = file.renameTo(new File(directoryName + fileNameSplit[0] + "_" + formule.getIdFormule() + "." + fileNameSplit[1]));
                                    if (!hasCopy) {
                                        throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Bad copy of files");
                                    }
                                }
                            }
                        }
                    }
                }
                Vehicule vehicule = this.createAndSaveVehicule(assuranceVoitureNewDto.getImmatriculation(), dateMiseEnService, assuranceVoitureNewDto.getKilometrageEnCours(), assuranceVoitureNewDto.getMarque(), assuranceVoitureNewDto.getModele(), String.valueOf(dateMiseEnService.getYear()), assuranceVoitureNewDto.getCarosserie(), assuranceVoitureNewDto.getPuissanceFiscale(), assuranceVoitureNewDto.getCarburant(), assuranceVoitureNewDto.getIdPersonne());
                Permis permis = this.createAndSavePermis(assuranceVoitureNewDto.getNumeroPermis(), assuranceVoitureNewDto.getTypePermis(), assuranceVoitureNewDto.getBonusMalus(), assuranceVoitureNewDto.getFormationPermis());
                this.createAndSaveAssocier(formule.getIdFormule(), vehicule.getImmatriculation());
                this.createAndSaveDetenir(assuranceVoitureNewDto.getIdPersonne(), permis.getNumeroPermis(), dateObtention);
                this.createAndSaveSouscrire(formule.getIdFormule(), assuranceVoitureNewDto.getIdPersonne(), LocalDate.parse(LocalDateTime.now().format(formatter), formatter).atStartOfDay());
                return formule;
            }
            throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Le mode de stationnement n'existe pas dans la table ModeStationnement !");
        }
        throw new ApplicationException(HttpStatus.UNAUTHORIZED, "L'usage n'existe pas dans la table UsageTrajet !");
    }

    /**
     * Méthode pour créer un contrat voyage et tou ce qui en découle à partir des données récupérées depuis le front de l'application
     *
     * @param assuranceVoyageNewDto
     * @return
     */
    public Formule newFormuleVoyage(AssuranceVoyageNewDto assuranceVoyageNewDto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime dateDepartVoyage = LocalDate.parse(assuranceVoyageNewDto.getDateDepartVoyage(), formatter).atStartOfDay();
        LocalDateTime dateRetourVoyage = LocalDate.parse(assuranceVoyageNewDto.getDateRetourVoyage(), formatter).atStartOfDay();
        String nomType = assuranceVoyageNewDto.getNomType();
        if (typeAssuranceVoyageRepository.existsByNomTypeIgnoreCase(nomType)) {
            Formule formule = this.createAndSaveFormule(assuranceVoyageNewDto.getNomFormule(), assuranceVoyageNewDto.getDetailsFormule(), assuranceVoyageNewDto.getPrixFormule(), assuranceVoyageNewDto.getNbDocument());
            this.createAndSaveFormuleVoyage(formule.getIdFormule(), assuranceVoyageNewDto.getDestinationVoyage(), dateDepartVoyage, dateRetourVoyage, assuranceVoyageNewDto.getTypeSejour(), assuranceVoyageNewDto.getCoutVoyage(), typeAssuranceVoyageRepository.findByNomTypeIgnoreCase(nomType).getIdType());
            this.createAndSaveSouscrire(formule.getIdFormule(), assuranceVoyageNewDto.getIdPersonne(), LocalDate.parse(LocalDateTime.now().format(formatter), formatter).atStartOfDay());
            List<DocumentsJustificatifs> documents = documentsJustificatifsRepository.getDocumentsJustificatifsByIdPersonne(assuranceVoyageNewDto.getIdPersonne());
            for(DocumentsJustificatifs doc : documents){
                if(doc.getIdFormule() == null){
                    doc.setIdFormule(formule.getIdFormule());
                    doc.setNomDocument(doc.getNomDocument() + "_" + formule.getIdFormule());String url = doc.getUrl_document();
                    String[] urlSplit = url.split("\\.");
                    doc.setUrl_document(urlSplit[0] + "_" + formule.getIdFormule() + "." + urlSplit[1]);
                    documentsJustificatifsRepository.save(doc);
                    final String PATH = "src/main/resources/";
                    String directoryName = PATH.concat(assuranceVoyageNewDto.getIdPersonne() + "/");
                    File directory = new File(directoryName);
                    if (directory.exists()) {
                        File[] files = directory.listFiles();
                        for(File file : files){
                            if(!file.getName().contains("_")){
                                String[] fileNameSplit = file.getName().split("\\.");
                                boolean hasCopy = file.renameTo(new File(directoryName + fileNameSplit[0] + "_" + formule.getIdFormule() + "." + fileNameSplit[1]));
                                if (!hasCopy) {
                                    throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Bad copy of files");
                                }
                            }
                        }
                    }
                }
            }
            return formule;
        }
        throw new ApplicationException(HttpStatus.UNAUTHORIZED, "Le type n'existe pas dans la table TypeAssuranceVoyage !");
    }

    /**
     * Méthode pour ajouter un nouveau module au contrat
     *
     * @param assuranceVoitureAddModuleDto
     * @return
     */
    public Composer addModule(AssuranceVoitureAddModuleDto assuranceVoitureAddModuleDto) {
        Composer composer = this.createAndSaveComposer(assuranceVoitureAddModuleDto.getIdFormule(), moduleFormuleRepository.findByNomModuleFormuleIgnoreCase(assuranceVoitureAddModuleDto.getNomModule()).getIdModuleFormule());
        return composer;
    }

    /**
     * Méthode pour ajouter un nouveau document justificatif
     *
     * @param assuranceAddFilleDto
     * @return
     */
    public DocumentsJustificatifs addDocumentsJustificatifs(AssuranceAddFilleDto assuranceAddFilleDto) {
        DocumentsJustificatifs doc = this.createAndSaveDocument(assuranceAddFilleDto.getIdPersonne(), assuranceAddFilleDto.getUrl_document(), assuranceAddFilleDto.getNom_document());
        return doc;
    }

    /**
     * Méthode pour récupérer une formule véhicule en fonction de l'id de l'assuré et de l'id du contrat
     *
     * @param idFormule
     * @param idPersonne
     * @return
     */
    public AssuranceVoitureGetDto getFormuleAuto(int idFormule, int idPersonne) {
        FormuleVehicule formuleVehicule = formuleVehiculeRepository.findByIdFormule(idFormule);
        if (formuleVehicule == null) throw new ApplicationException(HttpStatus.NOT_FOUND, "Formule auto non trouvé");
        if (!assureRepository.existsByIdPersonne(idPersonne))
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Personne assuré non trouvé");
        Souscrire souscrire = souscrireRepository.findByIdFormuleAndIdPersonne(idFormule, idPersonne);
        if (souscrire == null)
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Erreur dans la table souscription");
        Formule formule = formuleRepository.findByIdFormule(idFormule);
        Vehicule vehicule = vehiculeRepository.findVehiculeByImmatriculation(associerRepository.
                findByIdFormule(souscrire.getIdFormule()).getImmatriculation());
        ModeStationnement modeStationnement = modeStationnementRepository.findByIdMode(formuleVehicule.getIdMode());
        UsageTrajet usageTrajet = usageTrajetRepository.findByIdUsage(formuleVehicule.getIdUsage());
        List<Apparaitre> apparaitreList = apparaitreRepository.findAllByIdFormule(idFormule);
        AssuranceGetAssureDto assuranceGetAssureDto;
        if (apparaitreList.isEmpty()) {
            assuranceGetAssureDto = AssuranceGetAssureDto.builder().build();
        } else {
            List<PersonneDto> mineurs = new ArrayList<>();
            List<PersonneDto> majeurs = new ArrayList<>();
            for (Apparaitre apparaitre : apparaitreList) {
                Optional<Mineur> mineur = mineurRepository.findById(apparaitre.getIdPersonne());
                if (mineur.isPresent()) {
                    Personne personne = personneRepository.findByIdPersonne(mineur.get().getIdPersonne());
                    PersonneDto personneDto = PersonneDto.builder()
                            .dateNaissance(personne.getDateNaissance().toString())
                            .idPersonne(personne.getIdPersonne())
                            .nom(personne.getNom())
                            .prenom(personne.getPrenom())
                            .build();
                    mineurs.add(personneDto);
                }
                Optional<Majeur> majeur = majeurRepository.findById(apparaitre.getIdPersonne());
                if (majeur.isPresent()) {
                    Personne personne = personneRepository.findByIdPersonne(majeur.get().getIdPersonne());
                    PersonneDto personneDto = PersonneDto.builder()
                            .dateNaissance(personne.getDateNaissance().toString())
                            .idPersonne(personne.getIdPersonne())
                            .nom(personne.getNom())
                            .prenom(personne.getPrenom())
                            .build();
                    majeurs.add(personneDto);
                }
            }
            assuranceGetAssureDto = AssuranceGetAssureDto.builder()
                    .majeurs(majeurs)
                    .mineurs(mineurs)
                    .build();
        }

        return AssuranceVoitureGetDto.builder()
                .idFormule(souscrire.getIdFormule())
                .dateSouscription(souscrire.getDateSouscription().toString())
                .nomFormule(formule.getNomFormule())
                .nomUsage(usageTrajet.getNomUsage())
                .nomMode(modeStationnement.getNomMode())
                .immatriculation(vehicule.getImmatriculation())
                .marque(vehicule.getMarque())
                .modele(vehicule.getModele())
                .prix(formule.getPrixFormule())
                .status(formule.getStatus())
                .zoneGeographique(formuleVehicule.getZoneGeographique())
                .assuranceGetAssureDto(assuranceGetAssureDto)
                .build();
    }

    /**
     * Méthode qui permet de récupérer tous les contrats d'un assuré
     *
     * @param idPersonne
     * @return liste de contrats
     */
    public List<AssuranceVoitureGetAllDto> getAllFormuleByAssure(int idPersonne) {
        List<AssuranceVoitureGetAllDto> listFormules = new ArrayList<>();
        List<Souscrire> souscrires = souscrireRepository.findAllByIdPersonne(idPersonne);
        for (Souscrire s : souscrires) {
            if (formuleVehiculeRepository.existsByIdFormule(s.getIdFormule())) {
                Formule formule = formuleRepository.findByIdFormule(s.getIdFormule());
                FormuleVehicule formuleVehicule = formuleVehiculeRepository.findByIdFormule(s.getIdFormule());
                Vehicule vehicule = vehiculeRepository.findVehiculeByImmatriculation(associerRepository.findByIdFormule(s.getIdFormule()).getImmatriculation());
                ModeStationnement modeStationnement = modeStationnementRepository.findByIdMode(formuleVehicule.getIdMode());
                UsageTrajet usageTrajet = usageTrajetRepository.findByIdUsage(formuleVehicule.getIdUsage());
                AssuranceVoitureGetAllDto assuranceVoitureGetAllDto = AssuranceVoitureGetAllDto.builder()
                        .idFormule(s.getIdFormule())
                        .dateSouscription(s.getDateSouscription().toString())
                        .nomFormule(formule.getNomFormule())
                        .nomUsage(usageTrajet.getNomUsage())
                        .nomMode(modeStationnement.getNomMode())
                        .immatriculation(vehicule.getImmatriculation())
                        .marque(vehicule.getMarque())
                        .modele(vehicule.getModele())
                        .prix(formule.getPrixFormule())
                        .status(formule.getStatus())
                        .zoneGeographique(formuleVehicule.getZoneGeographique())
                        .build();
                listFormules.add(assuranceVoitureGetAllDto);
            }
        }
        return listFormules;
    }

    /**
     * Méthode pour récupérer toutes les personnes rattachées à un assuré
     *
     * @param idPersonne
     * @return
     */
    public AssuranceGetAssureDto getAllAssure(int idPersonne) {
       Optional<List<Rattacher>> rattacherList = Optional.ofNullable(rattacherRepository.findAllByIdPersonneMajeur(idPersonne))
                .orElseThrow(() -> new ApplicationException(HttpStatus.NOT_FOUND, "Pas de personne rattaché pour cette personne"));
        if (!rattacherList.isPresent()) {
            return AssuranceGetAssureDto.builder()
                    .mineurs(List.of())
                    .majeurs(List.of())
                    .build();
        } else {
            List<PersonneDto> mineurs = new ArrayList<>();
            List<PersonneDto> majeurs = new ArrayList<>();
            for (Rattacher rattacher : rattacherList.get()) {
                Optional<Mineur> mineur = mineurRepository.findById(rattacher.getIdPersonneAssure());
                if (mineur.isPresent()) {
                    Personne personne = personneRepository.findByIdPersonne(mineur.get().getIdPersonne());
                    PersonneDto personneDto = PersonneDto.builder()
                            .dateNaissance(personne.getDateNaissance().toString())
                            .idPersonne(personne.getIdPersonne())
                            .nom(personne.getNom())
                            .prenom(personne.getPrenom())
                            .build();
                    mineurs.add(personneDto);
                }
                Optional<Majeur> majeur = majeurRepository.findById(rattacher.getIdPersonneAssure());
                if (majeur.isPresent()) {
                    Personne personne = personneRepository.findByIdPersonne(majeur.get().getIdPersonne());
                    PersonneDto personneDto = PersonneDto.builder()
                            .dateNaissance(personne.getDateNaissance().toString())
                            .idPersonne(personne.getIdPersonne())
                            .nom(personne.getNom())
                            .prenom(personne.getPrenom())
                            .build();
                    majeurs.add(personneDto);
                }
            }

            return AssuranceGetAssureDto.builder()
                    .mineurs(mineurs)
                    .majeurs(majeurs)
                    .build();
        }
    }

    /**
     * Méthode pour récupérer un contrat voyage en fonction de l'id de l'assuré et de l'id du contrat
     *
     * @param idFormule
     * @param idPersonne
     * @return
     */
    public AssuranceVoyageGetDto getFormuleVoyage(int idFormule, int idPersonne) {
        FormuleVoyage formuleVoyage = formuleVoyageRepository.findByIdFormule(idFormule);
        if (formuleVoyage == null) throw new ApplicationException(HttpStatus.NOT_FOUND, "Formule voyage non trouvé");
        if (!assureRepository.existsByIdPersonne(idPersonne))
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Personne assuré non trouvé");
        Souscrire souscrire = souscrireRepository.findByIdFormuleAndIdPersonne(idFormule, idPersonne);
        if (souscrire == null)
            throw new ApplicationException(HttpStatus.NOT_FOUND, "Erreur dans la table souscription");
        Formule formule = formuleRepository.findByIdFormule(idFormule);
        List<Apparaitre> apparaitreList = apparaitreRepository.findAllByIdFormule(idFormule);
        AssuranceGetAssureDto assuranceGetAssureDto;
        if (apparaitreList.isEmpty()) {
            assuranceGetAssureDto = AssuranceGetAssureDto.builder().build();
        } else {
            List<PersonneDto> mineurs = new ArrayList<>();
            List<PersonneDto> majeurs = new ArrayList<>();
            for (Apparaitre apparaitre : apparaitreList) {
                Optional<Mineur> mineur = mineurRepository.findById(apparaitre.getIdPersonne());
                if (mineur.isPresent()) {
                    Personne personne = personneRepository.findByIdPersonne(mineur.get().getIdPersonne());
                    PersonneDto personneDto = PersonneDto.builder()
                            .dateNaissance(personne.getDateNaissance().toString())
                            .idPersonne(personne.getIdPersonne())
                            .nom(personne.getNom())
                            .prenom(personne.getPrenom())
                            .build();
                    mineurs.add(personneDto);
                }
                Optional<Majeur> majeur = majeurRepository.findById(apparaitre.getIdPersonne());
                if (majeur.isPresent()) {
                    Personne personne = personneRepository.findByIdPersonne(majeur.get().getIdPersonne());
                    PersonneDto personneDto = PersonneDto.builder()
                            .dateNaissance(personne.getDateNaissance().toString())
                            .idPersonne(personne.getIdPersonne())
                            .nom(personne.getNom())
                            .prenom(personne.getPrenom())
                            .build();
                    majeurs.add(personneDto);
                }
            }
            assuranceGetAssureDto = AssuranceGetAssureDto.builder()
                    .majeurs(majeurs)
                    .mineurs(mineurs)
                    .build();
        }
        return AssuranceVoyageGetDto.builder()
                .idFormule(souscrire.getIdFormule())
                .dateSouscription(souscrire.getDateSouscription().toString())
                .nomFormule(formule.getNomFormule())
                .nbVoyageurs(1)
                .type(formuleVoyage.getTypeSejour())
                .destination(formuleVoyage.getDestinationVoyage())
                .prix(formule.getPrixFormule())
                .status(formule.getStatus())
                .dateDebut(formuleVoyage.getDateDepartVoyage().toString())
                .dateFin(formuleVoyage.getDateRetourVoyage().toString())
                .assuranceGetAssureDto(assuranceGetAssureDto)
                .build();
    }

    /**
     * Méthode pour récupérer tous les contrats voyages d'un assuré
     *
     * @param idPersonne
     * @return
     */
    public List<AssuranceVoyageGetAllDto> getAllFormuleVoyageByAssure(int idPersonne) {
        List<AssuranceVoyageGetAllDto> listFormules = new ArrayList<>();
        List<Souscrire> souscrires = souscrireRepository.findAllByIdPersonne(idPersonne);
        for (Souscrire s : souscrires) {
            if (formuleVoyageRepository.existsByIdFormule(s.getIdFormule())) {
                Formule formule = formuleRepository.findByIdFormule(s.getIdFormule());
                FormuleVoyage formuleVoyage = formuleVoyageRepository.findByIdFormule(s.getIdFormule());
                AssuranceVoyageGetAllDto assuranceVoyageGetAllDto = AssuranceVoyageGetAllDto.builder()
                        .idFormule(s.getIdFormule())
                        .dateSouscription(s.getDateSouscription().toString())
                        .nomFormule(formule.getNomFormule())
                        .nbVoyageurs(1)
                        .type(formuleVoyage.getTypeSejour())
                        .destination(formuleVoyage.getDestinationVoyage())
                        .prix(formule.getPrixFormule())
                        .status(formule.getStatus())
                        .dateDebut(formuleVoyage.getDateDepartVoyage().toString())
                        .dateFin(formuleVoyage.getDateRetourVoyage().toString())
                        .build();
                listFormules.add(assuranceVoyageGetAllDto);
            }
        }
        return listFormules;
    }

    /**
     * Méthode pour rattacher une personne existante à un assuré
     *
     * @param assuranceExistingAssureDto
     * @return
     */
    public Apparaitre addExistingAssure(AssuranceExistingAssureDto assuranceExistingAssureDto) {
        Personne personneAssure = personneRepository.findByIdPersonne(assuranceExistingAssureDto.getIdPersonne());
        Personne personneMajeur = personneRepository.findByIdPersonne(assuranceExistingAssureDto.getIdPersonne());
        if (personneAssure == null || personneMajeur == null) {
            throw new ApplicationException(HttpStatus.NOT_FOUND,"Une personne n'a pas été trouvé dans la base");
        }
        return this.createAndSaveApparaitre(assuranceExistingAssureDto.getIdFormule(),assuranceExistingAssureDto.getIdPersonne());
    }

    /**
     * Méthode pour rattacher une nouvelle personne à un assuré
     *
     * @param assuranceAddAssureDto
     * @return
     */
    public Rattacher addAssure(AssuranceAddAssureDto assuranceAddAssureDto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime dateNaissance = LocalDate.parse(assuranceAddAssureDto.getDateNaissance(), formatter).atStartOfDay();
        Personne personne = profileService.createAndSavePersonne(assuranceAddAssureDto.getNom(), assuranceAddAssureDto.getPrenom(), dateNaissance);
        profileService.createAndSaveAssure(personne.getIdPersonne());
        int age = Period.between(dateNaissance.toLocalDate(), LocalDate.now()).getYears();
        if (age > 17) {
            Majeur majeur = Majeur.builder()
                    .idPersonne(personne.getIdPersonne())
                    .mailAssure(null)
                    .mdpAssure(null)
                    .telephoneAssure(null)
                    .build();
            majeurRepository.save(majeur);
        } else {
            this.createAndSaveMineur(personne.getIdPersonne());
        }
        Rattacher rattacher = this.createAndSaveRattacher(assuranceAddAssureDto.getIdPersonneMajeur(), personne.getIdPersonne());
        this.createAndSaveApparaitre(assuranceAddAssureDto.getIdFormule(), personne.getIdPersonne());
        return rattacher;
    }

    /**
     * Méthode pour récupérer tous les usages trajets.
     *
     * @return
     */
    public List<UsageTrajet> getUsages() {
        List<UsageTrajet> listUsages;
        listUsages = usageTrajetRepository.findAll();
        return listUsages;
    }

    /**
     * Méthode pour récupérer tous les modes de stationnement
     *
     * @return
     */
    public List<ModeStationnement> getModes() {
        List<ModeStationnement> listModes;
        listModes = modeStationnementRepository.findAll();
        return listModes;
    }

    /**
     * Méthode pour récupérer tous les modules des contrats
     *
     * @return
     */
    public List<ModuleFormule> getModules() {
        List<ModuleFormule> listModules;
        listModules = moduleFormuleRepository.findAll();
        return listModules;
    }

    /**
     * Méthode pour supprimer un contrat
     *
     * @param idFormule
     * @return
     */
    public boolean deleteFormule(int idFormule) {
        FormuleVehicule fVeh = formuleVehiculeRepository.findByIdFormule(idFormule);
        FormuleVoyage fVoy = formuleVoyageRepository.findByIdFormule(idFormule);
        if (fVoy != null) {
            formuleVoyageRepository.delete(fVoy);
        } else {
            formuleVehiculeRepository.delete(fVeh);
        }
        formuleRepository.delete(formuleRepository.findByIdFormule(idFormule));
        return true;
    }
}
