package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "formuleVehicule")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FormuleVehicule {
    @Id
    int idFormule;
    String zoneGeographique;
    boolean exclusif_;
    int idUsage;
    int idMode;
}
