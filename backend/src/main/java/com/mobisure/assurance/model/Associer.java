package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "associer")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Associer{
    @Id
    int idFormule;
    String immatriculation;
}
