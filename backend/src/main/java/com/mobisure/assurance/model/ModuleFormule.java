package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "moduleFormule")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ModuleFormule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idModuleFormule;
    String nomModuleFormule;
    float tarifModuleFormule;
}
