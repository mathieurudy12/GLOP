package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "rattacher")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Rattacher {
    @Id
    int idPersonneAssure;
    int idPersonneMajeur;
}
