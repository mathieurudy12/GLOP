package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "modeStationnement")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ModeStationnement {
    @Id
    int idMode;
    String nomMode;
}
