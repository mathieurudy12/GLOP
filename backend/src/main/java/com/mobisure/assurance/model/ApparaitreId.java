package com.mobisure.assurance.model;

import javax.persistence.Id;
import java.io.Serializable;

public class ApparaitreId implements Serializable {
    int idPersonne;
    int idFormule;

    public ApparaitreId() {

    }

    public ApparaitreId(int idPersonne, int idFormule) {
        this.idPersonne = idPersonne;
        this.idFormule = idFormule;
    }
}
