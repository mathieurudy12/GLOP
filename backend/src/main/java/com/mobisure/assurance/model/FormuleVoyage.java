package com.mobisure.assurance.model;



import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "formuleVoyage")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FormuleVoyage {
    @Id
    int idFormule;
    String destinationVoyage;
    LocalDateTime dateDepartVoyage;
    LocalDateTime dateRetourVoyage;
    String typeSejour;
    float coutVoyage;
    int idType;
}
