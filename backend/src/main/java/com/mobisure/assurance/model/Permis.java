package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "permis")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Permis {
    @Id
    String numeroPermis;
    String typePermis;
    float bonusMalus;
    String formationPermis;
}
