package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "apparaitre")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@IdClass(ApparaitreId.class)
public class Apparaitre {
    @Id
    int idFormule;
    @Id
    int idPersonne;
}
