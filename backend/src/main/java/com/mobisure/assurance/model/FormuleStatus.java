package com.mobisure.assurance.model;

public enum FormuleStatus {
    PENDING,ACCEPTED,REFUSED,ENDED;
}
