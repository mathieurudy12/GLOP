package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "documentsJustificatifs")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class DocumentsJustificatifs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idDocument;
    String url_document;
    String nomDocument;
    int idPersonne;
    Integer idFormule;
}
