package com.mobisure.assurance.model;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "vehicule")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Vehicule {
    @Id
    String immatriculation;
    LocalDateTime dateMiseEnService;
    float kilometrageEnCours;
    float kilometragePrecedent;
    String marque;
    String modele;
    String serie;
    String carosserie;
    int puissanceFiscale;
    String carburant;
    int idPersonne;
}
