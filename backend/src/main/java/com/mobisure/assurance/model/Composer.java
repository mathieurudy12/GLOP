package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "composer")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@IdClass(ComposerId.class)
public class Composer {
    @Id
    int idFormule;
    @Id
    int idModule;
}
