package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "souscrire")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Souscrire{
    @Id
    int idFormule;
    int idPersonne;
    LocalDateTime dateSouscription;
}
