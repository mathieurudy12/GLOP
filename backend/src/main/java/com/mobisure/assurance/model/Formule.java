package com.mobisure.assurance.model;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "formule")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Formule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idFormule;
    String nomFormule;
    String detailsFormule;
    float prixFormule;
    String status;
    int nbFichiers;
}
