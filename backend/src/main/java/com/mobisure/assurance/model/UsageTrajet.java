package com.mobisure.assurance.model;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "usageTrajet")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UsageTrajet {
    @Id
    int idUsage;
    String nomUsage;
}
