package com.mobisure.assurance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "detenir")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Detenir{
    @Id
    int idPersonne;
    String numeroPermis;
    LocalDateTime dateObtention;
}
