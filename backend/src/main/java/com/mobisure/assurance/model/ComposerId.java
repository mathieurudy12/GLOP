package com.mobisure.assurance.model;

import java.io.Serializable;


public class ComposerId implements Serializable {
    int idFormule;
    int idModule;

    public ComposerId(int idFormule, int idModule){
        this.idFormule = idFormule;
        this.idModule = idModule;
    }

    public ComposerId(){

    }
}
