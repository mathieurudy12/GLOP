package com.mobisure.assurance.model;



import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "typeAssuranceVoyage")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TypeAssuranceVoyage {
    @Id
    int idType;
    String nomType;
}
