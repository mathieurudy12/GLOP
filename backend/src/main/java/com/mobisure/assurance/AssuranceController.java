package com.mobisure.assurance;

import com.mobisure.assurance.dto.*;
import com.mobisure.assurance.model.ModeStationnement;
import com.mobisure.assurance.model.ModuleFormule;
import com.mobisure.assurance.model.UsageTrajet;
import com.mobisure.exception.ApplicationException;
import com.mobisure.utils.Routes;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequiredArgsConstructor
@RequestMapping(Routes.ASSURANCE)
public class AssuranceController {

    AssuranceService assuranceService;

    @PostMapping("/newFormule")
    public ResponseEntity newFormule(@RequestBody AssuranceVoitureNewDto assuranceVoitureNewDto) {
        try {
            return ResponseEntity.ok(assuranceService.newFormule(assuranceVoitureNewDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @PostMapping("/newFormuleVoyage")
    public ResponseEntity newFormuleVoyage(@RequestBody AssuranceVoyageNewDto assuranceVoyageNewDto) {
        try {
            return ResponseEntity.ok(assuranceService.newFormuleVoyage(assuranceVoyageNewDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @PostMapping("/addModule")
    public ResponseEntity addModule(@RequestBody AssuranceVoitureAddModuleDto assuranceVoitureAddModuleDto) {
        try {
            return ResponseEntity.ok(assuranceService.addModule(assuranceVoitureAddModuleDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @GetMapping("/getAllFormuleByAssure/{idPersonne}")
    public ResponseEntity getAllFormuleByAssure(@PathVariable int idPersonne) {
        try {
            return ResponseEntity.ok(assuranceService.getAllFormuleByAssure(idPersonne));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @GetMapping("/getFormuleVoyageByIdFormule")
    public ResponseEntity getFormuleVoyageByIdFormule(@RequestParam int idFormule, @RequestParam int idPersonne) {
        try {
            return ResponseEntity.ok(assuranceService.getFormuleVoyage(idFormule, idPersonne));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @GetMapping("/getFormuleAutoByIdFormule")
    public ResponseEntity getFormuleAutoByIdFormule(@RequestParam int idFormule, @RequestParam int idPersonne) {
        try {
            return ResponseEntity.ok(assuranceService.getFormuleAuto(idFormule, idPersonne));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @GetMapping("/getAllFormuleVoyageByAssure/{idPersonne}")
    public ResponseEntity getAllFormuleVoyageByAssure(@PathVariable int idPersonne) {
        try {
            return ResponseEntity.ok(assuranceService.getAllFormuleVoyageByAssure(idPersonne));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @GetMapping("/getAllAssures/{idPersonne}")
    public ResponseEntity getAllAssures(@PathVariable int idPersonne) {
        try {
            return ResponseEntity.ok(assuranceService.getAllAssure(idPersonne));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @PostMapping("/addAssure")
    public ResponseEntity addAssure(@RequestBody AssuranceAddAssureDto assuranceAddAssureDto) {
        try {
            return ResponseEntity.ok(assuranceService.addAssure(assuranceAddAssureDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @PostMapping("/addExistingAssure")
    public ResponseEntity addExistingAssure(@RequestBody AssuranceExistingAssureDto assurancePutAssureDto) {
        try {
            return ResponseEntity.ok(assuranceService.addExistingAssure(assurancePutAssureDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @PostMapping("/addDocumentsJustificatifs")
    public ResponseEntity addDocumentsJustificatifs(@RequestBody AssuranceAddFilleDto assuranceAddFilleDto) {
        try {
            return ResponseEntity.ok(assuranceService.addDocumentsJustificatifs(assuranceAddFilleDto));
        } catch (ApplicationException exception) {
            return ResponseEntity.status(exception.getErrCode()).body(exception.getErrMsg());
        }
    }

    @GetMapping("/getUsages")
    public ResponseEntity<List<UsageTrajet>> getUsages() {
        return ResponseEntity.ok(assuranceService.getUsages());
    }

    @GetMapping("/getModes")
    public ResponseEntity<List<ModeStationnement>> getModes() {
        return ResponseEntity.ok(assuranceService.getModes());
    }

    @GetMapping("/getModules")
    public ResponseEntity<List<ModuleFormule>> getModules() {
        return ResponseEntity.ok(assuranceService.getModules());
    }

    @DeleteMapping(value = "/deleteFormule/{idFormule}")
    public ResponseEntity deleteFormule(@PathVariable int idFormule) {

        boolean isRemoved = assuranceService.deleteFormule(idFormule);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(idFormule, HttpStatus.OK);
    }

}
