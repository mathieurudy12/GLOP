package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Vehicule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehiculeRepository extends JpaRepository<Vehicule,Integer> {
    Vehicule findVehiculeByImmatriculation(String immatriculation);
    boolean existsByImmatriculation(String immatriculation);
    List<Vehicule> findAllByIdPersonne(int idPersonne);
}