package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Formule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormuleRepository extends JpaRepository<Formule,Integer> {
    Formule findByIdFormule(int idFormule);
    boolean existsByIdFormule(int idFormule);
    void deleteByIdFormule(int idFormule);
}