package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.ModuleFormule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModuleFormuleRepository extends JpaRepository<ModuleFormule,Integer> {
    boolean existsByIdModuleFormule(int idModule);
    ModuleFormule findByNomModuleFormuleIgnoreCase(String nomModule);
    ModuleFormule findByIdModuleFormule(int idModule);
    List<ModuleFormule> findAll();
    List<ModuleFormule> findByTarifModuleFormule(float tarif);
}