package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Associer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssocierRepository extends JpaRepository<Associer,Integer> {
    Associer findByIdFormule(int idFormule);
}