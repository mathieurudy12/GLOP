package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Mineur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MineurRepository extends JpaRepository<Mineur,Integer> {
    boolean existsByIdPersonne(int idPersonne);
}