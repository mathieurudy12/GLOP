package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Composer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComposerRepository extends JpaRepository<Composer,Integer> {
    List<Composer> findByIdFormule(int idFormule);
    void deleteByIdModule(int idModule);
}