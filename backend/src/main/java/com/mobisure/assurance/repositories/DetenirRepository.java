package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Detenir;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetenirRepository extends JpaRepository<Detenir,Integer> {
    Detenir findByIdPersonne(int idPersonne);
}