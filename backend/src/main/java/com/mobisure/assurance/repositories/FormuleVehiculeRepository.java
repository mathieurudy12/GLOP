package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.FormuleVehicule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormuleVehiculeRepository extends JpaRepository<FormuleVehicule,Integer> {
    boolean existsByIdFormule(int idFormule);
    FormuleVehicule findByIdFormule(int idFormule);
}