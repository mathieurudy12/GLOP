package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.ModeStationnement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModeStationnementRepository extends JpaRepository<ModeStationnement,Integer> {
    boolean existsByNomModeIgnoreCase(String nomMode);
    ModeStationnement findByNomModeIgnoreCase(String nomMode);
    ModeStationnement findByIdMode(int idMode);
    List<ModeStationnement> findAll();
}