package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.FormuleVoyage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormuleVoyageRepository extends JpaRepository<FormuleVoyage, Integer> {
    boolean existsByIdFormule(int idFormule);

    FormuleVoyage findByIdFormule(int idFormule);
}
