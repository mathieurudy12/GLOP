package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.TypeAssuranceVoyage;
import com.mobisure.assurance.model.UsageTrajet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeAssuranceVoyageRepository extends JpaRepository<TypeAssuranceVoyage,Integer> {
    boolean existsByNomTypeIgnoreCase(String nomType);
    TypeAssuranceVoyage findByNomTypeIgnoreCase(String nomType);
    TypeAssuranceVoyage findByIdType(int idType);
}
