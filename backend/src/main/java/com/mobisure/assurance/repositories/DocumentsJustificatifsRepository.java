package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.DocumentsJustificatifs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.print.Doc;
import java.util.List;

@Repository
public interface DocumentsJustificatifsRepository extends JpaRepository<DocumentsJustificatifs,Integer> {
    List<DocumentsJustificatifs> getDocumentsJustificatifsByIdPersonne(int idPersonne);
    void deleteByIdDocument(int idDocument);
    List<DocumentsJustificatifs> getDocumentsJustificatifsByIdFormule(int idFormule);
    DocumentsJustificatifs getDocumentsJustificatifsByNomDocument(String nomDocument);
}
