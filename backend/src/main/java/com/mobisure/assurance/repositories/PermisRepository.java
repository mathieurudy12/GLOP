package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Permis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermisRepository extends JpaRepository<Permis,Integer> {
    boolean existsByNumeroPermis(String numeroPermis);
    Permis findByNumeroPermis(String numeroPermis);
}