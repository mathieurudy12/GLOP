package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Apparaitre;
import com.mobisure.assurance.model.Rattacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ApparaitreRepository extends JpaRepository<Apparaitre,Integer> {

    List<Apparaitre> findAllByIdFormule(int idFormule);
    List<Apparaitre> findAll();
}
