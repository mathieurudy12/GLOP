package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.UsageTrajet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsageTrajetRepository extends JpaRepository<UsageTrajet,Integer> {
    boolean existsByNomUsageIgnoreCase(String nomUsage);
    UsageTrajet findByNomUsageIgnoreCase(String nomUsage);
    UsageTrajet findByIdUsage(int idUsage);
    List<UsageTrajet> findAll();
}