package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Rattacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RattacherRepository extends JpaRepository<Rattacher,Integer> {

    Optional<List<Rattacher>> findAllByIdPersonneMajeur(int idPersonneMajeur);
}
