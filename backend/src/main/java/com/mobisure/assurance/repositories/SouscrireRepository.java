package com.mobisure.assurance.repositories;

import com.mobisure.assurance.model.Souscrire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SouscrireRepository extends JpaRepository<Souscrire,Integer> {
    List<Souscrire> findAllByIdPersonne(int idPersonne);
    List<Souscrire> findAll();
    Souscrire findByIdFormule(int idFormule);
    Souscrire findByIdFormuleAndIdPersonne(int idFormule, int idPersonne);
}