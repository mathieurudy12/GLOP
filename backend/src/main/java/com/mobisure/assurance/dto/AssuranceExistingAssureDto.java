package com.mobisure.assurance.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter un assuré qui existe déjà sur un contrat
 */
@Data
public class AssuranceExistingAssureDto {
    int idPersonne;
    int idPersonneMajeur;
    int idFormule;
}
