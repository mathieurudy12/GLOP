package com.mobisure.assurance.dto;

import lombok.*;

/**
 * Classe qui permet de récupérer tous les contrats voyages
 */
@Data
@Builder
public class AssuranceVoyageGetAllDto {
    int idFormule;
    String nomFormule;
    String destination;
    String type;
    int nbVoyageurs;
    float prix;
    String status;
    String dateSouscription;
    String dateDebut;
    String dateFin;
}
