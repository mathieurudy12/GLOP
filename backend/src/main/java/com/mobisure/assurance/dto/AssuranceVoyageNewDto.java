package com.mobisure.assurance.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter un contrat voyage
 */
@Data
public class AssuranceVoyageNewDto {
    String nomFormule;
    String detailsFormule;
    float prixFormule;
    int idPersonne;
    String destinationVoyage;
    String dateDepartVoyage;
    String dateRetourVoyage;
    String typeSejour;
    float coutVoyage;
    String nomType;
    int nbDocument;
}
