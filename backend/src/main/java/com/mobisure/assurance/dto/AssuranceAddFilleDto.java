package com.mobisure.assurance.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe qui permet d'ajouter un fichier sur un contrat
 */
@Data
@Builder
public class AssuranceAddFilleDto {
    String url_document;
    String nom_document;
    int idPersonne;
}
