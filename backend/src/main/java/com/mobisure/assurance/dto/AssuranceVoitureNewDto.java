package com.mobisure.assurance.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter un contrat véhicule
 */
@Data
public class AssuranceVoitureNewDto {
    String immatriculation;
    String dateMiseEnservice;
    float kilometrageEnCours;
    String marque;
    String modele;
    String carosserie;
    int puissanceFiscale;
    String carburant;
    int idPersonne;
    String numeroPermis;
    String typePermis;
    float bonusMalus;
    String formationPermis;
    String dateObtention;
    String zoneGeographique;
    boolean exclusif_;
    String nomUsage;
    String nomMode;
    String nomFormule;
    String detailsFormule;
    float prixFormule;
    int nbDocument;
}
