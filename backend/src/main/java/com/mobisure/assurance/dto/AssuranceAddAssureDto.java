package com.mobisure.assurance.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter un assuré sur un contrat
 */
@Data
public class AssuranceAddAssureDto {
    int idPersonneMajeur;
    int idFormule;
    String nom;
    String prenom;
    String dateNaissance;
}
