package com.mobisure.assurance.dto;

import lombok.Data;

/**
 * Classe qui permet d'ajouter un module sur un contrat
 */
@Data
public class AssuranceVoitureAddModuleDto {
    int idFormule;
    String nomModule;
}
