package com.mobisure.assurance.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe qui permet de récupérer un contrat véhicule en particulier
 */
@Data
@Builder
public class AssuranceVoitureGetDto {
    int idFormule;
    String immatriculation;
    String marque;
    String modele;
    String nomFormule;
    float prix;
    String status;
    String zoneGeographique;
    String nomUsage;
    String nomMode;
    String dateSouscription;
    AssuranceGetAssureDto assuranceGetAssureDto;
}
