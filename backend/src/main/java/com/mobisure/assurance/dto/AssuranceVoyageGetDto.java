package com.mobisure.assurance.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Classe qui permet de récupérer un contrat voyage
 */
@Data
@Builder
public class AssuranceVoyageGetDto {
    int idFormule;
    String nomFormule;
    String destination;
    String type;
    int nbVoyageurs;
    float prix;
    String status;
    String dateSouscription;
    String dateDebut;
    String dateFin;
    AssuranceGetAssureDto assuranceGetAssureDto;
}
