package com.mobisure.assurance.dto;

import lombok.*;

/**
 * Classe qui permet de récupérer tous les contrats véhicules
 */
@Data
@Builder
public class AssuranceVoitureGetAllDto {
    int idFormule;
    String immatriculation;
    String marque;
    String modele;
    String nomFormule;
    float prix;
    String status;
    String zoneGeographique;
    String nomUsage;
    String nomMode;
    String dateSouscription;
}
