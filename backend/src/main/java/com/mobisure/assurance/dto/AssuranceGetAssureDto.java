package com.mobisure.assurance.dto;

import com.mobisure.profile.dtos.PersonneDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Classe qui permet de retourner les personnes rattachées au contrat
 */
@Data
@Builder
public class AssuranceGetAssureDto {
    List<PersonneDto> mineurs;
    List<PersonneDto> majeurs;
}
