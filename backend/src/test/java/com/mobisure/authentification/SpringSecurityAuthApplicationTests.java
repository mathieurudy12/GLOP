package com.mobisure.authentification;

import com.mobisure.auth.AuthController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Configuration
@ContextConfiguration
@ExtendWith(SpringExtension.class)

public class SpringSecurityAuthApplicationTests {

    @Autowired
    AuthController controller;
    @Test
    void contextLoads(){
        assertThat(controller).isNotNull();
    }

}
