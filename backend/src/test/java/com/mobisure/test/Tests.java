/**package com.mobisure.test;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Configuration
@ContextConfiguration
public class Tests {

    @Autowired
    private TestService ts;

    @Test
    void contextLoads() {}

    @Test
    public void testGetAllTests() {
        com.mobisure.test.Test first = (com.mobisure.test.Test) ts.getAllTests().toArray()[0];
        assertEquals(first.getNom(),"Lejeune");
    }

}
**/