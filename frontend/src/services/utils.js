export class Utils {
    /**
     * Méthode qui permet d'obtenir l'âge à partir d'une date de naissance.
     *
     * @param birthDate -> Date de naissance.
     * @returns {number}
     */
    static getAge(birthDate) {
        var now = new Date();

        /**
         * Méthode pour vérifier si l'année est bissextile.
         *
         * @param year -> Année à vérifier.
         * @returns {boolean}
         */
        function isLeap(year) {
            return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
        }

        // days since the birthdate
        var days = Math.floor((now.getTime() - birthDate.getTime()) / 1000 / 60 / 60 / 24);
        var age = 0;
        // iterate the years
        for (var y = birthDate.getFullYear(); y <= now.getFullYear(); y++) {
            var daysInYear = isLeap(y) ? 366 : 365;
            if (days >= daysInYear) {
                days -= daysInYear;
                age++;
                // increment the age only if there are available enough days for the year.
            }
        }
        return age;
    }

    /**
     * Méthode qui permet de retourner le bon format de date.
     *
     * @param date -> La date a convertir.
     * @returns {*}
     */
    static formatDate(date) {
        Date.prototype.yyyymmdd = function () {
            var mm = this.getMonth() + 1; // getMonth() is zero-based
            var dd = this.getDate();
            return [(dd > 9 ? '' : '0') + dd,
                (mm > 9 ? '' : '0') + mm,
                this.getFullYear(),
            ].join('-');
        };
        return date.yyyymmdd()
    }

    static monthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }

    static dateCheck(from, to, check) {

        var fDate, lDate, cDate;
        fDate = Date.parse(from);
        lDate = Date.parse(to);
        cDate = Date.parse(check);

        if ((cDate <= lDate && cDate >= fDate)) {
            return true;
        }
        return false;
    }

}
