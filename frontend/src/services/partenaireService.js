import axios from "axios";
import {AuthService} from "./authService";

export class PartenaireService {

    /**
     * Méthode qui permet de récupérer tous les partenaires d'un pays.
     *
     * @param codeIso -> Code ISO du pays recherché.
     * @returns {Promise<List<Partenaire>>}
     */
    static async getPartenaires(codeIso) {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "partenaire?codeIso=" + codeIso)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui ermet de récupérer les contact partenaire d'un partenaire
     *
     * @param id -> Id du partenaire.
     * @returns {Promise<List<ContactPartenaire>>}
     */
    static async getContactPartenaires(id) {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "partenaire/contact?id=" + id)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de récupérer un partenaire.
     *
     * @param id -> Id du partenaire.
     * @returns {Promise<Partenaire>}
     */
    static async getOnePartenaire(id) {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "partenaire/one?id=" + id)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de retourner toutes les assistances d'un contact partenaire.
     *
     * @param id -> Id du contact partenaire.
     * @returns {Promise<List<Assistance>>}
     */
    static async getAssistanceByIdContactPartenaire(id) {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "partenaire/assistance?id=" + id)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de supprimer un partenaire.
     *
     * @param id -> Id du partenaire.
     * @returns {Promise<unknown>}
     */
    static async deletePartenaire(id) {
        return new Promise((resolve, reject) => {
            axios.delete(AuthService.url + "partenaire?id="+id)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de supprimer un contact partenaire.
     *
     * @param id -> Id du contact partenaire.
     * @returns {Promise<unknown>}
     */
    static async deleteContactPartenaire(id) {
        return new Promise((resolve, reject) => {
            axios.delete(AuthService.url + "partenaire/contact?id="+id)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de mettre à jour un partenaire.
     *
     * @param data -> Les données à modifier dans le partenaire.
     * @returns {Promise<AxiosResponse<*>>}
     */
    static async updatePartenaire(data) {
        return axios.put(AuthService.url + 'partenaire/',
            data,
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
    }

    /**
     * Méthode qui permet de créer un partenaire.
     *
     * @param data -> Les informations du partenaire.
     * @returns {Promise<AxiosResponse<*>>}
     */
    static async createPartenaire(data) {
        let path = AuthService.url + 'partenaire'
        return axios.post(path, data)
    }

    /**
     * Méthode qui permet de créer un contact partenaire.
     *
     * @param data -> Les informations du contact partenaire.
     * @returns {Promise<AxiosResponse<*>>}
     */
    static async createContactPartenaire(data) {
        let path = AuthService.url + 'partenaire/contact'
        return axios.post(path, data)
    }
}