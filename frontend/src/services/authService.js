import axios from 'axios'

export class AuthService {
    static url = 'http://localhost:8080/'


    //static url = 'https://mobisure-backend.herokuapp.com/'

    /**
     * Méthode qui permet de créer une adresse pour une personne.
     *
     * @param adresse -> Adresse de la personne
     * @param personID -> Id de la ppersonne.
     * @returns {Promise<AxiosResponse<{complements: string, idPersonne, codeIso: (string|(function(): Promise<void>)|*), villeCpAdresse: string, rueAdresse: string}>>}
     */
    static async createAdresse(adresse, personID) {
        let path = this.url + 'auth/adresse'
        const data = {
            "codeIso": adresse.pays,
            "complements": adresse.complement.toLowerCase(),
            "idPersonne": personID,
            "rueAdresse": (adresse.numero + '_' + adresse.rue.toLowerCase()),
            "villeCpAdresse": (adresse.ville.toLowerCase() + '_' + adresse.codePostal)
        }
        return axios.post(path, data)
    }

    /**
     * Méthode qui permet de créer la personne dans la base de données
     *
     * @param dateNaissance -> Date de naissance de la personne.
     * @param mail -> Mail de la personne.
     * @param mdp -> Mot de passe de la personne.
     * @param nom -> Nom de la personne.
     * @param prenom -> Prénom de la personne.
     * @param telephone -> Téléphone de la personne.
     * @returns {Promise<AxiosResponse<{dateNaissance, mdpAssure, mailAssure, nom, prenom, telephoneAssure}>>}
     */
    static async createPerson(dateNaissance, mail, mdp, nom, prenom, telephone) {
        let path = this.url + 'auth/signup'
        const data = {
            "nom": nom,
            "dateNaissance": dateNaissance,
            "mailAssure": mail,
            "mdpAssure": mdp,
            "prenom": prenom,
            "telephoneAssure": telephone
        }
        return axios.post(path, data, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                //"Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
                "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
            }
        })

    }

    /**
     * Méthode pour la connexion au site.
     *
     * @param mailAssure -> Mail de la personne qui veut se connecter
     * @param mdpAssure -> Son mot de passe
     * @returns {Promise<AxiosResponse<{mdpAssure, mailAssure}>>}
     */
    static async signin(mailAssure, mdpAssure) {
        let path = this.url + 'auth/signin'
        const data = {
            "mailAssure": mailAssure,
            "mdpAssure": mdpAssure
        }
        return axios.post(path, data, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
            }
        })
    }
    
}
