import axios from "axios";
import {AuthService} from "./authService";

export class SearchService {

    /**
     * Méthode qui permet de chercher un majeur.
     *
     * @param research -> Lettres pour la recherche.
     * @returns {Promise<unknown>}
     */
    static async searchMajeur(research) {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "search/majeurs/"+ research)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }
}