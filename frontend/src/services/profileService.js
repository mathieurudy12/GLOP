import axios from 'axios'
import {AuthService} from "./authService";

export class ProfileService {

    //static url = 'https://mobisure-backend.herokuapp.com/'

    /**
     * Méthode qui permet de récupérer un assuré.
     *
     * @param idPersonne -> Id de l'assuré.
     * @returns {Promise<AxiosResponse<Personne>>}
     */
    static async getPerson(idPersonne) {
        let path = AuthService.url + 'profile/getInfos/' + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })

    }

    /**
     * Méthode qui permet de récupérer les informations d'un administrateur.
     *
     * @param idPersonne -> Id de l'administrateur.
     * @returns {Promise<AxiosResponse<Personne>>}
     */
    static async getAdmin(idPersonne) {
        let path = AuthService.url + 'collaborateur/getInfosAdmin/' + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })

    }

    /**
     * Méthode qui permet de récupérer les adresses d'un assuré.
     *
     * @param idPersonne -> Id de l'assuré.
     * @returns {Promise<AxiosResponse<List<Adresse>>>}
     */
    static async getAdresses(idPersonne) {
        let path = AuthService.url + 'profile/getAdresses/' + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })

    }

    /**
     * Méthode qui permet de récupérer les véhicules d'un assuré.
     *
     * @param idPersonne -> Id de l'assuré.
     * @returns {Promise<AxiosResponse<List<Vehicule>>>}
     */
    static async getVehicules(idPersonne) {
        let path = AuthService.url + 'profile/getAllVehicules/' + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })

    }
}
