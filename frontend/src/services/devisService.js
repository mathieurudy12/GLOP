import {AuthService} from "./authService";
import axios from 'axios'

export class DevisService {

    /**
     * Méthode qui permet d'ajouter une fichier justificatif.
     *
     * @param file -> Le fichier à upload.
     * @returns {Promise<AxiosResponse<*>>}
     */
    static async upload(file) {
        return axios.post(AuthService.url + "files/upload", file, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
    }

    /**
     * Méthode qui permet d'ajouter un contrat véhicule.
     *
     * @param bonusMalus -> BonusMalus de l'assuré
     * @param carburant -> Carburant du véhicule de l'assuré
     * @param carosserie -> Carosserie du véhicule de l'assuré.
     * @param dateMiseEnService -> Date de mise en service du véhicule de l'assuré.
     * @param dateObtention -> Date d'obtention du permis de l'assuré.
     * @param detailsFormule -> Détails du contrat véhicule.
     * @param exclusif -> Exclusivité ou non de l'utilisation du véhicule.
     * @param formationPermis -> Type de formation de permis de l'assuré.
     * @param idPersonne -> Id de l'assuré.
     * @param immatriculation -> immatriculation du véhicule de la personne.
     * @param kilometrageEnCours -> Kilométrage actuel du véhicule de l'assuré.
     * @param marque -> Marque du véhicule de l'assuré.
     * @param modele -> Modèle du véhciule de l'assuré.
     * @param nomFormule -> Nom du contrat.
     * @param nomMode -> Nom du mode de stationnement du véhicule.
     * @param nomUsage -> Nom de l'usage trajet du véhicule.
     * @param numeroPermis -> Numéro de permis de l'assuré.
     * @param prixFormule -> Coût du contrat.
     * @param puissanceFiscale -> Puissance fiscale du véhicule de l'assuré.
     * @param typePermis -> Type du permis déténu par l'assuré.
     * @param zoneGeographique -> Zone géographique de l'utilisation du véhicule.
     * @param nbDocument -> Nombre de documents fournis.
     * @returns {Promise<AxiosResponse<{formationPermis, prixFormule, dateObtention, carosserie, carburant, nomFormule, nomUsage, dateMiseEnservice, exclusif_, marque, detailsFormule, zoneGeographique, nomMode, puissanceFiscale, typePermis, modele, numeroPermis, nbDocument, idPersonne, kilometrageEnCours, bonusMalus, immatriculation}>>}
     */
    static async sendDevis(bonusMalus, carburant, carosserie, dateMiseEnService, dateObtention, detailsFormule, exclusif, formationPermis, idPersonne, immatriculation, kilometrageEnCours,
                           marque, modele, nomFormule, nomMode, nomUsage, numeroPermis, prixFormule, puissanceFiscale, typePermis, zoneGeographique, nbDocument) {
        return axios.post(AuthService.url + "assurance/newFormule",
            {
                bonusMalus: bonusMalus,
                carburant: carburant,
                carosserie: carosserie,
                dateMiseEnservice: dateMiseEnService,
                dateObtention: dateObtention,
                detailsFormule: detailsFormule,
                exclusif_: exclusif,
                formationPermis: formationPermis,
                idPersonne: idPersonne,
                immatriculation: immatriculation,
                kilometrageEnCours: kilometrageEnCours,
                marque: marque,
                modele: modele,
                nomFormule: nomFormule,
                nomMode: nomMode,
                nomUsage: nomUsage,
                numeroPermis: numeroPermis,
                prixFormule: prixFormule,
                puissanceFiscale: puissanceFiscale,
                typePermis: typePermis,
                zoneGeographique: zoneGeographique,
                nbDocument:nbDocument
            },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
    }

    /**
     * Méthode qui permet d'ajouter un contrat voyage.
     *
     * @param coutVoyage -> Coût du voyage.
     * @param dateDepartVoyage -> Date de départ du voyage prévu.
     * @param dateRetourVoyage -> Date de retour du voyage prévu.
     * @param destinationVoyage -> Destination du voyage prévu.
     * @param detailsFormule -> Détails du contrat.
     * @param idPersonne -> Id de l'assuré.
     * @param nomFormule -> Nom du contrat.
     * @param nomType -> Nom du type de voyage.
     * @param prixFormule -> Coût du contrat.
     * @param typeSejour -> Type de séjour prévu.
     * @param nbDocument -> Nombre de documents fournis.
     * @returns {Promise<AxiosResponse<{detailsFormule, prixFormule, dateRetourVoyage, destinationVoyage, typeSejour, dateDepartVoyage, nbDocument, idPersonne, nomType, nomFormule, coutVoyage}>>}
     */
    static async sendDevisVoyage(coutVoyage, dateDepartVoyage, dateRetourVoyage, destinationVoyage, detailsFormule, idPersonne, nomFormule,
                                 nomType, prixFormule, typeSejour, nbDocument) {

        return axios.post(AuthService.url + "assurance/newFormuleVoyage",
            {
                coutVoyage : coutVoyage,
                dateDepartVoyage : dateDepartVoyage,
                dateRetourVoyage : dateRetourVoyage,
                destinationVoyage : destinationVoyage,
                detailsFormule : detailsFormule,
                idPersonne : idPersonne,
                nomFormule : nomFormule,
                nomType : nomType,
                prixFormule : prixFormule,
                typeSejour : typeSejour,
                nbDocument: nbDocument
            },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
    }

    /**
     * Méthode qui permet d'ajouter un module au contrat.
     *
     * @param idFormule -> Id du contrat.
     * @param nomModule -> Nom du module à ajouter.
     * @returns {Promise<AxiosResponse<{idFormule, nomModule}>>}
     */
    static async addModule(idFormule, nomModule) {
        return axios.post(AuthService.url + "assurance/addModule",
            {
                idFormule: idFormule,
                nomModule: nomModule
            },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
    }

    /**
     * Méthode qui permet d'ajouter un assuré à un contrat.
     *
     * @param idFormule -> Id du contrat.
     * @param idPersonne -> Id de l'assuré à ajouter.
     * @param idPersonneMajeur -> Id de l'assuré concerné.
     * @returns {Promise<AxiosResponse<{idFormule, idPersonneMajeur, idPersonne}>>}
     */
    static async addExistingAssure(idFormule, idPersonne , idPersonneMajeur) {
        return axios.post(AuthService.url + "assurance/addExistingAssure",
            {
                idFormule: idFormule,
                idPersonne: idPersonne,
                idPersonneMajeur : idPersonneMajeur
            },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
    }

    /**
     * Méthode qui permet d'ajouter un nouvel assuré non présent dans la base.
     *
     * @param idFormule -> Id du contrat.
     * @param idPersonne -> Id du nouvel assuré.
     * @param nom -> Nom de la personne.
     * @param prenom -> Prénom de la personne.
     * @param dateNaissance -> Date de naissance de l'assuré.
     * @returns {Promise<AxiosResponse<{idFormule, idPersonneMajeur, dateNaissance, nom, prenom}>>}
     */
    static async addAssure(idFormule, idPersonne,nom,prenom,dateNaissance) {
        return axios.post(AuthService.url + "assurance/addAssure",
            {
                idFormule: idFormule,
                idPersonneMajeur : idPersonne,
                nom : nom,
                prenom : prenom,
                dateNaissance : dateNaissance
            },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
    }

    /**
     * Méthode qui permet de récupérer tous les usage trajet.
     *
     * @returns {Promise<List<UsageTrajet>>}
     */
    static async getUsages() {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assurance/getUsages")
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de récupérer tous les mode de stationnement.
     *
     * @returns {Promise<List<ModeStationnement>>}
     */
    static async getModes() {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assurance/getModes")
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }
}