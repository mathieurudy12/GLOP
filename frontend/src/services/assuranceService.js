import axios from 'axios'
import {AuthService} from "./authService";

export class AssuranceService {

    //static url = 'https://mobisure-backend.herokuapp.com/'

    /**
     * Méthode qui permet d'obtenir tous les contrats vehicules d'une personne en particulier.
     *
     * @param idPersonne -> Id de la personne
     * @returns {Promise<AxiosResponse<List<FormuleVehicule>>>}
     */
    static async getContratsVehicules(idPersonne) {
        let path = AuthService.url + 'assurance/getAllFormuleByAssure/' + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode qui permet d'obtenir une formule véhicule à partir de son id et de l'id d'une personne.
     *
     * @param idFormule -> Id de la formule souhaitée.
     * @param idPersonne -> Id de la personne qui a souscrit à la formule.
     * @returns {Promise<AxiosResponse<FormuleVehicule>>}
     */
    static async getFormuleAuto(idFormule,idPersonne) {
        let path = AuthService.url + 'assurance/getFormuleAutoByIdFormule?idFormule=' + idFormule + "&idPersonne=" + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode qui permet d'obtenir une formule voyage à partir de son id et de l'id d'une personne.
     *
     * @param idFormule -> Id de la formule souhaitée.
     * @param idPersonne -> Id de la personne qui a souscrit à la formule.
     * @returns {Promise<AxiosResponse<FormuleVoyage>>}
     */
    static async getFormuleVoyage(idFormule,idPersonne) {
        let path = AuthService.url + 'assurance/getFormuleVoyageByIdFormule?idFormule=' + idFormule + "&idPersonne=" + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode qui permet de retourner toutes les personnes rattachées à une assuré.
     *
     * @param idPersonne -> Id de la personne.
     * @returns {Promise<AxiosResponse<List<Personne>>>}
     */
    static async getAllAssure(idPersonne) {
        let path = AuthService.url + 'assurance/getAllAssures/' + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode qui permet d'obtenir tous les contrats voyages d'une personne en particulier.
     *
     * @param idPersonne -> Id de la personne
     * @returns {Promise<AxiosResponse<List<FormuleVoyage>>>}
     */
    static async getContratsVoyage(idPersonne) {
        let path = AuthService.url + 'assurance/getAllFormuleVoyageByAssure/' + idPersonne
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }
}
