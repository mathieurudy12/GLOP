import axios from 'axios'
import {AuthService} from "./authService";

export class AssistanceService {

    /**
     * Méthode pour obtenir tous les pays disponibles pour créer une assistance
     * @returns {Promise<List<Pays>>}
     */
    static async getPays() {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getPays")
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode pour obtenir tous les types assistances existants
     * @returns {Promise<List<TypeAssistance>>}
     */
    static async getTypeAssistances() {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getTypeAssistances")
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode pour obtenir tous les partenaires du pays avec son code ISO
     *
     * @param codeIso -> Code ISO du pays recherché
     * @returns {Promise<List<Partenaire>>}
     */
    static async getPartenaires(codeIso) {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getPartenaires?codeIso="+codeIso)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode pour obtenir le type assistance en fonction de son libellé
     *
     * @param libelleTypeAssistance -> Libellé du type d'assistance
     * @returns {Promise<AxiosResponse<TypeAssistance>>}
     */
    static async postTypeAssistance(libelleTypeAssistance) {
        let path = AuthService.url + 'assistance/postTypeAssistance'
        return axios.post(path, libelleTypeAssistance, {
            headers: {
                "Content-Type" : "text/plain",
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
            }
        })
    }

    /**
     * Méthode pour enregistrer une nouvelle assistance.
     *
     * @param tarif -> Tarif de l'assistance.
     * @param commentaires -> Commentaires liés à l'assistance.
     * @param idPersonneEmploye -> Id de la personne qui déclare l'assistance.
     * @param idPersonneAssure -> Id de la personne qui a besoin d'une assistance.
     * @param idTypeAssistance -> Id tu type d'assistance déclarée
     * @param valueTypeSinistre -> Le type de sinistre associé à l'assistance.
     * @param codeIso -> Le code ISO du pays dans lequel l'assistance est demandée.
     * @param idPartenaire -> Id du partenaire qui va prendre en charger l'assistance.
     * @returns {Promise<AxiosResponse<{idPersonneEmploye, idPersonneAssure, idPartenaire, tarif, valueTypeSinistre, commentaires, idTypeAssistance, codeIso}>>}
     */
    static async postAssistance(tarif,commentaires,idPersonneEmploye,idPersonneAssure,idTypeAssistance,valueTypeSinistre,codeIso,idPartenaire) {
        let path = AuthService.url + 'assistance/postAssistance'
        return axios.post(path,
            {
                tarif:tarif,
                commentaires:commentaires,
                idPersonneEmploye:idPersonneEmploye,
                idPersonneAssure:idPersonneAssure,
                idTypeAssistance:idTypeAssistance,
                valueTypeSinistre:valueTypeSinistre,
                codeIso:codeIso,
                idPartenaire:idPartenaire
            },
            {
            headers: {
                "Content-Type" : "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
            }
        })
    }

    /**
     * Méthode pour obtenir toutes les assistances existantes.
     *
     * @returns {Promise<List<Assistance>>}
     */
    static async getAllAssist() {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getAllAssist")
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de récupérer les assistances d'une personne
     *
     * @param idPerson -> Id de l'assuré pour lequel on cherche ses assistances
     * @returns {Promise<List<Assistance>>}
     */
    static async getMyAssist(idPerson) {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getMyAssist?idPerson="+idPerson)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de récupérer les informations d'une personne avec son Id.
     *
     * @param idPerson -> Id de l'assuré
     * @returns {Promise<Assure>}
     */
    static async getAssureFromId(idPerson){
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getAssureFromId?idPerson="+idPerson)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet récupérer les informatuions d'un partenaire à partir de son Id.
     *
     * @param idPartenaire -> Id du partenaire
     * @returns {Promise<Partenaire>}
     */
    static async getPartenaireFromId(idPartenaire){
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getPartenaireFromId?idPartenaire="+idPartenaire)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de récupérer l'état en cours d'une assistance.
     *
     * @param idAssistance -> Id de l'assistance recherchée.
     * @returns {Promise<EtatAssistance>}
     */
    static async getCurrentEtat(idAssistance){
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getCurrentEtat?idAssistance="+idAssistance)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de récupérer tous les états possibles pour une assistance.
     *
     * @returns {Promise<List<EtatAssistance>>}
     */
    static async getEtats(){
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getEtats")
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de récupérer les informations d'un type d'assistance en fonction de son id.
     *
     * @param idTypeAssistance -> Id du type d'assistance recherché.
     * @returns {Promise<TypeAssistance>}
     */
    static async getTypeAssistanceFromId(idTypeAssistance){
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getTypeAssistanceFromId?idTypeAssistance="+idTypeAssistance)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode qui permet de récupérer les informations d'un pays en fonction de son code ISO.
     *
     * @param codeIso -> Code ISO du pays recherché.
     * @returns {Promise<Pays>}
     */
    static async getPaysFromId(codeIso){
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "assistance/getPaysFromId?codeIso="+codeIso)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     *
     *
     * @param idAssistance
     * @param idEtatAssistance
     * @returns {Promise<AxiosResponse<{idEtatAssistance, idAssistance}>>}
     */
    static async postState(idAssistance,idEtatAssistance) {
        let path = AuthService.url + 'assistance/postState'
        return axios.post(path,
            {
                idAssistance:idAssistance,
                idEtatAssistance:idEtatAssistance
            },
            {
                headers: {
                    "Content-Type" : "application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                    "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
                }
            })
    }
}
