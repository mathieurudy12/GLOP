import axios from 'axios'
import {AuthService} from "./authService";
import FileSaver from 'file-saver'

export class CollaborateurService {

    /**
     * Méthode qui récupère tous les clients du système.
     *
     * @returns {Promise<AxiosResponse<List<Personne>>>}
     */
    static async getClients() {
        let path = AuthService.url + 'collaborateur/getAllClients/'
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode pour supprimer un client.
     *
     * @param idPersonne -> Id du client à supprimer.
     * @returns {Promise<AxiosResponse<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Methods": string, "Access-Control-Allow-Headers": string}}>>}
     */
    static async deleteClient(idPersonne) {
        let path = AuthService.url + 'collaborateur/deleteClient/' + parseInt(idPersonne)
        return axios.put(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode pour obtenir tous les contrats vehicules.
     *
     * @returns {Promise<AxiosResponse<List<FormuleVehicule>>>}
     */
    static async getAllContratsVehicules() {
        let path = AuthService.url + 'collaborateur/getAllFormuleVehicule/'
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode pour récupérer tous les contrats de voyage.
     *
     * @returns {Promise<AxiosResponse<List<FormuleVoyage>>>}
     */
    static async getAllContratsVoyages() {
        let path = AuthService.url + 'collaborateur/getAllFormuleVoyage/'
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })

    }

    /**
     * Méthode pour supprimer un contrat.
     *
     * @param idFormule -> Id du contrat à supprimer.
     * @returns {Promise<AxiosResponse<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Methods": string, "Access-Control-Allow-Headers": string}}>>}
     */
    static async deleteDossier(idFormule) {
        let path = AuthService.url + 'collaborateur/deleteDossier/' + parseInt(idFormule)
        return axios.put(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode pour récupérer un contrat spécifique à partir de son id.
     *
     * @param idDossier -> Id du contrat.
     * @returns {Promise<AxiosResponse<Formule>>}
     */
    static async getDossier(idDossier) {
        let path = AuthService.url + 'collaborateur/getDossier/' + parseInt(idDossier)
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode qui permet de mettre à jour un contrat.
     *
     * @param status -> Le statut à mettre à jour
     * @param idDossier -> Id du contrat à mettre à jour.
     * @returns {Promise<AxiosResponse<{status}>>}
     */
    static async updateDossier(status, idDossier) {
        return axios.put(AuthService.url + 'collaborateur/updateDossier/' + parseInt(idDossier),
            {
                status: status
            },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
    }

    /**
     * Méthode qui permet de récupérer les modules pour une formule.
     *
     * @param idFormule -> Id de la formule recherchée.
     * @returns {Promise<AxiosResponse<List<ModuleFormule>>>}
     */
    static async getModulesFormule(idFormule) {
        let path = AuthService.url + 'collaborateur/getModulesFormule/' + parseInt(idFormule)
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode qui permet de récupérer tous les modules existants.
     *
     * @returns {Promise<AxiosResponse<List<ModuleFormule>>>}
     */
    static async getAllModules() {
        let path = AuthService.url + 'assurance/getModules/'
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode qui permet de mettre à jour les modules pour un contrat.
     *
     * @param etat -> Etat du module (à supprimer ou à ajouter)
     * @param nomModule -> Nom du module à modifier
     * @param idDossier -> Id du contrat concerné.
     * @returns {Promise<AxiosResponse<{nomModule, etat}>>}
     */
    static updateModulesDossier(etat, nomModule, idDossier) {
        return axios.put(AuthService.url + 'collaborateur/updateModulesDossier/' + parseInt(idDossier),
            {
                etat: etat,
                nomModule: nomModule
            },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
    }

    /**
     * Méthode pour mettre à jour le prix d'un contrat.
     *
     * @param prixFormule -> Nouveau prix du contrat.
     * @param status -> Nouveau statut du contrat.
     * @param idDossier -> CId du contrat concerné.
     * @returns {Promise<AxiosResponse<{prixFormule, statut}>>}
     */
    static updatePrixFormule(prixFormule, status, idDossier) {
        return axios.put(AuthService.url + 'collaborateur/updatePrixFormule/' + parseInt(idDossier),
        {
            prixFormule: prixFormule,
            statut: status
        },
        {
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }

    /**
     * Méthode pour récupérer les fichiers justificatifs d'un contrat(uniquement nom et emplacement).
     *
     * @param idDossier -> Id du contrat recherché.
     * @returns {Promise<AxiosResponse<List<File>>>}
     */
    static getFilesDossier(idDossier){
        return axios.get(AuthService.url + "collaborateur/getFilesDossier/" + parseInt(idDossier), {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode pour obtenir le détail d'un fichier justificatif (ce qu'il contient).
     *
     * @param fileName -> Nom du fihcier à extraire.
     * @param extension -> Extension du fichier (.pdf, .jpg ou .png)
     * @returns {Promise<AxiosResponse<never>>}
     */
    static getBytesFilesDossier(fileName, extension){
        return axios.get(AuthService.url + "collaborateur/getBytesFilesDossier/" + fileName,{
            headers: {
                    "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                    "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            },
            responseType: "blob"
        }).then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("download", extension);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }).catch(error => {
                console.log(error);
        });
    }

    /**
     * Méthode pour envoyer un mail à un assuré.
     *
     * @param mail -> Mail de l'assuré à contacter.
     * @param message -> Message à adresser.
     * @param  -> Objet du mail.
     * @returns {Promise<AxiosResponse<{mail, message, objet}>>}
     */
    static envoiMail(mail, message, objet){
        return axios.put(AuthService.url + "collaborateur/envoiMail", {
            mail: mail,
            message: message,
            objet:objet
        },
            {
            headers: {
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
            }
        })
    }

    /**
     * Méthode pour supprimer les fichiers non utilisés.
     *
     * @returns {Promise<AxiosResponse<never>>}
     */
    static async verifFichiers() {
        return axios.delete(AuthService.url + "collaborateur/verifFichiers/",{
                headers: {
                    "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                    "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
                }
            })
    }

    /**
     * Méthode pour obtenir tous les collaborateurs existants.
     *
     * @returns {Promise<List<Employe>>}
     */
    static async getCollaborateurs() {
        return new Promise((resolve, reject) => {
            axios.get(AuthService.url + "collaborateur/employee")
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode pour supprimer un collaborateur.
     *
     * @param id -> Id du collaborateur à supprimer.
     * @returns {Promise<unknown>}
     */
    static async deleteCollaborateur(id) {
        return new Promise((resolve, reject) => {
            axios.delete(AuthService.url + "collaborateur/employee?id="+id)
                .then(res => {
                    resolve(res)
                })
                .catch(err => reject(err))
        })
    }

    /**
     * Méthode pour ajouter un collaborateur.
     *
     * @param data -> Données du collaborateur pour l'ajouter.
     * @returns {Promise<AxiosResponse<*>>}
     */
    static async createCollaborateur(data) {
        let path = AuthService.url + 'collaborateur/employee'
        return axios.post(path, data)
    }

    /**
     * Méthode qui permet d'ajouter une souscription à l'entrepôt de données.
     *
     * @param idFormule -> Id du contrat à ajouter.
     * @param status -> Statut du contrat actuel.
     * @returns {Promise<AxiosResponse<{idFormule, statut}>>}
     */
    static async addSouscription(idFormule, status){
        let path = AuthService.url + 'data/addSous'
        const data = {
            "idFormule": idFormule,
            "statut": status
        }
        return axios.post(path, data, {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                //"Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
                "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
            }
        })
    }

    /**
     * Méthode pour modifier une souscription dans l'entrepôt de données.
     *
     * @param idFormule -> Id du contrat à modifier.
     * @param nbModules -> Nombre de modules du contrat actuels.
     * @param tarif -> Tarif actuel du contrat.
     * @returns {Promise<AxiosResponse<{idFormule, tarif, nbModules}>>}
     */
    static async updateSouscription(idFormule, nbModules, tarif){
        let path = AuthService.url + 'data/updateSous'
        const data = {
            "idFormule": idFormule,
            "nbModules": nbModules,
            "tarif": tarif
        }
        return axios.put(path, data, {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                //"Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
                "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
            }
        })
    }

    /**
     * Méthode pour récupérer toutes les souscriptions de voyage.
     *
     * @returns {Promise<AxiosResponse<List<SouscriptionVoyage>>>}
     */
    static async getAllSousVoyages(){
        let path = AuthService.url + 'data/getAllSousVoyages'
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                //"Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
                "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
            },
            responseType: 'blob'}).then(result => {
            const blob = new Blob([result.data], {type: 'text/csv'});
            FileSaver.saveAs(blob, 'export_sous_voyages.csv');
        })
    }

    /**
     * Méthode pour récupérer toutes les souscriptions de véhicule.
     *
     * @returns {Promise<AxiosResponse<List<SouscriptionVehicule>>>}
     */
    static async getAllSousVoitures(){
        let path = AuthService.url + 'data/getAllSousVoitures'
        return axios.get(path, {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
                //"Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
                "Access-Control-Allow-Headers": "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"
            },
            responseType: 'blob'}).then(result => {
            const blob = new Blob([result.data], {type: 'text/csv'});
            FileSaver.saveAs(blob, 'export_sous_voitures.csv');
        })
    }
}
