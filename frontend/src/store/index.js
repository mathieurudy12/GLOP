import {createStore} from 'vuex'

export default createStore({
    state: {
        isConnected : false,
        prenomConnected : "",
        devisAutoPage: 1,
        devisVoyagePage: 1,
        assistancePage: 1,
        devisAutoFiles : [
            {
                text: "Carte d'identité",
                id: "CI"
            },
            {
                text: "Permis de conduire",
                id : "PERMIS"
            },
            {
                text: "Mandat de prélèvement SEPA",
                id: "MANDAT",
                condition : true
            },
            {
                text: "Carte grise",
                id: "GRISE"
            }
        ],
        devisVoyageFiles : [
            {
                text: "Carte d'identité",
                id: "CI"
            },
            {
                text: "Mandat de prélèvement SEPA",
                id: "MANDAT"
            },
            {
                text: "Justificat coût du voyage",
                id: "VOYAGE"
            }
        ],
        devisVoyageGaranties: [
            {
                "text": "Téléconsultation médicale 24/7",
                "detail" : "Un médecin généraliste francophone disponible 24/7 pour une consultation médicale à distance."
            },
            {
                "text": "Assurance frais médicaux à l'étranger",
                "detail" : "Si vous êtes malade ou blessé lors de votre voyage, nous prenons en charge vos frais médicaux à l'étranger. Si vous êtes hospitalisé pour une durée supérieure à 24h, nous pouvons procéder à l'avance de vos frais d'hospitalisation.",
                "plafond" : "1 000 000 € avec franchise de 50€"
            },
            {
                "text": "Assistance médicale et rapatriement"
            },
            {
                "text": "Retour anticipé",
                "detail" : "Si vous devez écourter votre séjour suite au décès ou à l'hospitalisation d'un membre de votre famille, nous organisons et prenons en charge votre trajet de retour à votre domicile.",
                "plafond" : "Frais de transport"
            },
            {
                "text": "Rapatriement différé de l'animal domestique",
                "detail" : "Si vous êtes rapatrié, nous organisons et prenons en charge la venue d'une personne de votre choix pour aller chercher votre chat et/ou votre chien resté sur votre lieu de séjour et le ramener à votre domicile.",
                "plafond" : "Frais de transport"
            },
            {
                "text": "Assurance annulation",
                "detail" : "A la suite d'un événement garanti, si vous êtes contraint d'annuler votre voyage, nous remboursons vos frais d'annulation jusqu'à 8 000€. Si l'annulation concerne plusieurs personnes participant au voyage, nous intervenons à hauteur de 24 000 €.",
                "plafond" : " 8 000 €(franchise : maladie/décès : 50 €, autre cause listée : 10%, min 50 €)"
            },
            {
                "text": "Retard des bagages",
                "detail" : "Si vos bagages vous sont livrés avec plus de 12h de retard, nous vous remboursons vos achats de première nécessité.",
                "plafond" : "300 €"
            },
            {
                "text": "Assistance perte/vol des papiers à l'étranger",
                "detail" : "En cas de perte ou de vol de vos papiers, nous vous aidons dans vos démarches d'opposition et de renouvellement."
            },
            {
                "text": "Frais de duplicata des papiers officiels perdus ou volés",
                "plafond" : "150 €"
            },
            {
                "text": "Aide ménagère après rapatriement\n",
                "detail" : "Après rapatriement, si vous êtes immobilisé à votre domicile ou hospitalisé, nous organisons et prenons en charge les services d'une aide ménagère à votre domicile.",
                "plafond" : "10 h"
            },
            {
                "text": "Confort hospitalier",
                "detail" : "En cas d'hospitalisation, nous vous remboursons vos frais de location de téléviseur à concurrence du montant indiqué.",
                "plafond" : "80 €"
            },
            {
                "text": "Assistance en cas de sinistre au domicile pendant le voyage",
                "detail" : "Si, pendant votre voyage, votre domicile situé en France subit un dégât des eaux, un incendie ou un cambriolage, nous organisons et prenons en charge l'intervention d'un prestataire pour réaliser les mesures conservatoires.",
                "plafond" : "150€ transport et main d'oeuvre"
            },
            {
                "text": "Départ ou retour impossible",
                "detail" : "En cas de perturbations des transports dûes à des phénomènes naturels, vous bénéficiez d'une indemnisation pour couvrir les frais que vous avez dû engager en attendant de pouvoir poursuivre votre voyage.",
                "plafond" : "1 000 € (franchise : 50 €)"
            },
            {
                "text": "Retard d'avion",
                "detail" : "En cas de retard > 3h pour un vol régulier, ou > 6h pour un vol de type charter, nous vous indemnisons pour les frais que vous aurez dû engager sur place (rafraîchissements, repas, hôtel, transferts).",
                "plafond" : "100 €"
            },
            {
                "text": "Départ / correspondance manquée",
                "detail" : "Si vous loupez votre départ ou votre correspondance en raison d'un accident ou d'une panne d'un moyen de transport public ou de votre véhicule utilisé pour vous rendre à l'aéroport, au port ou à la gare de départ, nous vous indemnisons pour les frais que vous aurez dû engager sur place (rafraîchissements, repas, hôtel, transferts).",
                "plafond" : "1 000 €"
            },
            {
                "text": "Assurance responsabilité civile vie privée à l'étranger",
                "detail" : "Si vous causez accidentellement,des dommages corporels, matériels ou immatériels à autrui, nous prenons en charge les conséquences pécuniaires qui vous incombent.",
                "plafond" : "4 500 000€ (franchise : 150 €)"
            },
            {
                "text": "Assurance individuelle accidents",
                "detail" : "Si vous êtes victime d'un accident au cours de votre séjour, entraînant votre invalidité permanente ou votre décès, nous vous versons (ou à vos ayants droit) un capital.",
                "plafond" : "50 000€"
            }
        ],
        devisAutoModule: [
            {
                "text": "Responsabilité civile",
                "base": true,
                "coefficient": 1,
                "detail" : "Prise en charge des dommages que vous pourriez causer à d’autres lors d’un accident de la circulation.",
                "plafond" : "Dommage matériels : 100 millions d'euros & Dommages corporels : pas de limite."
            },
            {
                "text": "Défense pénale et recours suite à un accident",
                "base": true,
                "coefficient": 1,
                "detail" : "Défense de vos droits en cas de poursuite pénale et exercice d'un recours amiable ou judiciaire pour vous ou les personnes transportées, suite à un accident de la circulation.",
                "plafond" : "Jusqu'à 20 000 euros de frais de justice et honoraires."
            },
            {
                "text": "Garantie du conducteur",
                "base": true,
                "coefficient": 1,
                "detail" : "Prise en charge des frais liés aux blessures ou au décès du conducteur, même lorsqu'il est responsable de l'accident (exemples : perte de revenu, aide à domicile, frais d'aménagement du domicile...).",
                "plafond" : "Jusqu'à 250 000 d'euros."
            },
            {
                "text": "Assistance véhicule et personne",
                "base": true,
                "coefficient": 1,
                "detail" : "Dépannage et remorquage de votre véhicule 24h/24, 7j/7 et prise en charge des garanties d'assistance à la personne en cas d'accident corporel ou du décès du bénéficiaire en cas d'accident.",
                "seuil" : "30 km de chez vous en cas de panne, et sans seuil dans les autres cas."
            },
            {
                "text": "Bris de glace",
                "base": true,
                "coefficient": 1,
                "detail" : "Prise en charge des dommages subis par le pare-brise, les vitres, le toit vitré et les feux avant. Les feux-arrière sont couverts au titre de la garantie Dommages tous accidents, si celle-ci est souscrite.",
                "plafond" : "Frais de remplacement ou de réparation."
            },
            {
                "text": "Dommage tous accidents et vandalisme",
                "base": true,
                "coefficient": 1,
                "detail" : "Prise en charge des dommages en cas d'accident (responsable ou non) et de vandalisme." ,
                "plafond" : "Pas de plafond. L'indemnisation se base sur la valeur déterminée par un expert ou une valeur majorée avec l'option Indemnisation renforcée."
            },
            {
                "text": "Evenement climatique et attentat",
                "base": true,
                "coefficient": 1,
                "detail" : "Prise en charge des dommages résultant de tempête, ouragan, cyclone, grêle, chute de neige, attentat ou acte de terrorisme.",
                "plafond" : "Pas de plafond. Valeur déterminée par un expert ou valeur majorée avec l'option Indemnisation renforcée."
            },
            {
                "text": "Indemnisation standard",
                "base": true,
                "coefficient": 1,
                "detail" : "Indemnisation standard si votre véhicule est irréparable ou volé.",
                "plafond" :"Indemnisation basée sur la valeur à dire d'expert"
            },
            {
                "text": "Incendie et vol",
                "base": false,
                "coefficient": 1.04,
                "detail" : "Prise en charge des dommages en cas de vol, tentative de vol, incendie, foudre et explosion.",
                "plafond" : "Pas de plafond. L'indemnisation se base sur la valeur déterminée par un expert ou une valeur majorée avec l'option Indemnisation renforcée."
            },
            {
                "text": "Véhicule de prêt",
                "base": false,
                "coefficient": 1.05,
                "detail" : "Mise à disposition d’une voiture de location de catégorie B (citadine), ou de catégorie équivalente pour les utilitaires des pros à usage pro.",
                "pret" :
                    "En cas de panne : jusqu'à 7 jours \n" +
                    "En cas d'accident ou d'incendie : jusqu'à 15 jours\n" +
                    "En cas de vol :  jusqu'à 30 jours"
            },
            {
                "text": "Protection juridique",
                "base": false,
                "coefficient": 1.08,
                "detail" : "Conseils juridiques et prise en charge des litiges liés à votre véhicule (achat/vente, location, réparation, usurpation de plaques...)",
                "plafond" :"Jusqu'à 20 000 euros de frais."
            },
            {
                "text": "Contenu de véhicule",
                "base": false,
                "coefficient": 1.07,
                "detail" : "Indemnisation de vos effets personnels, de votre matériel professionnel et de vos marchandises transportés lorsqu’ils sont volés ou endommagés lors d'un accident, ou volés seuls suite à effraction du véhicule.",
                "plafond" : "Jusqu'à 1 000 euros."
            },
            {
                "text": "Bris de rétro",
                "base": false,
                "coefficient": 1.1,
                "detail" : "Prise en charge des dommages subis sur les rétroviseurs",
                "plafond" : "Frais de remplacement ou de réparation."
            },
            {
                "text": "Equipement extérieur",
                "base": false,
                "coefficient": 1.1,
                "detail" : "Prise en charge des dommages subis sur les équipements extérieurs (antenne..)",
                "plafond" : "Frais de remplacement ou de réparation."
            },
            {
                "text": "Indemnisation renforcée",
                "base": false,
                "coefficient": 1.06,
                "detail" : "Augmentation de votre indemnisation si votre véhicule est irréparable ou volé",
                "plafond" : "Indemnisation selon l'âge du véhicule au moment du sinistre :\n" +
                    "- jusqu'à 2 ans : indemnisation basée sur la valeur d'achat du véhicule (sur facture);\n" +
                    "- au-delà de 2 ans : indemnisation basée sur la valeur à dire d'expert + 20 % (avec toujours un minimum de 1 000 euros)."
            }
        ],
        devisAutoPrice: [
            {
                marque: "ALFA ROMEO",
                modeles: [
                    {
                        text: "GIULETTA",
                        prix: 31.60
                    },
                    {
                        text: "GIULIA",
                        prix: 49.30
                    },
                    {
                        text: "MITO",
                        prix: 28.60
                    },
                    {
                        text: "STELVIO",
                        prix: 44.60
                    }
                ]
            },
            {
                marque: "AUDI",
                modeles: [
                    {
                        text: "A1",
                        prix: 27.90
                    },
                    {
                        text: "A3",
                        prix: 34.20
                    },
                    {
                        text: "A4 V",
                        prix: 37.50
                    },
                    {
                        text: "R8",
                        prix: 65
                    }
                ]
            },
            {
                marque: "CITROEN",
                modeles: [
                    {
                        text: "C3 III",
                        prix: 28.20
                    },
                    {
                        text: "C4 PICASSO II",
                        prix: 33.10
                    },
                    {
                        text: "DS5",
                        prix: 35.20
                    },
                    {
                        text: "SAXO",
                        prix: 23.20
                    },
                    {
                        text: "XANTIA",
                        prix: 25.20
                    }
                ]
            },
            {
                marque: "DACIA",
                modeles: [
                    {
                        text: "DUSTER",
                        prix: 34.30
                    },
                    {
                        text: "LODGY",
                        prix: 28.40
                    },
                    {
                        text: "LOGAN",
                        prix: 32.10
                    },
                    {
                        text: "SANDERO",
                        prix: 35.60
                    },
                    {
                        text: "SPRING",
                        prix: 42.50
                    }
                ]
            },
            {
                marque: "FIAT",
                modeles: [
                    {
                        text: "500X",
                        prix: 37.80
                    },
                    {
                        text: "BRAVO II",
                        prix: 32.10
                    },
                    {
                        text: "DUCATO",
                        prix: 52.10
                    },
                    {
                        text: "MULTIPLA",
                        prix: 39.10
                    },
                    {
                        text: "STRADA",
                        prix: 44.20
                    }
                ]
            },
            {
                marque: "FORD",
                modeles: [
                    {
                        text: "ESCORT",
                        prix: 29.70
                    },
                    {
                        text: "FIESTA",
                        prix: 35.30
                    },
                    {
                        text: "FOCUS IV",
                        prix: 41.20
                    },
                    {
                        text: "MONDEO",
                        prix: 39.30
                    }
                ]
            },
            {
                marque: "MERCEDES",
                modeles: [
                    {
                        text: "CLASSE A IV",
                        prix: 38.10
                    },
                    {
                        text: "CLASSE E",
                        prix: 52.30
                    },
                    {
                        text: "CLASSE GLA II",
                        prix: 55.10
                    },
                    {
                        text: "CLASSE SLS",
                        prix: 48.30
                    },
                    {
                        text: "GT",
                        prix: 51.30
                    }
                ]
            },
            {
                marque: "OPEL",
                modeles: [
                    {
                        text: "ASTRA",
                        prix: 31.10
                    },
                    {
                        text: "CORSA",
                        prix: 33.30
                    },
                    {
                        text: "CROSSLAND X",
                        prix: 40.50
                    },
                    {
                        text: "ZAFIRA",
                        prix: 42.30
                    }
                ]
            },
            {
                marque: "PEUGEOT",
                modeles: [
                    {
                        text: "206",
                        prix: 29.10
                    },
                    {
                        text: "308",
                        prix: 35.30
                    },
                    {
                        text: "5008",
                        prix: 38.30
                    },
                    {
                        text: "607",
                        prix: 32.70
                    },
                    {
                        text: "RCZ",
                        prix: 40.10
                    }
                ]
            },
            {
                marque: "RENAULT",
                modeles: [
                    {
                        text: "CAPTUR II",
                        prix: 43.40
                    },
                    {
                        text: "CLIO IV",
                        prix: 31.90
                    },
                    {
                        text: "ESPACE",
                        prix: 32.70
                    },
                    {
                        text: "KADJAR",
                        prix: 35.50
                    },
                    {
                        text: "MEGANE III",
                        prix: 42.80
                    }
                ]
            },
            {
                marque: "SKODA",
                modeles: [
                    {
                        text: "ENYAQ",
                        prix: 45.80
                    },
                    {
                        text: "FABIA IV",
                        prix: 41.50
                    },
                    {
                        text: "KODIAQ",
                        prix: 39.20
                    },
                    {
                        text: "OCTAVIA",
                        prix: 28.20
                    }
                ]
            },
            {
                marque: "TESLA",
                modeles: [
                    {
                        text: "MODEL 3",
                        prix: 47.30
                    },
                    {
                        text: "MODEL S",
                        prix: 53.90
                    },
                    {
                        text: "MODEL X",
                        prix: 57.40
                    },
                    {
                        text: "MODEL Y",
                        prix: 55.40
                    }
                ]
            },
            {
                marque: "TOYOTA",
                modeles: [
                    {
                        text: "AURIS",
                        prix: 32.30
                    },
                    {
                        text: "COROLLA",
                        prix: 29.80
                    },
                    {
                        text: "HIGHLANDER",
                        prix: 43.10
                    },
                    {
                        text: "RAV4 V",
                        prix: 47.40
                    }
                ]
            },
            {
                marque: "VOLKSWAGEN",
                modeles: [
                    {
                        text: "GOLF IV",
                        prix: 29.50
                    },
                    {
                        text: "POLO V",
                        prix: 35.10
                    },
                    {
                        text: "T-ROC",
                        prix: 42.30
                    },
                    {
                        text: "TIGUAN",
                        prix: 39.20
                    }
                ]
            },
        ]
    },
    mutations: {
        INCREASE_DEVIS_AUTO_PAGE(state, amount = 1) {
            if (state.devisAutoPage <= 10) {
                state.devisAutoPage += Number(amount)
            }
        },
        DECREASE_DEVIS_AUTO_PAGE(state, amount = 1) {
            if (state.devisAutoPage > 1) {
                state.devisAutoPage -= Number(amount)
            }
        },
        INCREASE_DEVIS_VOYAGE_PAGE(state, amount = 1) {
            if (state.devisVoyagePage <= 5) {
                state.devisVoyagePage += Number(amount)
            }
        },
        DECREASE_DEVIS_VOYAGE_PAGE(state, amount = 1) {
            if (state.devisVoyagePage > 1) {
                state.devisVoyagePage -= Number(amount)
            }
        },
        INCREASE_ASSISTANCE_PAGE(state, amount = 1) {
            if (state.assistancePage <= 5) {
                state.assistancePage += Number(amount)
            }
        },
        DECREASE_ASSISTANCE_PAGE(state, amount = 1) {
            if (state.assistancePage > 1) {
                state.assistancePage -= Number(amount)
            }
        },
        CONNECT(state) {
            state.isConnected = true
        },
        DISCONNECT(state) {
            state.isConnected = false
        },
        CHANGE_PRENOM(state,amount) {
            state.prenomConnected = String(amount)
        },
        RESET_PAGE(state) {
            state.devisVoyagePage = 1
            state.devisAutoPage = 1
        }
    },
    actions: {
        increaseDevisAutoPage({commit}, amount) {
            commit('INCREASE_DEVIS_AUTO_PAGE', amount)
        },
        decreaseDevisAutoPage({commit}, amount) {
            commit('DECREASE_DEVIS_AUTO_PAGE', amount)
        },
        increaseDevisVoyagePage({commit}, amount) {
            commit('INCREASE_DEVIS_VOYAGE_PAGE', amount)
        },
        decreaseDevisVoyagePage({commit}, amount) {
            commit('DECREASE_DEVIS_VOYAGE_PAGE', amount)
        },
        increaseAssistancePage({commit}, amount) {
            commit('INCREASE_ASSISTANCE_PAGE', amount)
        },
        decreaseAssistancePage({commit}, amount) {
            commit('DECREASE_ASSISTANCE_PAGE', amount)
        },
        resetPage({commit}) {
            commit('RESET_PAGE')
        },
        connect({commit}) {
            commit('CONNECT')
        },
        disconnect({commit}) {
            commit('DISCONNECT')
        },
        changePrenom({commit}, amount) {
            commit('CHANGE_PRENOM',amount)
        }
    },
    modules: {}
})
