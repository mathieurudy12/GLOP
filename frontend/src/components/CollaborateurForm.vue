<template>
  <div v-if="error">
    <h3 style="color: red">{{ $t('error.unauthorized') }}</h3>
  </div>
  <div v-else-if="this.cookies.get('droitsAcces') === '2'">
    <p style="color: red; font-size: large; font-weight: bold">/!\PAGE NON AUTORISEE AVEC VOS DROITS/!\</p>
  </div>
  <div v-else-if="this.isPartenaire" style="
  -webkit-box-shadow: 0px 1px 6px 2px #3E3D3E;
  box-shadow: 0px 1px 6px 2px #3E3D3E;
width: 80%;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
padding: 3%">
    <h3 style="text-decoration: underline">Information
      {{ this.isPartenaire ? "partenaire" : this.isCollaborateur ? "collaborateur" : "" }}</h3>
    <label class="mb-2 mt-2" style="font-weight: bold">Nom</label>
    <input type="text" class="form-control" v-model="part.info.nom" placeholder="Nom">
    <label class="mb-2 mt-2" style="font-weight: bold">N° de téléphone</label>
    <input type="text" class="form-control" v-model="part.info.numero" placeholder="N°">
    <h3 class="mt-4" style="text-decoration: underline">Adresse</h3>
    <label class="mb-2 mt-2" style="font-weight: bold">N°</label>
    <input type="text" class="form-control" v-model="part.adresse.numero" placeholder="N°">
    <label class="mb-2 mt-2" style="font-weight: bold">Rue</label>
    <input type="text" class="form-control" v-model="part.adresse.rue" placeholder="Rue">
    <label class="mb-2 mt-2" style="font-weight: bold">Compléments</label>
    <input type="text" class="form-control" v-model="part.adresse.complement" placeholder="Compléments">
    <label class="mb-2 mt-2" style="font-weight: bold">Ville</label>
    <input type="text" class="form-control" v-model="part.adresse.ville" placeholder="Ville">
    <label class="mb-2 mt-2" style="font-weight: bold">Code postal</label>
    <input type="text" class="form-control" v-model="part.adresse.codePostal" placeholder="Code postal">
    <label class="mb-2 mt-2" style="font-weight: bold">Code ISO du pays</label>
    <select v-model="part.adresse.codeIso" class="form-control">
      <option v-for="option in iso" :key="option">
        {{ option }}
      </option>
    </select>
    <button type="button" class="m-1 mt-3 btn btn-success" @click="save">Valider</button>
    <div v-if="errorMsg" class="mt-3 alert alert-danger alert-dismissible fade show">
      Veuillez saisir la totalité des champs
    </div>
  </div>
  <div v-else-if="this.isCollaborateur" style="-webkit-box-shadow: 0px 1px 6px 2px #3E3D3E;
  box-shadow: 0px 1px 6px 2px #3E3D3E;
width: 80%;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
padding: 3%"
  >
    <h3 style="text-decoration: underline">Information</h3>
    <label class="mb-2 mt-2" style="font-weight: bold">Nom</label>
    <input type="text" class="form-control" v-model="collab.info.nom" placeholder="Nom">
    <label class="mb-2 mt-2" style="font-weight: bold">Prénom</label>
    <input type="text" class="form-control" v-model="collab.info.prenom" placeholder="Prénom">
    <label class="mb-2 mt-2" style="font-weight: bold">Mail</label>
    <input type="email" class="form-control" v-model="collab.info.mail" placeholder="Mail">
    <label class="mb-2 mt-2" style="font-weight: bold">Mot de passe</label>
    <input type="password" class="form-control" v-model="collab.info.mdp" placeholder="Mot de passe">
    <label class="mb-2 mt-2" style="font-weight: bold">Matricule</label>
    <input type="text" class="form-control" v-model="collab.info.matricule" placeholder="Matricule">
    <label class="mb-2 mt-2" style="font-weight: bold">Date embauche</label>
    <datepicker v-model="collab.info.dateEmbauche"  class="form-control" inputFormat="dd-MM-yyyy"
                />
    <button type="button" class="m-1 mt-3 btn btn-success" @click="save">Valider</button>
    <div v-if="errorMsg" class="mt-3 alert alert-danger alert-dismissible fade show">
      Veuillez saisir la totalité des champs
    </div>
  </div>

</template>

<script>
import {useCookies} from "vue3-cookies";
import Datepicker from 'vue3-datepicker'
import {Utils} from "../services/utils";

export default {
  name: "CollaborateurForm",
  components: {
    Datepicker
  },
  data() {
    return {
      error: false,//Pour gérer les erreurs
      errorMsg: false,//Pour gérer le message d'erreur
      iso: ["FR", "BE"],//Liste des codes ISO
      part: {
        info: {
          nom: "",
          numero: ""
        },//Informations du partenaire
        adresse: {
          numero: "",
          rue: "",
          complement: "",
          codePostal: "",
          ville: "",
          codeIso: ""
        }//Adresse du partenaire
      },
      collab: {
        info: {
          prenom: "",
          nom: "",
          mail: "",
          mdp: "",
          dateEmbauche: "",
          matricule: ""
        }//Informations du collaborateur
      }
    }
  },
  props: {
    createMode: {
      type: Boolean
    },
    isPartenaire: {
      type: Boolean
    },
    isCollaborateur: {
      type: Boolean
    },
    partenaire: {
      type: Object
    },
    collaborateur: {
      type: Object
    },
    display: {
      type: Boolean
    }
  },
  methods: {
    //Ajouter ou modifier un partenaire ou un collaborateur
    save() {
      if (this.isPartenaire) {
        const data = {
          "codeIso": this.part.adresse.codeIso,
          "complements": this.part.adresse.complement.toLowerCase(),
          "rueAdresse": (this.part.adresse.numero + '_' + this.part.adresse.rue.toLowerCase()),
          "villeCpAdresse": (this.part.adresse.ville.toLowerCase() + '_' + this.part.adresse.codePostal),
          "nomPartenaire": this.part.info.nom,
          "numTelephone": this.part.info.numero,
        }
        this.errorMsg = false
        if (this.partenaire) {
          if (!this.createMode) {
            data.idPartenaire = this.partenaire.partenaire.idPartenaire
            if (this.checkField(data)) {
              this.$emit("updatePartenaire", data)
            } else {
              this.errorMsg = true
            }
          } else {
            if (this.checkField(data)) {
              this.$emit("addPartenaire", data)
            } else {
              this.errorMsg = true
            }
          }
        }
      } else if (this.isCollaborateur) {
        const data = {
          "nom": this.collab.info.nom,
          "prenom": this.collab.info.prenom,
          "mail": this.collab.info.mail,
          "mdp": this.collab.info.mdp,
          "matricule": this.collab.info.matricule,
          "dateEmbauche": this.collab.info.dateEmbauche === '' ? '' : Utils.formatDate(this.collab.info.dateEmbauche)
        }
        this.errorMsg = false;
        if(this.checkField(data)) {
          this.$emit("addCollaborateur", data)
        } else {
          this.errorMsg = true;
        }
      }

    },
    //Mettre la première lettre en majuscule
    ucfirst(str) {
      const firstLetter = str.substr(0, 1);
      return firstLetter.toUpperCase() + str.substr(1);
    },
    //Vérifier que la valeur n'est pas vide
    checkField(data) {
      let variable = true;
      Object.values(data).forEach(value => {
        if (value == '' || value == '_') {
          variable = false;
        }
      })
      return variable;
    }
  },

  //Setup des cookies
  setup() {
    const {cookies} = useCookies();
    return {cookies};
  },
  mounted() {
    if (!this.cookies.get('collaborateur') || this.cookies.get('collaborateur') === 'false') {
      this.error = true;
    }
    if (this.isPartenaire && !this.createMode) {
      this.part.info.nom = this.partenaire.partenaire.nomPartenaire
      this.part.info.numero = this.partenaire.partenaire.numTelephone
      this.part.adresse.numero = this.partenaire.adresse.rueAdresse.split("_")[0]
      this.part.adresse.rue = this.ucfirst(this.partenaire.adresse.rueAdresse.split("_")[1])
      this.part.adresse.complement = this.ucfirst(this.partenaire.adresse.complements)
      this.part.adresse.ville = this.ucfirst(this.partenaire.adresse.villeCpAdresse.split("_")[0])
      this.part.adresse.codePostal = this.partenaire.adresse.villeCpAdresse.split("_")[1]
      this.part.adresse.codeIso = this.partenaire.adresse.codeIso
    }
  },

}
</script>
