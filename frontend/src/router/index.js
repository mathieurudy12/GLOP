import {createRouter, createWebHistory} from 'vue-router'
import Home from '../views/Home.vue'
import Contact from '../views/Contact.vue'
import Auth from "../views/Auth";
import DevisAuto from "../views/DevisAuto";
import DevisVoyage from "../views/DevisVoyage";
import Profile from "../views/Profile";
import Vehicules from "../views/Vehicules";
import Contrats from "../views/Contrats";
import ProfileAdmin from "../views/ProfileAdmin";
import AllClients from "../views/AllClients";
import AllDossiers from "../views/AllDossiers";
import ProfileContrat from "../components/ProfileContrat";
import UpdateDossier from "../views/UpdateDossier";
import SeeFilesDossier from "../views/SeeFilesDossier";
import Assistance from "../views/Assistance"
import Partenaire from "../views/Partenaire";
import ContactPartenaire from "../components/ContactPartenaire";
import Collaborateur from "../views/Collaborateur";
import DashboardMobisure from "../views/dashboard/DashboardMobisure";
import DashboardClient from "../views/dashboard/DashboardClient";
import DashboardPartenaire from "../views/dashboard/DashboardPartenaire";
import AllAssistances from "../views/AllAssistances";

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/contact',
        name: 'contact',
        component: Contact
    },
    {
        path: '/connexion',
        name: 'connexion',
        component: Auth
    },
    {
        path: '/inscription',
        name: 'inscription',
        component: Auth
    },
    {
        path: '/devis_auto',
        name: 'devis-auto',
        component: DevisAuto
    },
    {
        path: '/devis_voyage',
        name: 'devis-voyage',
        component: DevisVoyage
    },
    {
        path: "/profil",
        name: 'profil',
        component: Profile
    },
    {
        path: "/profilAdmin",
        name: 'profilAdmin',
        component: ProfileAdmin
    },
    {
        path: "/vehicules",
        name: 'vehciules',
        component: Vehicules
    },
    {
        path: "/contrats/:id",
        name: 'contrats',
        component: Contrats,
        props: true
    },
    {
        path: "/contrat/:id",
        name: 'contrat',
        component: ProfileContrat,
        props: true
    },
    {
        path: "/all_clients",
        name: 'allClients',
        component: AllClients
    },
    {
        path: "/all_dossiers",
        name: 'allDossiers',
        component: AllDossiers
    },
    {
        path: "/updateDossier/:id",
        name: 'updateDossier',
        component: UpdateDossier,
        props: true
    },
    {
        path: "/seeFilesDossier/:id",
        name: 'seeFilesDossier',
        component: SeeFilesDossier,
        props: true
    },
    {
        path: "/:catchAll(.*)",
        component: Home,
    },
    {
        path: "/dashboardMobisure",
        component: DashboardMobisure
    },
    {
        path: "/dashboardClient",
        component: DashboardClient
    },
    {
        path: "/dashboardPartenaire",
        component: DashboardPartenaire
    },
    {
        path: "/assistance",
        name: 'assistance',
        component: Assistance
    },
    {
        path: "/partenaire",
        name: 'partenaire',
        component: Partenaire
    },
    {
        path: "/contactPartenaire/:id",
        name: "contactPartenaire",
        component: ContactPartenaire,
        props: true
    },
    {
        path: "/collaborateur",
        name: 'collaborateur',
        component: Collaborateur
    },
    {
        path: "/all_assistances",
        name: 'all_assistances',
        component: AllAssistances
    }
]

const router = createRouter({
    mode: 'history',
    history: createWebHistory(process.env.BASE_URL),
    routes: routes,
    linkActiveClass: "active",
    linkExactActiveClass: "exact-active",
    historyApiFallback: true
})

export default router
